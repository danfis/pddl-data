(define 
(problem MABlocksWorld-4-10-5)
(:domain MABlocksWorld-4-10-5)
(:init
	(clear T0 S0)
	(clear T1 S1)
	(on B7 T2 S2)
	(clear B7 S2)
	(on B3 T3 S3)
	(clear B3 S3)
	(clear T4 S4)
	(on B6 T5 S5)
	(clear B6 S5)
	(on B2 T6 S6)
	(clear B2 S6)
	(on B8 T7 S7)
	(clear B8 S7)
	(clear T8 S8)
	(clear T9 S9)
	(clear T10 S10)
	(clear T11 S11)
	(on B0 T12 S12)
	(clear B0 S12)
	(on B1 T13 S13)
	(on B9 B1 S13)
	(clear B9 S13)
	(clear T14 S14)
	(clear T15 S15)
	(on B5 T16 S16)
	(on B4 B5 S16)
	(clear B4 S16)
	(clear T17 S17)
	(clear T18 S18)
	(clear T19 S19)
)
(:goal (and
	(on B7 T2 S2)
	(on B8 B7 S2)
	(on B5 B8 S2)
	(clear B5 S2)
	(on B2 T9 S9)
	(clear B2 S9)
	(on B9 T11 S11)
	(on B1 B9 S11)
	(clear B1 S11)
	(on B3 T12 S12)
	(clear B3 S12)
	(on B6 T14 S14)
	(on B0 B6 S14)
	(clear B0 S14)
	(on B4 T16 S16)
	(clear B4 S16)
))
)
