(define 
(problem MABlocksWorld-4-5-3)
(:domain MABlocksWorld-4-5-3)
(:init
	(on B1 T0 S0)
	(clear B1 S0)
	(clear T1 S1)
	(on B2 T2 S2)
	(on B0 B2 S2)
	(clear B0 S2)
	(clear T3 S3)
	(clear T4 S4)
	(on B4 T5 S5)
	(clear B4 S5)
	(clear T6 S6)
	(on B3 T7 S7)
	(clear B3 S7)
	(clear T8 S8)
	(clear T9 S9)
	(clear T10 S10)
	(clear T11 S11)
)
(:goal (and
	(on B0 T0 S0)
	(on B3 B0 S0)
	(clear B3 S0)
	(on B4 T5 S5)
	(on B2 B4 S5)
	(clear B2 S5)
	(on B1 T10 S10)
	(clear B1 S10)
))
)
