(define 
(domain MABlocksWorld-6-5-5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 A0 - ARM
	 A1 - ARM
	 A2 - ARM
	 A3 - ARM
	 A4 - ARM
	 A5 - ARM
	 S0 - STACK
	 S1 - STACK
	 S2 - STACK
	 S3 - STACK
	 S4 - STACK
	 S5 - STACK
	 S6 - STACK
	 S7 - STACK
	 S8 - STACK
	 S9 - STACK
	 S10 - STACK
	 S11 - STACK
	 S12 - STACK
	 S13 - STACK
	 S14 - STACK
	 S15 - STACK
	 S16 - STACK
	 S17 - STACK
	 S18 - STACK
	 S19 - STACK
	 S20 - STACK
	 S21 - STACK
	 S22 - STACK
	 S23 - STACK
	 S24 - STACK
	 S25 - STACK
	 S26 - STACK
	 S27 - STACK
	 S28 - STACK
	 S29 - STACK
	 S30 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a0-s0-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (clear ?bMove S0) (clear ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S0)) (on ?bMove ?bTarget S1) (clear ?bSource S0) (not (clear ?bTarget S1)) (not (clear ?bMove S0)) (clear ?bMove S1))
)

(:action move-a0-s1-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (clear ?bMove S1) (clear ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S1)) (on ?bMove ?bTarget S0) (clear ?bSource S1) (not (clear ?bTarget S0)) (not (clear ?bMove S1)) (clear ?bMove S0))
)

(:action move-a0-s1-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (clear ?bMove S1) (clear ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S1)) (on ?bMove ?bTarget S2) (clear ?bSource S1) (not (clear ?bTarget S2)) (not (clear ?bMove S1)) (clear ?bMove S2))
)

(:action move-a0-s2-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (clear ?bMove S2) (clear ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S2)) (on ?bMove ?bTarget S1) (clear ?bSource S2) (not (clear ?bTarget S1)) (not (clear ?bMove S2)) (clear ?bMove S1))
)

(:action move-a0-s2-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (clear ?bMove S2) (clear ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S2)) (on ?bMove ?bTarget S3) (clear ?bSource S2) (not (clear ?bTarget S3)) (not (clear ?bMove S2)) (clear ?bMove S3))
)

(:action move-a0-s3-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (clear ?bMove S3) (clear ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S3)) (on ?bMove ?bTarget S2) (clear ?bSource S3) (not (clear ?bTarget S2)) (not (clear ?bMove S3)) (clear ?bMove S2))
)

(:action move-a0-s3-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (clear ?bMove S3) (clear ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S3)) (on ?bMove ?bTarget S4) (clear ?bSource S3) (not (clear ?bTarget S4)) (not (clear ?bMove S3)) (clear ?bMove S4))
)

(:action move-a0-s4-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (clear ?bMove S4) (clear ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S4)) (on ?bMove ?bTarget S3) (clear ?bSource S4) (not (clear ?bTarget S3)) (not (clear ?bMove S4)) (clear ?bMove S3))
)

(:action move-a0-s4-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (clear ?bMove S4) (clear ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S4)) (on ?bMove ?bTarget S5) (clear ?bSource S4) (not (clear ?bTarget S5)) (not (clear ?bMove S4)) (clear ?bMove S5))
)

(:action move-a0-s5-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (clear ?bMove S5) (clear ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S5)) (on ?bMove ?bTarget S4) (clear ?bSource S5) (not (clear ?bTarget S4)) (not (clear ?bMove S5)) (clear ?bMove S4))
)

(:action move-a1-s5-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (clear ?bMove S5) (clear ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S5)) (on ?bMove ?bTarget S6) (clear ?bSource S5) (not (clear ?bTarget S6)) (not (clear ?bMove S5)) (clear ?bMove S6))
)

(:action move-a1-s6-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (clear ?bMove S6) (clear ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S6)) (on ?bMove ?bTarget S5) (clear ?bSource S6) (not (clear ?bTarget S5)) (not (clear ?bMove S6)) (clear ?bMove S5))
)

(:action move-a1-s6-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (clear ?bMove S6) (clear ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S6)) (on ?bMove ?bTarget S7) (clear ?bSource S6) (not (clear ?bTarget S7)) (not (clear ?bMove S6)) (clear ?bMove S7))
)

(:action move-a1-s7-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (clear ?bMove S7) (clear ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S7)) (on ?bMove ?bTarget S6) (clear ?bSource S7) (not (clear ?bTarget S6)) (not (clear ?bMove S7)) (clear ?bMove S6))
)

(:action move-a1-s7-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (clear ?bMove S7) (clear ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S7)) (on ?bMove ?bTarget S8) (clear ?bSource S7) (not (clear ?bTarget S8)) (not (clear ?bMove S7)) (clear ?bMove S8))
)

(:action move-a1-s8-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (clear ?bMove S8) (clear ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S8)) (on ?bMove ?bTarget S7) (clear ?bSource S8) (not (clear ?bTarget S7)) (not (clear ?bMove S8)) (clear ?bMove S7))
)

(:action move-a1-s8-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (clear ?bMove S8) (clear ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S8)) (on ?bMove ?bTarget S9) (clear ?bSource S8) (not (clear ?bTarget S9)) (not (clear ?bMove S8)) (clear ?bMove S9))
)

(:action move-a1-s9-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (clear ?bMove S9) (clear ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S9)) (on ?bMove ?bTarget S8) (clear ?bSource S9) (not (clear ?bTarget S8)) (not (clear ?bMove S9)) (clear ?bMove S8))
)

(:action move-a1-s9-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (clear ?bMove S9) (clear ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S9)) (on ?bMove ?bTarget S10) (clear ?bSource S9) (not (clear ?bTarget S10)) (not (clear ?bMove S9)) (clear ?bMove S10))
)

(:action move-a1-s10-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (clear ?bMove S10) (clear ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S10)) (on ?bMove ?bTarget S9) (clear ?bSource S10) (not (clear ?bTarget S9)) (not (clear ?bMove S10)) (clear ?bMove S9))
)

(:action move-a2-s10-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (clear ?bMove S10) (clear ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S10)) (on ?bMove ?bTarget S11) (clear ?bSource S10) (not (clear ?bTarget S11)) (not (clear ?bMove S10)) (clear ?bMove S11))
)

(:action move-a2-s11-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (clear ?bMove S11) (clear ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S11)) (on ?bMove ?bTarget S10) (clear ?bSource S11) (not (clear ?bTarget S10)) (not (clear ?bMove S11)) (clear ?bMove S10))
)

(:action move-a2-s11-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (clear ?bMove S11) (clear ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S11)) (on ?bMove ?bTarget S12) (clear ?bSource S11) (not (clear ?bTarget S12)) (not (clear ?bMove S11)) (clear ?bMove S12))
)

(:action move-a2-s12-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (clear ?bMove S12) (clear ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S12)) (on ?bMove ?bTarget S11) (clear ?bSource S12) (not (clear ?bTarget S11)) (not (clear ?bMove S12)) (clear ?bMove S11))
)

(:action move-a2-s12-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (clear ?bMove S12) (clear ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S12)) (on ?bMove ?bTarget S13) (clear ?bSource S12) (not (clear ?bTarget S13)) (not (clear ?bMove S12)) (clear ?bMove S13))
)

(:action move-a2-s13-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (clear ?bMove S13) (clear ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S13)) (on ?bMove ?bTarget S12) (clear ?bSource S13) (not (clear ?bTarget S12)) (not (clear ?bMove S13)) (clear ?bMove S12))
)

(:action move-a2-s13-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (clear ?bMove S13) (clear ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S13)) (on ?bMove ?bTarget S14) (clear ?bSource S13) (not (clear ?bTarget S14)) (not (clear ?bMove S13)) (clear ?bMove S14))
)

(:action move-a2-s14-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (clear ?bMove S14) (clear ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S14)) (on ?bMove ?bTarget S13) (clear ?bSource S14) (not (clear ?bTarget S13)) (not (clear ?bMove S14)) (clear ?bMove S13))
)

(:action move-a2-s14-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (clear ?bMove S14) (clear ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S14)) (on ?bMove ?bTarget S15) (clear ?bSource S14) (not (clear ?bTarget S15)) (not (clear ?bMove S14)) (clear ?bMove S15))
)

(:action move-a2-s15-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (clear ?bMove S15) (clear ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S15)) (on ?bMove ?bTarget S14) (clear ?bSource S15) (not (clear ?bTarget S14)) (not (clear ?bMove S15)) (clear ?bMove S14))
)

(:action move-a3-s15-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (clear ?bMove S15) (clear ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S15)) (on ?bMove ?bTarget S16) (clear ?bSource S15) (not (clear ?bTarget S16)) (not (clear ?bMove S15)) (clear ?bMove S16))
)

(:action move-a3-s16-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (clear ?bMove S16) (clear ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S16)) (on ?bMove ?bTarget S15) (clear ?bSource S16) (not (clear ?bTarget S15)) (not (clear ?bMove S16)) (clear ?bMove S15))
)

(:action move-a3-s16-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (clear ?bMove S16) (clear ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S16)) (on ?bMove ?bTarget S17) (clear ?bSource S16) (not (clear ?bTarget S17)) (not (clear ?bMove S16)) (clear ?bMove S17))
)

(:action move-a3-s17-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (clear ?bMove S17) (clear ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S17)) (on ?bMove ?bTarget S16) (clear ?bSource S17) (not (clear ?bTarget S16)) (not (clear ?bMove S17)) (clear ?bMove S16))
)

(:action move-a3-s17-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (clear ?bMove S17) (clear ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S17)) (on ?bMove ?bTarget S18) (clear ?bSource S17) (not (clear ?bTarget S18)) (not (clear ?bMove S17)) (clear ?bMove S18))
)

(:action move-a3-s18-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (clear ?bMove S18) (clear ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S18)) (on ?bMove ?bTarget S17) (clear ?bSource S18) (not (clear ?bTarget S17)) (not (clear ?bMove S18)) (clear ?bMove S17))
)

(:action move-a3-s18-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (clear ?bMove S18) (clear ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S18)) (on ?bMove ?bTarget S19) (clear ?bSource S18) (not (clear ?bTarget S19)) (not (clear ?bMove S18)) (clear ?bMove S19))
)

(:action move-a3-s19-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (clear ?bMove S19) (clear ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S19)) (on ?bMove ?bTarget S18) (clear ?bSource S19) (not (clear ?bTarget S18)) (not (clear ?bMove S19)) (clear ?bMove S18))
)

(:action move-a3-s19-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (clear ?bMove S19) (clear ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S19)) (on ?bMove ?bTarget S20) (clear ?bSource S19) (not (clear ?bTarget S20)) (not (clear ?bMove S19)) (clear ?bMove S20))
)

(:action move-a3-s20-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (clear ?bMove S20) (clear ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S20)) (on ?bMove ?bTarget S19) (clear ?bSource S20) (not (clear ?bTarget S19)) (not (clear ?bMove S20)) (clear ?bMove S19))
)

(:action move-a4-s20-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (clear ?bMove S20) (clear ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S20)) (on ?bMove ?bTarget S21) (clear ?bSource S20) (not (clear ?bTarget S21)) (not (clear ?bMove S20)) (clear ?bMove S21))
)

(:action move-a4-s21-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (clear ?bMove S21) (clear ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S21)) (on ?bMove ?bTarget S20) (clear ?bSource S21) (not (clear ?bTarget S20)) (not (clear ?bMove S21)) (clear ?bMove S20))
)

(:action move-a4-s21-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (clear ?bMove S21) (clear ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S21)) (on ?bMove ?bTarget S22) (clear ?bSource S21) (not (clear ?bTarget S22)) (not (clear ?bMove S21)) (clear ?bMove S22))
)

(:action move-a4-s22-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (clear ?bMove S22) (clear ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S22)) (on ?bMove ?bTarget S21) (clear ?bSource S22) (not (clear ?bTarget S21)) (not (clear ?bMove S22)) (clear ?bMove S21))
)

(:action move-a4-s22-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (clear ?bMove S22) (clear ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S22)) (on ?bMove ?bTarget S23) (clear ?bSource S22) (not (clear ?bTarget S23)) (not (clear ?bMove S22)) (clear ?bMove S23))
)

(:action move-a4-s23-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (clear ?bMove S23) (clear ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S23)) (on ?bMove ?bTarget S22) (clear ?bSource S23) (not (clear ?bTarget S22)) (not (clear ?bMove S23)) (clear ?bMove S22))
)

(:action move-a4-s23-s24
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (clear ?bMove S23) (clear ?bTarget S24) )
:effect (and (not (on ?bMove ?bSource S23)) (on ?bMove ?bTarget S24) (clear ?bSource S23) (not (clear ?bTarget S24)) (not (clear ?bMove S23)) (clear ?bMove S24))
)

(:action move-a4-s24-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S24) (clear ?bMove S24) (clear ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S24)) (on ?bMove ?bTarget S23) (clear ?bSource S24) (not (clear ?bTarget S23)) (not (clear ?bMove S24)) (clear ?bMove S23))
)

(:action move-a4-s24-s25
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S24) (clear ?bMove S24) (clear ?bTarget S25) )
:effect (and (not (on ?bMove ?bSource S24)) (on ?bMove ?bTarget S25) (clear ?bSource S24) (not (clear ?bTarget S25)) (not (clear ?bMove S24)) (clear ?bMove S25))
)

(:action move-a4-s25-s24
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S25) (clear ?bMove S25) (clear ?bTarget S24) )
:effect (and (not (on ?bMove ?bSource S25)) (on ?bMove ?bTarget S24) (clear ?bSource S25) (not (clear ?bTarget S24)) (not (clear ?bMove S25)) (clear ?bMove S24))
)

(:action move-a5-s25-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S25) (clear ?bMove S25) (clear ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S25)) (on ?bMove ?bTarget S26) (clear ?bSource S25) (not (clear ?bTarget S26)) (not (clear ?bMove S25)) (clear ?bMove S26))
)

(:action move-a5-s26-s25
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (clear ?bMove S26) (clear ?bTarget S25) )
:effect (and (not (on ?bMove ?bSource S26)) (on ?bMove ?bTarget S25) (clear ?bSource S26) (not (clear ?bTarget S25)) (not (clear ?bMove S26)) (clear ?bMove S25))
)

(:action move-a5-s26-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (clear ?bMove S26) (clear ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S26)) (on ?bMove ?bTarget S27) (clear ?bSource S26) (not (clear ?bTarget S27)) (not (clear ?bMove S26)) (clear ?bMove S27))
)

(:action move-a5-s27-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (clear ?bMove S27) (clear ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S27)) (on ?bMove ?bTarget S26) (clear ?bSource S27) (not (clear ?bTarget S26)) (not (clear ?bMove S27)) (clear ?bMove S26))
)

(:action move-a5-s27-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (clear ?bMove S27) (clear ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S27)) (on ?bMove ?bTarget S28) (clear ?bSource S27) (not (clear ?bTarget S28)) (not (clear ?bMove S27)) (clear ?bMove S28))
)

(:action move-a5-s28-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (clear ?bMove S28) (clear ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S28)) (on ?bMove ?bTarget S27) (clear ?bSource S28) (not (clear ?bTarget S27)) (not (clear ?bMove S28)) (clear ?bMove S27))
)

(:action move-a5-s28-s29
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (clear ?bMove S28) (clear ?bTarget S29) )
:effect (and (not (on ?bMove ?bSource S28)) (on ?bMove ?bTarget S29) (clear ?bSource S28) (not (clear ?bTarget S29)) (not (clear ?bMove S28)) (clear ?bMove S29))
)

(:action move-a5-s29-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S29) (clear ?bMove S29) (clear ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S29)) (on ?bMove ?bTarget S28) (clear ?bSource S29) (not (clear ?bTarget S28)) (not (clear ?bMove S29)) (clear ?bMove S28))
)

(:action move-a5-s29-s30
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S29) (clear ?bMove S29) (clear ?bTarget S30) )
:effect (and (not (on ?bMove ?bSource S29)) (on ?bMove ?bTarget S30) (clear ?bSource S29) (not (clear ?bTarget S30)) (not (clear ?bMove S29)) (clear ?bMove S30))
)

(:action move-a5-s30-s29
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S30) (clear ?bMove S30) (clear ?bTarget S29) )
:effect (and (not (on ?bMove ?bSource S30)) (on ?bMove ?bTarget S29) (clear ?bSource S30) (not (clear ?bTarget S29)) (not (clear ?bMove S30)) (clear ?bMove S29))
)

)
