(define 
(problem MABlocksWorld-4-15-5)
(:domain MABlocksWorld-4-15-5)
(:init
	(clear T0 S0)
	(clear T1 S1)
	(on B11 T2 S2)
	(on B4 B11 S2)
	(clear B4 S2)
	(clear T3 S3)
	(clear T4 S4)
	(on B3 T5 S5)
	(clear B3 S5)
	(on B12 T6 S6)
	(clear B12 S6)
	(clear T7 S7)
	(clear T8 S8)
	(clear T9 S9)
	(on B10 T10 S10)
	(clear B10 S10)
	(on B0 T11 S11)
	(on B13 B0 S11)
	(on B7 B13 S11)
	(clear B7 S11)
	(on B2 T12 S12)
	(on B5 B2 S12)
	(on B6 B5 S12)
	(on B8 B6 S12)
	(on B9 B8 S12)
	(clear B9 S12)
	(clear T13 S13)
	(clear T14 S14)
	(clear T15 S15)
	(clear T16 S16)
	(on B1 T17 S17)
	(on B14 B1 S17)
	(clear B14 S17)
	(clear T18 S18)
	(clear T19 S19)
)
(:goal (and
	(on B3 T2 S2)
	(clear B3 S2)
	(on B7 T3 S3)
	(on B4 B7 S3)
	(clear B4 S3)
	(on B9 T4 S4)
	(clear B9 S4)
	(on B0 T6 S6)
	(on B10 B0 S6)
	(clear B10 S6)
	(on B8 T7 S7)
	(clear B8 S7)
	(on B5 T10 S10)
	(clear B5 S10)
	(on B1 T12 S12)
	(clear B1 S12)
	(on B13 T14 S14)
	(on B12 B13 S14)
	(clear B12 S14)
	(on B6 T19 S19)
	(on B2 B6 S19)
	(on B11 B2 S19)
	(on B14 B11 S19)
	(clear B14 S19)
))
)
