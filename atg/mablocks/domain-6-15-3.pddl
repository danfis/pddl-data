(define 
(domain MABlocksWorld-6-15-3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 B8 - BLOCK
	 B9 - BLOCK
	 B10 - BLOCK
	 B11 - BLOCK
	 B12 - BLOCK
	 B13 - BLOCK
	 B14 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 A0 - ARM
	 A1 - ARM
	 A2 - ARM
	 A3 - ARM
	 A4 - ARM
	 A5 - ARM
	 S0 - STACK
	 S1 - STACK
	 S2 - STACK
	 S3 - STACK
	 S4 - STACK
	 S5 - STACK
	 S6 - STACK
	 S7 - STACK
	 S8 - STACK
	 S9 - STACK
	 S10 - STACK
	 S11 - STACK
	 S12 - STACK
	 S13 - STACK
	 S14 - STACK
	 S15 - STACK
	 S16 - STACK
	 S17 - STACK
	 S18 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a0-s0-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (clear ?bMove S0) (clear ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S0)) (on ?bMove ?bTarget S1) (clear ?bSource S0) (not (clear ?bTarget S1)) (not (clear ?bMove S0)) (clear ?bMove S1))
)

(:action move-a0-s1-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (clear ?bMove S1) (clear ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S1)) (on ?bMove ?bTarget S0) (clear ?bSource S1) (not (clear ?bTarget S0)) (not (clear ?bMove S1)) (clear ?bMove S0))
)

(:action move-a0-s1-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (clear ?bMove S1) (clear ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S1)) (on ?bMove ?bTarget S2) (clear ?bSource S1) (not (clear ?bTarget S2)) (not (clear ?bMove S1)) (clear ?bMove S2))
)

(:action move-a0-s2-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (clear ?bMove S2) (clear ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S2)) (on ?bMove ?bTarget S1) (clear ?bSource S2) (not (clear ?bTarget S1)) (not (clear ?bMove S2)) (clear ?bMove S1))
)

(:action move-a0-s2-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (clear ?bMove S2) (clear ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S2)) (on ?bMove ?bTarget S3) (clear ?bSource S2) (not (clear ?bTarget S3)) (not (clear ?bMove S2)) (clear ?bMove S3))
)

(:action move-a0-s3-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (clear ?bMove S3) (clear ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S3)) (on ?bMove ?bTarget S2) (clear ?bSource S3) (not (clear ?bTarget S2)) (not (clear ?bMove S3)) (clear ?bMove S2))
)

(:action move-a1-s3-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (clear ?bMove S3) (clear ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S3)) (on ?bMove ?bTarget S4) (clear ?bSource S3) (not (clear ?bTarget S4)) (not (clear ?bMove S3)) (clear ?bMove S4))
)

(:action move-a1-s4-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (clear ?bMove S4) (clear ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S4)) (on ?bMove ?bTarget S3) (clear ?bSource S4) (not (clear ?bTarget S3)) (not (clear ?bMove S4)) (clear ?bMove S3))
)

(:action move-a1-s4-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (clear ?bMove S4) (clear ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S4)) (on ?bMove ?bTarget S5) (clear ?bSource S4) (not (clear ?bTarget S5)) (not (clear ?bMove S4)) (clear ?bMove S5))
)

(:action move-a1-s5-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (clear ?bMove S5) (clear ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S5)) (on ?bMove ?bTarget S4) (clear ?bSource S5) (not (clear ?bTarget S4)) (not (clear ?bMove S5)) (clear ?bMove S4))
)

(:action move-a1-s5-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (clear ?bMove S5) (clear ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S5)) (on ?bMove ?bTarget S6) (clear ?bSource S5) (not (clear ?bTarget S6)) (not (clear ?bMove S5)) (clear ?bMove S6))
)

(:action move-a1-s6-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (clear ?bMove S6) (clear ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S6)) (on ?bMove ?bTarget S5) (clear ?bSource S6) (not (clear ?bTarget S5)) (not (clear ?bMove S6)) (clear ?bMove S5))
)

(:action move-a2-s6-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (clear ?bMove S6) (clear ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S6)) (on ?bMove ?bTarget S7) (clear ?bSource S6) (not (clear ?bTarget S7)) (not (clear ?bMove S6)) (clear ?bMove S7))
)

(:action move-a2-s7-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (clear ?bMove S7) (clear ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S7)) (on ?bMove ?bTarget S6) (clear ?bSource S7) (not (clear ?bTarget S6)) (not (clear ?bMove S7)) (clear ?bMove S6))
)

(:action move-a2-s7-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (clear ?bMove S7) (clear ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S7)) (on ?bMove ?bTarget S8) (clear ?bSource S7) (not (clear ?bTarget S8)) (not (clear ?bMove S7)) (clear ?bMove S8))
)

(:action move-a2-s8-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (clear ?bMove S8) (clear ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S8)) (on ?bMove ?bTarget S7) (clear ?bSource S8) (not (clear ?bTarget S7)) (not (clear ?bMove S8)) (clear ?bMove S7))
)

(:action move-a2-s8-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (clear ?bMove S8) (clear ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S8)) (on ?bMove ?bTarget S9) (clear ?bSource S8) (not (clear ?bTarget S9)) (not (clear ?bMove S8)) (clear ?bMove S9))
)

(:action move-a2-s9-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (clear ?bMove S9) (clear ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S9)) (on ?bMove ?bTarget S8) (clear ?bSource S9) (not (clear ?bTarget S8)) (not (clear ?bMove S9)) (clear ?bMove S8))
)

(:action move-a3-s9-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (clear ?bMove S9) (clear ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S9)) (on ?bMove ?bTarget S10) (clear ?bSource S9) (not (clear ?bTarget S10)) (not (clear ?bMove S9)) (clear ?bMove S10))
)

(:action move-a3-s10-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (clear ?bMove S10) (clear ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S10)) (on ?bMove ?bTarget S9) (clear ?bSource S10) (not (clear ?bTarget S9)) (not (clear ?bMove S10)) (clear ?bMove S9))
)

(:action move-a3-s10-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (clear ?bMove S10) (clear ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S10)) (on ?bMove ?bTarget S11) (clear ?bSource S10) (not (clear ?bTarget S11)) (not (clear ?bMove S10)) (clear ?bMove S11))
)

(:action move-a3-s11-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (clear ?bMove S11) (clear ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S11)) (on ?bMove ?bTarget S10) (clear ?bSource S11) (not (clear ?bTarget S10)) (not (clear ?bMove S11)) (clear ?bMove S10))
)

(:action move-a3-s11-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (clear ?bMove S11) (clear ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S11)) (on ?bMove ?bTarget S12) (clear ?bSource S11) (not (clear ?bTarget S12)) (not (clear ?bMove S11)) (clear ?bMove S12))
)

(:action move-a3-s12-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (clear ?bMove S12) (clear ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S12)) (on ?bMove ?bTarget S11) (clear ?bSource S12) (not (clear ?bTarget S11)) (not (clear ?bMove S12)) (clear ?bMove S11))
)

(:action move-a4-s12-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (clear ?bMove S12) (clear ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S12)) (on ?bMove ?bTarget S13) (clear ?bSource S12) (not (clear ?bTarget S13)) (not (clear ?bMove S12)) (clear ?bMove S13))
)

(:action move-a4-s13-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (clear ?bMove S13) (clear ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S13)) (on ?bMove ?bTarget S12) (clear ?bSource S13) (not (clear ?bTarget S12)) (not (clear ?bMove S13)) (clear ?bMove S12))
)

(:action move-a4-s13-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (clear ?bMove S13) (clear ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S13)) (on ?bMove ?bTarget S14) (clear ?bSource S13) (not (clear ?bTarget S14)) (not (clear ?bMove S13)) (clear ?bMove S14))
)

(:action move-a4-s14-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (clear ?bMove S14) (clear ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S14)) (on ?bMove ?bTarget S13) (clear ?bSource S14) (not (clear ?bTarget S13)) (not (clear ?bMove S14)) (clear ?bMove S13))
)

(:action move-a4-s14-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (clear ?bMove S14) (clear ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S14)) (on ?bMove ?bTarget S15) (clear ?bSource S14) (not (clear ?bTarget S15)) (not (clear ?bMove S14)) (clear ?bMove S15))
)

(:action move-a4-s15-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (clear ?bMove S15) (clear ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S15)) (on ?bMove ?bTarget S14) (clear ?bSource S15) (not (clear ?bTarget S14)) (not (clear ?bMove S15)) (clear ?bMove S14))
)

(:action move-a5-s15-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (clear ?bMove S15) (clear ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S15)) (on ?bMove ?bTarget S16) (clear ?bSource S15) (not (clear ?bTarget S16)) (not (clear ?bMove S15)) (clear ?bMove S16))
)

(:action move-a5-s16-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (clear ?bMove S16) (clear ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S16)) (on ?bMove ?bTarget S15) (clear ?bSource S16) (not (clear ?bTarget S15)) (not (clear ?bMove S16)) (clear ?bMove S15))
)

(:action move-a5-s16-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (clear ?bMove S16) (clear ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S16)) (on ?bMove ?bTarget S17) (clear ?bSource S16) (not (clear ?bTarget S17)) (not (clear ?bMove S16)) (clear ?bMove S17))
)

(:action move-a5-s17-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (clear ?bMove S17) (clear ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S17)) (on ?bMove ?bTarget S16) (clear ?bSource S17) (not (clear ?bTarget S16)) (not (clear ?bMove S17)) (clear ?bMove S16))
)

(:action move-a5-s17-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (clear ?bMove S17) (clear ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S17)) (on ?bMove ?bTarget S18) (clear ?bSource S17) (not (clear ?bTarget S18)) (not (clear ?bMove S17)) (clear ?bMove S18))
)

(:action move-a5-s18-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (clear ?bMove S18) (clear ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S18)) (on ?bMove ?bTarget S17) (clear ?bSource S18) (not (clear ?bTarget S17)) (not (clear ?bMove S18)) (clear ?bMove S17))
)

)
