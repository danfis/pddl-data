(define 
(problem MABlocksWorld-6-15-5)
(:domain MABlocksWorld-6-15-5)
(:init
	(clear T0 S0)
	(on B8 T1 S1)
	(clear B8 S1)
	(clear T2 S2)
	(on B2 T3 S3)
	(clear B2 S3)
	(clear T4 S4)
	(on B1 T5 S5)
	(clear B1 S5)
	(clear T6 S6)
	(on B7 T7 S7)
	(on B12 B7 S7)
	(on B14 B12 S7)
	(on B3 B14 S7)
	(clear B3 S7)
	(on B6 T8 S8)
	(clear B6 S8)
	(on B9 T9 S9)
	(on B0 B9 S9)
	(clear B0 S9)
	(clear T10 S10)
	(clear T11 S11)
	(clear T12 S12)
	(clear T13 S13)
	(clear T14 S14)
	(on B11 T15 S15)
	(on B5 B11 S15)
	(clear B5 S15)
	(clear T16 S16)
	(clear T17 S17)
	(clear T18 S18)
	(clear T19 S19)
	(clear T20 S20)
	(clear T21 S21)
	(clear T22 S22)
	(clear T23 S23)
	(clear T24 S24)
	(clear T25 S25)
	(clear T26 S26)
	(clear T27 S27)
	(on B10 T28 S28)
	(clear B10 S28)
	(on B4 T29 S29)
	(on B13 B4 S29)
	(clear B13 S29)
)
(:goal (and
	(on B9 T2 S2)
	(on B8 B9 S2)
	(clear B8 S2)
	(on B10 T3 S3)
	(clear B10 S3)
	(on B6 T7 S7)
	(clear B6 S7)
	(on B13 T9 S9)
	(clear B13 S9)
	(on B12 T11 S11)
	(clear B12 S11)
	(on B3 T14 S14)
	(on B11 B3 S14)
	(on B4 B11 S14)
	(clear B4 S14)
	(on B1 T16 S16)
	(clear B1 S16)
	(on B2 T17 S17)
	(clear B2 S17)
	(on B0 T20 S20)
	(clear B0 S20)
	(on B14 T24 S24)
	(clear B14 S24)
	(on B7 T26 S26)
	(clear B7 S26)
	(on B5 T29 S29)
	(clear B5 S29)
))
)
