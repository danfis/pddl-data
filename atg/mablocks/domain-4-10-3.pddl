(define 
(domain MABlocksWorld-4-10-3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 B8 - BLOCK
	 B9 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 A0 - ARM
	 A1 - ARM
	 A2 - ARM
	 A3 - ARM
	 S0 - STACK
	 S1 - STACK
	 S2 - STACK
	 S3 - STACK
	 S4 - STACK
	 S5 - STACK
	 S6 - STACK
	 S7 - STACK
	 S8 - STACK
	 S9 - STACK
	 S10 - STACK
	 S11 - STACK
	 S12 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a0-s0-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (clear ?bMove S0) (clear ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S0)) (on ?bMove ?bTarget S1) (clear ?bSource S0) (not (clear ?bTarget S1)) (not (clear ?bMove S0)) (clear ?bMove S1))
)

(:action move-a0-s1-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (clear ?bMove S1) (clear ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S1)) (on ?bMove ?bTarget S0) (clear ?bSource S1) (not (clear ?bTarget S0)) (not (clear ?bMove S1)) (clear ?bMove S0))
)

(:action move-a0-s1-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (clear ?bMove S1) (clear ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S1)) (on ?bMove ?bTarget S2) (clear ?bSource S1) (not (clear ?bTarget S2)) (not (clear ?bMove S1)) (clear ?bMove S2))
)

(:action move-a0-s2-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (clear ?bMove S2) (clear ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S2)) (on ?bMove ?bTarget S1) (clear ?bSource S2) (not (clear ?bTarget S1)) (not (clear ?bMove S2)) (clear ?bMove S1))
)

(:action move-a0-s2-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (clear ?bMove S2) (clear ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S2)) (on ?bMove ?bTarget S3) (clear ?bSource S2) (not (clear ?bTarget S3)) (not (clear ?bMove S2)) (clear ?bMove S3))
)

(:action move-a0-s3-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (clear ?bMove S3) (clear ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S3)) (on ?bMove ?bTarget S2) (clear ?bSource S3) (not (clear ?bTarget S2)) (not (clear ?bMove S3)) (clear ?bMove S2))
)

(:action move-a1-s3-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (clear ?bMove S3) (clear ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S3)) (on ?bMove ?bTarget S4) (clear ?bSource S3) (not (clear ?bTarget S4)) (not (clear ?bMove S3)) (clear ?bMove S4))
)

(:action move-a1-s4-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (clear ?bMove S4) (clear ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S4)) (on ?bMove ?bTarget S3) (clear ?bSource S4) (not (clear ?bTarget S3)) (not (clear ?bMove S4)) (clear ?bMove S3))
)

(:action move-a1-s4-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (clear ?bMove S4) (clear ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S4)) (on ?bMove ?bTarget S5) (clear ?bSource S4) (not (clear ?bTarget S5)) (not (clear ?bMove S4)) (clear ?bMove S5))
)

(:action move-a1-s5-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (clear ?bMove S5) (clear ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S5)) (on ?bMove ?bTarget S4) (clear ?bSource S5) (not (clear ?bTarget S4)) (not (clear ?bMove S5)) (clear ?bMove S4))
)

(:action move-a1-s5-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (clear ?bMove S5) (clear ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S5)) (on ?bMove ?bTarget S6) (clear ?bSource S5) (not (clear ?bTarget S6)) (not (clear ?bMove S5)) (clear ?bMove S6))
)

(:action move-a1-s6-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (clear ?bMove S6) (clear ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S6)) (on ?bMove ?bTarget S5) (clear ?bSource S6) (not (clear ?bTarget S5)) (not (clear ?bMove S6)) (clear ?bMove S5))
)

(:action move-a2-s6-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (clear ?bMove S6) (clear ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S6)) (on ?bMove ?bTarget S7) (clear ?bSource S6) (not (clear ?bTarget S7)) (not (clear ?bMove S6)) (clear ?bMove S7))
)

(:action move-a2-s7-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (clear ?bMove S7) (clear ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S7)) (on ?bMove ?bTarget S6) (clear ?bSource S7) (not (clear ?bTarget S6)) (not (clear ?bMove S7)) (clear ?bMove S6))
)

(:action move-a2-s7-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (clear ?bMove S7) (clear ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S7)) (on ?bMove ?bTarget S8) (clear ?bSource S7) (not (clear ?bTarget S8)) (not (clear ?bMove S7)) (clear ?bMove S8))
)

(:action move-a2-s8-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (clear ?bMove S8) (clear ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S8)) (on ?bMove ?bTarget S7) (clear ?bSource S8) (not (clear ?bTarget S7)) (not (clear ?bMove S8)) (clear ?bMove S7))
)

(:action move-a2-s8-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (clear ?bMove S8) (clear ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S8)) (on ?bMove ?bTarget S9) (clear ?bSource S8) (not (clear ?bTarget S9)) (not (clear ?bMove S8)) (clear ?bMove S9))
)

(:action move-a2-s9-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (clear ?bMove S9) (clear ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S9)) (on ?bMove ?bTarget S8) (clear ?bSource S9) (not (clear ?bTarget S8)) (not (clear ?bMove S9)) (clear ?bMove S8))
)

(:action move-a3-s9-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (clear ?bMove S9) (clear ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S9)) (on ?bMove ?bTarget S10) (clear ?bSource S9) (not (clear ?bTarget S10)) (not (clear ?bMove S9)) (clear ?bMove S10))
)

(:action move-a3-s10-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (clear ?bMove S10) (clear ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S10)) (on ?bMove ?bTarget S9) (clear ?bSource S10) (not (clear ?bTarget S9)) (not (clear ?bMove S10)) (clear ?bMove S9))
)

(:action move-a3-s10-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (clear ?bMove S10) (clear ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S10)) (on ?bMove ?bTarget S11) (clear ?bSource S10) (not (clear ?bTarget S11)) (not (clear ?bMove S10)) (clear ?bMove S11))
)

(:action move-a3-s11-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (clear ?bMove S11) (clear ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S11)) (on ?bMove ?bTarget S10) (clear ?bSource S11) (not (clear ?bTarget S10)) (not (clear ?bMove S11)) (clear ?bMove S10))
)

(:action move-a3-s11-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (clear ?bMove S11) (clear ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S11)) (on ?bMove ?bTarget S12) (clear ?bSource S11) (not (clear ?bTarget S12)) (not (clear ?bMove S11)) (clear ?bMove S12))
)

(:action move-a3-s12-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (clear ?bMove S12) (clear ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S12)) (on ?bMove ?bTarget S11) (clear ?bSource S12) (not (clear ?bTarget S11)) (not (clear ?bMove S12)) (clear ?bMove S11))
)

)
