#!/bin/bash


TOPDIR=$(pwd)
for topdir in factored/*/; do
    cd $topdir;
    for probdir in */; do
        cd $probdir;
        echo $topdir/$probdir;
        ~/dev/libplan/third-party/translate/translate-factored.sh;
        cd ..
    done;
    cd $TOPDIR
done
