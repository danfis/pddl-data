#!/bin/bash

for domain in unfactored/*; do
    for prob in $domain/*.pddl; do
        if [ "$(basename $prob)" = "domain.pddl" ]; then
            continue
        fi
        prob=$(basename $prob)
        prob=${prob%.pddl}
        domain_name=${domain##*/}
        echo $domain $prob
        echo ./ma-to-pddl.py $domain domain $prob seq/$domain_name
        ./ma-to-pddl.py $domain domain $prob seq/$domain_name
    done
done
