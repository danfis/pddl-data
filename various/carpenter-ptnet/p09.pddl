; carpenter task with 42 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 s13 s14 - boardtype
    bench1 bench2 bench3 bench4 bench5 - bench
    stool1 stool2 stool3 stool4 stool5 - stool
  )
  (:init
    (boardsize board1 s14)
    (boardsize board2 s14)
    (boardsize board3 s14)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (boardsize-successor s8 s9)
    (boardsize-successor s9 s10)
    (boardsize-successor s10 s11)
    (boardsize-successor s11 s12)
    (boardsize-successor s12 s13)
    (boardsize-successor s13 s14)
    (= (available-colour-partsize natural small) 0)
    (= (available-colour-partsize natural medium) 0)
    (= (available-colour-partsize black small) 0)
    (= (available-colour-partsize black medium) 0)
    (= (available-colour-partsize white small) 0)
    (= (available-colour-partsize white medium) 0)
    (= (available-colour-partsize red small) 0)
    (= (available-colour-partsize red medium) 0)
    (= (available-colour-partsize green small) 0)
    (= (available-colour-partsize green medium) 0)
    (= (available-colour-partsize blue small) 0)
    (= (available-colour-partsize blue medium) 0)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished bench2 black)
      (finished bench3 red)
      (finished bench4 black)
      (finished bench5 green)
      (finished stool1 black)
      (finished stool2 white)
      (finished stool3 white)
      (finished stool4 black)
      (finished stool5 black)
    )
  )
)
