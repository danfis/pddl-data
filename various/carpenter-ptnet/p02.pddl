; carpenter task with 12 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 - boardtype
    bench1 - bench
    stool1 stool2 - stool
  )
  (:init
    (boardsize board1 s4)
    (boardsize board2 s4)
    (boardsize board3 s4)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (= (available-colour-partsize natural small) 0)
    (= (available-colour-partsize natural medium) 0)
    (= (available-colour-partsize black small) 0)
    (= (available-colour-partsize black medium) 0)
    (= (available-colour-partsize white small) 0)
    (= (available-colour-partsize white medium) 0)
    (= (available-colour-partsize red small) 0)
    (= (available-colour-partsize red medium) 0)
    (= (available-colour-partsize green small) 0)
    (= (available-colour-partsize green medium) 0)
    (= (available-colour-partsize blue small) 0)
    (= (available-colour-partsize blue medium) 0)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished stool1 black)
      (finished stool2 red)
    )
  )
)
