;; A modification of the Woodworking domain 
;; by Tomas de la Rosa

(define (domain carpenter)
  (:requirements :typing :fluents)
  (:types
      colortype awood woodobj  
      boardtype parttype - object
      board part - woodobj
      stool bench - finalproduct)

  (:constants
              natural - colortype
              small medium large - parttype)

  (:predicates 
            (boardsize ?board - board ?size - boardtype)
            (boardsize-successor ?size1 ?size2 - boardtype)
            (finished  ?product - finalproduct ?c - colortype)
	    )
   
  (:functions
           (available-colour-partsize ?c - colortype ?s - parttype)
   )

  (:action do-glaze
    :parameters (?newcolour - colortype ?z - parttype)
    :precondition (and
            (>= (available-colour-partsize natural ?z) 1)
            )
    :effect (and 
            (decrease (available-colour-partsize natural ?z) 1)
	    (increase (available-colour-partsize ?newcolour ?z) 1)
            ))

  
  (:action do-plane
    :parameters (?oldcolour - colortype ?z - parttype) 
    :precondition (and 
            (>= (available-colour-partsize ?oldcolour ?z) 1))
    :effect (and
            (decrease (available-colour-partsize ?oldcolour ?z) 1)
	    (increase (available-colour-partsize natural ?z) 1))
  )

  
  (:action do-saw-small
    :parameters (?b - board ?size_before ?size_after - boardtype) 
    :precondition (and 
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?size_before))
    :effect (and
            (not (boardsize ?b ?size_before))
            (boardsize ?b ?size_after)
            (increase (available-colour-partsize natural small) 1)
            ))

  (:action do-saw-medium
    :parameters (?b - board ?size_before ?s1 ?size_after - boardtype) 
    :precondition (and 
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?size_before))
    :effect (and
            (not (boardsize ?b ?size_before))
            (boardsize ?b ?size_after)
            (increase (available-colour-partsize natural medium) 1)
            ))

  
(:action build-stool
	 :parameters (?s - stool ?c - colortype)
	 :precondition (and
			(>= (available-colour-partsize ?c small) 3)
                        )
	 :effect (and   (finished ?s ?c)
		  	(decrease (available-colour-partsize ?c small) 3)
			;(decrease (available-partsize small) 3)
			;(decrease (available) 3) 
			
			))

(:action build-bench
	 :parameters (?b - bench ?c - colortype)
	 :precondition (and
                        
			(>= (available-colour-partsize ?c small) 2)
                        (>= (available-colour-partsize ?c medium) 1)
			)
	 :effect (and   (finished ?b ?c)
		  	(decrease (available-colour-partsize ?c small) 2)
                        (decrease (available-colour-partsize ?c medium) 1)

		  	;(decrease (available-partsize small) 2)
			;(decrease (available-partsize medium) 1)

			;(decrease (available 3)
			))
	 

)

