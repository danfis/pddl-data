; carpenter task with 34 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 - boardtype
    bench1 bench2 bench3 bench4 - bench
    stool1 stool2 stool3 stool4 - stool
  )
  (:init
    (boardsize board1 s11)
    (boardsize board2 s11)
    (boardsize board3 s11)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (boardsize-successor s8 s9)
    (boardsize-successor s9 s10)
    (boardsize-successor s10 s11)
    (= (available-colour-partsize natural small) 0)
    (= (available-colour-partsize natural medium) 0)
    (= (available-colour-partsize black small) 0)
    (= (available-colour-partsize black medium) 0)
    (= (available-colour-partsize white small) 0)
    (= (available-colour-partsize white medium) 0)
    (= (available-colour-partsize red small) 0)
    (= (available-colour-partsize red medium) 0)
    (= (available-colour-partsize green small) 0)
    (= (available-colour-partsize green medium) 0)
    (= (available-colour-partsize blue small) 0)
    (= (available-colour-partsize blue medium) 0)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished bench2 black)
      (finished bench3 red)
      (finished bench4 black)
      (finished stool1 green)
      (finished stool2 black)
      (finished stool3 white)
      (finished stool4 white)
    )
  )
)
