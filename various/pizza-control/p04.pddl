; pizza task with 2 trays and 7 guests
; needed slices 14 with constant factor of 1.2
; random seed: 101

(define (problem pizza-prob)
  (:domain pizza)
  (:objects
    one half quarter eighth sixteenth - size
    guest1 guest2 guest3 guest4 guest5 guest6 guest7 - person
    tray1 tray2 - tray
    slice1 slice2 slice3 slice4 slice5 slice6 slice7 slice8 slice9 slice10 slice11 slice12 slice13 slice14 slice15 slice16 - slice
  )
  (:init
     (undecidedsize)
     (freearms)
     (nextsize one half)
     (nextsize half quarter)
     (nextsize quarter eighth)
     (nextsize eighth sixteenth)
     (ontray slice1 tray1)
     (pizzasize slice1 one)
     (ontray slice2 tray2)
     (pizzasize slice2 one)
     (current-slice slice3)
     (next-slice slice3 slice4)
     (next-slice slice4 slice5)
     (next-slice slice5 slice6)
     (next-slice slice6 slice7)
     (next-slice slice7 slice8)
     (next-slice slice8 slice9)
     (next-slice slice9 slice10)
     (next-slice slice10 slice11)
     (next-slice slice11 slice12)
     (next-slice slice12 slice13)
     (next-slice slice13 slice14)
     (next-slice slice14 slice15)
     (next-slice slice15 slice16)
  )
  (:goal
    (and
     (served guest1)
     (served guest2)
     (served guest3)
     (served guest4)
     (served guest5)
     (served guest6)
     (served guest7)
    )
  )
)
