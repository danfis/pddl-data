; pizza task with 3 trays and 10 guests
; needed slices 21 with constant factor of 1.2
; random seed: 101

(define (problem pizza-prob)
  (:domain pizza)
  (:objects
    one half quarter eighth sixteenth - size
    guest1 guest2 guest3 guest4 guest5 guest6 guest7 guest8 guest9 guest10 - person
    tray1 tray2 tray3 - tray
    slice1 slice2 slice3 slice4 slice5 slice6 slice7 slice8 slice9 slice10 slice11 slice12 slice13 slice14 slice15 slice16 slice17 slice18 slice19 slice20 slice21 slice22 slice23 slice24 slice25 - slice
  )
  (:init
     (undecidedsize)
     (freearms)
     (nextsize one half)
     (nextsize half quarter)
     (nextsize quarter eighth)
     (nextsize eighth sixteenth)
     (ontray slice1 tray1)
     (pizzasize slice1 one)
     (ontray slice2 tray2)
     (pizzasize slice2 one)
     (ontray slice3 tray3)
     (pizzasize slice3 one)
     (current-slice slice4)
     (next-slice slice4 slice5)
     (next-slice slice5 slice6)
     (next-slice slice6 slice7)
     (next-slice slice7 slice8)
     (next-slice slice8 slice9)
     (next-slice slice9 slice10)
     (next-slice slice10 slice11)
     (next-slice slice11 slice12)
     (next-slice slice12 slice13)
     (next-slice slice13 slice14)
     (next-slice slice14 slice15)
     (next-slice slice15 slice16)
     (next-slice slice16 slice17)
     (next-slice slice17 slice18)
     (next-slice slice18 slice19)
     (next-slice slice19 slice20)
     (next-slice slice20 slice21)
     (next-slice slice21 slice22)
     (next-slice slice22 slice23)
     (next-slice slice23 slice24)
     (next-slice slice24 slice25)
  )
  (:goal
    (and
     (served guest1)
     (served guest2)
     (served guest3)
     (served guest4)
     (served guest5)
     (served guest6)
     (served guest7)
     (served guest8)
     (served guest9)
     (served guest10)
    )
  )
)
