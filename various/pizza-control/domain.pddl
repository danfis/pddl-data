(define (domain pizza)
(:requirements :typing :equality)
(:types slice person tray size)

(:predicates (ontray ?x - slice ?y - tray)
	     (holding ?x - slice ?y - tray)
	     (served ?x - person)
	     (inmind ?x - slice)
	     (pizzasize ?x - slice ?z - size)
             (nextsize ?x - size ?y - size)
	     (servedsize ?x - size)
	     (undecidedsize)
	     (freearms)

             (current-slice ?s - slice)
	     (next-slice ?s1 - slice ?s2 - slice)
	   )

(:action hold
  :parameters (?x - slice ?y - tray)
  :precondition (and (ontray ?x ?y) (freearms))
  :effect (and (not (ontray ?x ?y)) (holding ?x ?y) (not (freearms))))


(:action leave
  :parameters (?x - slice ?y - tray)
  :precondition (and (holding ?x ?y))
  :effect (and (ontray ?x ?y) (not (holding ?x ?y)) (freearms)))



(:action cut
  :parameters (?s ?half1 ?half2 ?other - slice ?t - tray ?z1 - size ?zhalf - size)
  :precondition (and (holding ?s ?t) (pizzasize ?s ?z1)
		     (nextsize ?z1 ?zhalf)
                     (current-slice ?half1)
		     (next-slice ?half1 ?half2)
                     (next-slice ?half2 ?other))
  :effect (and (not (holding ?s ?t)) (freearms)
               (not (current-slice ?half1)) (current-slice ?other)
	       (ontray ?half1 ?t) (ontray ?half2 ?t)
               (pizzasize ?half1 ?zhalf) (pizzasize ?half2 ?zhalf)))


(:action first-serve
  :parameters (?s - slice ?p - person ?t - tray ?z - size)
  :precondition (and (ontray ?s ?t) (pizzasize ?s ?z)
		     (freearms) (undecidedsize))
  :effect (and (not (ontray ?s ?t))
	       (served ?p)
               (not (undecidedsize))
               (servedsize ?z)))


(:action serve
  :parameters (?s - slice ?p - person ?t - tray ?z - size)
  :precondition (and (ontray ?s ?t) (pizzasize ?s ?z) 
                     (freearms) (servedsize ?z))
  :effect (and (not (ontray ?s ?t))
	       (served ?p)))


)

