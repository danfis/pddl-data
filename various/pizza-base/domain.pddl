(define (domain pizza)
(:requirements :typing :equality)
(:types slice person tray size)

(:predicates (ontray ?x - slice ?y - tray)
	     (holding ?x - slice ?y - tray)
	     (served ?x - person)
	     (inmind ?x - slice)
	     (pizzasize ?x - slice ?z - size)
             (nextsize ?x - size ?y - size)
	     (servedsize ?x - size)
	     (undecidedsize)
	     (freearms)

             (free ?s - slice)
	   )

(:action hold
  :parameters (?x - slice ?y - tray)
  :precondition (and (ontray ?x ?y) (freearms))
  :effect (and (not (ontray ?x ?y)) (holding ?x ?y) (not (freearms))))


(:action leave
  :parameters (?x - slice ?y - tray)
  :precondition (and (holding ?x ?y))
  :effect (and (ontray ?x ?y) (not (holding ?x ?y)) (freearms)))



(:action cut
  :parameters (?s - slice ?half1 - slice ?half2 - slice ?t - tray ?z1 - size ?zhalf - size)
  :precondition (and (not (= ?half1 ?half2))
		     (holding ?s ?t) (pizzasize ?s ?z1)
		     (nextsize ?z1 ?zhalf)
                     (free ?half1) (free ?half2))
  :effect (and (not (holding ?s ?t)) (freearms)
               (not (free ?half1)) (not (free ?half2))
	       (ontray ?half1 ?t) (ontray ?half2 ?t)
               (pizzasize ?half1 ?zhalf) (pizzasize ?half2 ?zhalf)))


(:action first-serve
  :parameters (?s - slice ?p - person ?t - tray ?z - size)
  :precondition (and (ontray ?s ?t) (pizzasize ?s ?z)
		     (freearms) (undecidedsize))
  :effect (and (not (ontray ?s ?t))
	       (served ?p)
               (not (undecidedsize))
               (servedsize ?z)))


(:action serve
  :parameters (?s - slice ?p - person ?t - tray ?z - size)
  :precondition (and (ontray ?s ?t) (pizzasize ?s ?z) 
                     (freearms) (servedsize ?z))
  :effect (and (not (ontray ?s ?t))
	       (served ?p)))


)

