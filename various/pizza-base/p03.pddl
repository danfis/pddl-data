; pizza task with 2 trays and 6 guests
; needed slices 14 with constant factor of 1.2
; random seed: 101

(define (problem pizza-prob)
  (:domain pizza)
  (:objects
    one half quarter eighth sixteenth - size
    guest1 guest2 guest3 guest4 guest5 guest6 - person
    tray1 tray2 - tray
    slice1 slice2 slice3 slice4 slice5 slice6 slice7 slice8 slice9 slice10 slice11 slice12 slice13 slice14 slice15 slice16 - slice
  )
  (:init
     (undecidedsize)
     (freearms)
     (nextsize one half)
     (nextsize half quarter)
     (nextsize quarter eighth)
     (nextsize eighth sixteenth)
     (ontray slice1 tray1)
     (pizzasize slice1 one)
     (ontray slice2 tray2)
     (pizzasize slice2 one)
     (free slice3)
     (free slice4)
     (free slice5)
     (free slice6)
     (free slice7)
     (free slice8)
     (free slice9)
     (free slice10)
     (free slice11)
     (free slice12)
     (free slice13)
     (free slice14)
     (free slice15)
     (free slice16)
  )
  (:goal
    (and
     (served guest1)
     (served guest2)
     (served guest3)
     (served guest4)
     (served guest5)
     (served guest6)
    )
  )
)
