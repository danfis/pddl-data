; pizza task with 3 trays and 9 guests
; needed slices 21 with constant factor of 1.2
; random seed: 101

(define (problem pizza-prob)
  (:domain pizza)
  (:objects
    one half quarter eighth sixteenth - size
    guest1 guest2 guest3 guest4 guest5 guest6 guest7 guest8 guest9 - person
    tray1 tray2 tray3 - tray
    slice1 slice2 slice3 slice4 slice5 slice6 slice7 slice8 slice9 slice10 slice11 slice12 slice13 slice14 slice15 slice16 slice17 slice18 slice19 slice20 slice21 slice22 slice23 slice24 slice25 - slice
  )
  (:init
     (undecidedsize)
     (freearms)
     (nextsize one half)
     (nextsize half quarter)
     (nextsize quarter eighth)
     (nextsize eighth sixteenth)
     (ontray slice1 tray1)
     (pizzasize slice1 one)
     (ontray slice2 tray2)
     (pizzasize slice2 one)
     (ontray slice3 tray3)
     (pizzasize slice3 one)
     (free slice4)
     (free slice5)
     (free slice6)
     (free slice7)
     (free slice8)
     (free slice9)
     (free slice10)
     (free slice11)
     (free slice12)
     (free slice13)
     (free slice14)
     (free slice15)
     (free slice16)
     (free slice17)
     (free slice18)
     (free slice19)
     (free slice20)
     (free slice21)
     (free slice22)
     (free slice23)
     (free slice24)
     (free slice25)
  )
  (:goal
    (and
     (served guest1)
     (served guest2)
     (served guest3)
     (served guest4)
     (served guest5)
     (served guest6)
     (served guest7)
     (served guest8)
     (served guest9)
    )
  )
)
