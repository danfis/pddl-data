#!/usr/bin/env python

from math import ceil
import random
import sys

class Task(object):
  def __init__(self, seed,  n_trays, n_guests, domain_type="base", const_factor=1.2):
    self.type = domain_type
    self.seed = seed 
    self.n_trays = n_trays
    self.n_guests = n_guests
    self.const_factor = const_factor

    needed_guest_per_tray = int(ceil(float(self.n_guests)/float(self.n_trays)))

    self.needed_slices = needed_slices(needed_guest_per_tray) * self.n_trays
    self.n_slices = int(self.needed_slices * self.const_factor)

    self.slices = enum_objects("slice", self.n_slices)
    self.trays = enum_objects("tray", self.n_trays)
    self.guests = enum_objects("guest", self.n_guests)
    

  def dump(self, out=None):
    self._dump_header(out)
    self._dump_objects("  ", out)
    self._dump_init("  ", out)
    self._dump_goal("  ", out)
    #self._dump_metric("  ", out)
    print >> out, ")"

  def _dump_header(self, out=None):
    print >> out, "; pizza task with %s trays and %d guests" % (self.n_trays, self.n_guests)
    print >> out, "; needed slices %s with constant factor of %s" % (self.needed_slices, self.const_factor)
    print >> out, "; random seed: %d" % self.seed
    print >> out
    print >> out, "(define (problem pizza-prob)"
    print >> out, "  (:domain pizza)"

  def _dump_objects(self, indent="", out=None):
    print >> out, indent + "(:objects"
    print >> out, "%s  one half quarter eighth sixteenth - size" % indent
    print >> out, "%s  %s - person" % (indent, " ".join(self.guests))
    print >> out, "%s  %s - tray" % (indent, " ".join(self.trays))
    
    if self.type != "ptnet":
      print >> out, "%s  %s - slice" % (indent, " ".join(self.slices))
    else:
      print >> out, "%s  %s - slice" % (indent, " ".join(self.slices[:self.n_trays]))
    
    print >> out, indent + ")"

  def _dump_init(self, indent="", out=None):
    print >> out, indent + "(:init"

    print >> out, indent + "%s (undecidedsize)" % indent
    print >> out, indent + "%s (freearms)" % indent
    print >> out, indent + "%s (nextsize one half)" % indent
    print >> out, indent + "%s (nextsize half quarter)" % indent
    print >> out, indent + "%s (nextsize quarter eighth)" % indent
    print >> out, indent + "%s (nextsize eighth sixteenth)" % indent

    if self.type != "ptnet":
      for islice, i in zip(self.slices[:self.n_trays],range(1,self.n_trays+1)):
        print >> out, indent + "%s (ontray %s tray%s)" % (indent, islice, i)
        print >> out, indent + "%s (pizzasize %s one)" % (indent, islice)
    else:
      for tray in self.trays:
        print >> out, indent + "%s (= (ontray_pizzasize %s one) 1)" % (indent, tray)
        print >> out, indent + "%s (= (holding_pizzasize %s one) 0)" % (indent, tray)
      for size in ["half","quarter","eighth","sixteenth"]:
        for tray in self.trays:
          print >> out, indent + "%s (= (ontray_pizzasize %s %s) 0)" % (indent, tray, size)
          print >> out, indent + "%s (= (holding_pizzasize %s %s) 0)" % (indent, tray, size)
    
  
    if self.type == "base":
      for islice in self.slices[self.n_trays:]:
        print >> out, indent + "%s (free %s)" % (indent, islice)
      
    elif self.type == "control":
      print >> out, "%s   (current-slice %s)" % (indent , self.slices[self.n_trays])
      rest_slices = self.slices[self.n_trays:]
      for i in range(1, len(rest_slices)):
        print >> out, "%s   (next-slice %s %s)" % (indent, rest_slices[i-1], rest_slices[i])

    print >> out, indent + ")"

  def _dump_goal(self, indent="", out=None):
    print >> out, indent + "(:goal"
    print >> out, indent + "  (and"
    for g in self.guests:
      print >> out, indent + "%s (served %s)" % (indent, g)
    print >> out, indent + "  )"
    print >> out, indent + ")"

  def _dump_metric(self, indent="", out=None):
    print >> out, indent + "(:metric minimize (total-cost))"
   
def reseed():
  new_seed = random.randrange(10 ** 6)
  random.seed(new_seed)
  return new_seed


def enum_objects(pddltype, n):
  return ["%s%s" % (pddltype,i) for i in range(1,n+1)]

   
def needed_slices (n):
  
  res = 0
  for i in range(n):
    parts = pow(2,i)
    res += parts
    if (parts >=n):
      #print ">>>>>", n, res
      return res






if __name__ == "__main__":

#  try:
    domain_type = sys.argv[1]
    seed = int(sys.argv[2])
    n_trays = int(sys.argv[3])
    n_guests = int(sys.argv[4])
    const_factor = float(sys.argv[5])
    
    random.seed(seed)
    pddltask = Task(seed, n_trays, n_guests, domain_type, const_factor)
    pddltask.dump()

    
#  except:
#    print "\n Incorrect input arguments "
#    print "\n Usage: " +sys.argv[0] + " [base | control  ptnet] <seed> <num_trays> <num_guests> <slice_factor>"
#    print "\n    <slice_factor>: min 1.0"
#    sys.exit(1)


