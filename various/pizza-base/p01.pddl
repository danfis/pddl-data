; pizza task with 1 trays and 4 guests
; needed slices 7 with constant factor of 1.2
; random seed: 101

(define (problem pizza-prob)
  (:domain pizza)
  (:objects
    one half quarter eighth sixteenth - size
    guest1 guest2 guest3 guest4 - person
    tray1 - tray
    slice1 slice2 slice3 slice4 slice5 slice6 slice7 slice8 - slice
  )
  (:init
     (undecidedsize)
     (freearms)
     (nextsize one half)
     (nextsize half quarter)
     (nextsize quarter eighth)
     (nextsize eighth sixteenth)
     (ontray slice1 tray1)
     (pizzasize slice1 one)
     (free slice2)
     (free slice3)
     (free slice4)
     (free slice5)
     (free slice6)
     (free slice7)
     (free slice8)
  )
  (:goal
    (and
     (served guest1)
     (served guest2)
     (served guest3)
     (served guest4)
    )
  )
)
