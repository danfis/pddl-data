; carpenter task with 12 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 - boardtype
    bench1 - bench
    stool1 stool2 - stool
  )
  (:init
    (boardsize board1 s4)
    (boardsize board2 s4)
    (boardsize board3 s4)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (unused p1)
    (unused p2)
    (unused p3)
    (unused p4)
    (unused p5)
    (unused p6)
    (unused p7)
    (unused p8)
    (unused p9)
    (unused p10)
    (unused p11)
    (unused p12)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished stool1 black)
      (finished stool2 red)
    )
  )
)
