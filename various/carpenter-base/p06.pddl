; carpenter task with 29 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27 p28 p29 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 - boardtype
    bench1 bench2 bench3 - bench
    stool1 stool2 stool3 stool4 - stool
  )
  (:init
    (boardsize board1 s9)
    (boardsize board2 s9)
    (boardsize board3 s9)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (boardsize-successor s8 s9)
    (unused p1)
    (unused p2)
    (unused p3)
    (unused p4)
    (unused p5)
    (unused p6)
    (unused p7)
    (unused p8)
    (unused p9)
    (unused p10)
    (unused p11)
    (unused p12)
    (unused p13)
    (unused p14)
    (unused p15)
    (unused p16)
    (unused p17)
    (unused p18)
    (unused p19)
    (unused p20)
    (unused p21)
    (unused p22)
    (unused p23)
    (unused p24)
    (unused p25)
    (unused p26)
    (unused p27)
    (unused p28)
    (unused p29)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished bench2 black)
      (finished bench3 red)
      (finished stool1 black)
      (finished stool2 green)
      (finished stool3 black)
      (finished stool4 white)
    )
  )
)
