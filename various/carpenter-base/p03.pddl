; carpenter task with 17 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 s5 - boardtype
    bench1 bench2 - bench
    stool1 stool2 - stool
  )
  (:init
    (boardsize board1 s5)
    (boardsize board2 s5)
    (boardsize board3 s5)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (unused p1)
    (unused p2)
    (unused p3)
    (unused p4)
    (unused p5)
    (unused p6)
    (unused p7)
    (unused p8)
    (unused p9)
    (unused p10)
    (unused p11)
    (unused p12)
    (unused p13)
    (unused p14)
    (unused p15)
    (unused p16)
    (unused p17)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished bench2 black)
      (finished stool1 red)
      (finished stool2 black)
    )
  )
)
