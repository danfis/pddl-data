; carpenter task with 9 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 - boardtype
    bench1 - bench
    stool1 - stool
  )
  (:init
    (boardsize board1 s3)
    (boardsize board2 s3)
    (boardsize board3 s3)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (unused p1)
    (unused p2)
    (unused p3)
    (unused p4)
    (unused p5)
    (unused p6)
    (unused p7)
    (unused p8)
    (unused p9)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished stool1 black)
    )
  )
)
