; carpenter task with 29 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27 p28 p29 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 - boardtype
    bench1 bench2 bench3 - bench
    stool1 stool2 stool3 stool4 - stool
  )
  (:init
    (boardsize board1 s9)
    (boardsize board2 s9)
    (boardsize board3 s9)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (boardsize-successor s8 s9)
    (current-part p1)
    (next-part p1 p2)
    (next-part p2 p3)
    (next-part p3 p4)
    (next-part p4 p5)
    (next-part p5 p6)
    (next-part p6 p7)
    (next-part p7 p8)
    (next-part p8 p9)
    (next-part p9 p10)
    (next-part p10 p11)
    (next-part p11 p12)
    (next-part p12 p13)
    (next-part p13 p14)
    (next-part p14 p15)
    (next-part p15 p16)
    (next-part p16 p17)
    (next-part p17 p18)
    (next-part p18 p19)
    (next-part p19 p20)
    (next-part p20 p21)
    (next-part p21 p22)
    (next-part p22 p23)
    (next-part p23 p24)
    (next-part p24 p25)
    (next-part p25 p26)
    (next-part p26 p27)
    (next-part p27 p28)
    (next-part p28 p29)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished bench2 black)
      (finished bench3 red)
      (finished stool1 black)
      (finished stool2 green)
      (finished stool3 black)
      (finished stool4 white)
    )
  )
)
