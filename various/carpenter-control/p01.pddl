; carpenter task with 9 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 - boardtype
    bench1 - bench
    stool1 - stool
  )
  (:init
    (boardsize board1 s3)
    (boardsize board2 s3)
    (boardsize board3 s3)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (current-part p1)
    (next-part p1 p2)
    (next-part p2 p3)
    (next-part p3 p4)
    (next-part p4 p5)
    (next-part p5 p6)
    (next-part p6 p7)
    (next-part p7 p8)
    (next-part p8 p9)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished stool1 black)
    )
  )
)
