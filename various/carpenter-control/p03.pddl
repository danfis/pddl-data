; carpenter task with 17 parts and 120% wood
; random seed: 581152

(define (problem wood-prob)
  (:domain carpenter)
  (:objects
    black white red green blue - colortype
    p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 - part
    board1 board2 board3 - board
    s0 s1 s2 s3 s4 s5 - boardtype
    bench1 bench2 - bench
    stool1 stool2 - stool
  )
  (:init
    (boardsize board1 s5)
    (boardsize board2 s5)
    (boardsize board3 s5)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (current-part p1)
    (next-part p1 p2)
    (next-part p2 p3)
    (next-part p3 p4)
    (next-part p4 p5)
    (next-part p5 p6)
    (next-part p6 p7)
    (next-part p7 p8)
    (next-part p8 p9)
    (next-part p9 p10)
    (next-part p10 p11)
    (next-part p11 p12)
    (next-part p12 p13)
    (next-part p13 p14)
    (next-part p14 p15)
    (next-part p15 p16)
    (next-part p16 p17)
  )
  (:goal
    (and
      (finished bench1 red)
      (finished bench2 black)
      (finished stool1 red)
      (finished stool2 black)
    )
  )
)
