;; A modification of the Woodworking domain 
;; by Tomas de la Rosa

(define (domain carpenter)
  (:requirements :typing)
  (:types
      colortype awood woodobj  
      boardtype parttype - object
      board part - woodobj
      stool bench - finalproduct)

  (:constants
              natural - colortype
              small medium large - parttype)

  (:predicates 

            (available ?obj - part)
            (colour ?obj - part ?colour - colortype)
            (boardsize ?board - board ?size - boardtype)
            (partsize ?part - part ?size - parttype)
            (boardsize-successor ?size1 ?size2 - boardtype)
  
            (finished  ?product - finalproduct ?c - colortype)

	    (current-part ?p - part)
	    (next-part ?p - part ?nextp - part)
	    )
   

  (:action do-glaze
    :parameters (?x - part ?newcolour - colortype)
    :precondition (and
            (available ?x)
             )
    :effect (and 
            (not (colour ?x natural))
            (colour ?x ?newcolour)))

  
  (:action do-plane
    :parameters (?x - part ?oldcolour - colortype) 
    :precondition (and 
            (available ?x)
            (colour ?x ?oldcolour))
    :effect (and
            (not (colour ?x ?oldcolour))
            (colour ?x natural)))


  (:action do-saw-small
    :parameters (?b - board ?p - part ?nextp - part  ?size_before ?size_after - boardtype) 
    :precondition (and 
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?size_before)
            (next-part ?p ?nextp)
	    (current-part ?p)
            )
    :effect (and
            
            (available ?p)
            (colour ?p natural) 

            (partsize ?p small)
            (not (boardsize ?b ?size_before))
            (boardsize ?b ?size_after)
            (not (current-part ?p))
	    (current-part ?nextp)
            ))

  (:action do-saw-medium
    :parameters (?b - board ?p ?nextp - part ?size_before ?s1 ?size_after - boardtype) 
    :precondition (and 
            (boardsize ?b ?size_before)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?size_before)
            (next-part ?p ?nextp)
	    (current-part ?p)
            )
    :effect (and
            
            (available ?p)
            (colour ?p natural) 

            (partsize ?p medium) 
            (not (boardsize ?b ?size_before))
            (boardsize ?b ?size_after)
            (not (current-part ?p))
	    (current-part ?nextp))
)

  
(:action build-stool
	 :parameters (?s - stool ?c - colortype ?p1 ?p2 ?p3 - part)
	 :precondition (and
			(not (= ?p1 ?p2))
			(not (= ?p1 ?p3))
			(not (= ?p2 ?p3))	
			(available ?p1) (partsize ?p1 small) (colour ?p1 ?c)
			(available ?p2) (partsize ?p2 small) (colour ?p2 ?c)
			(available ?p3) (partsize ?p3 small) (colour ?p3 ?c)
                        )
	 :effect (and
		  	(not (available ?p1))
			(not (available ?p2))
			(not (available ?p3))
			(finished ?s ?c)
			))

(:action build-bench
	 :parameters (?b - bench ?c - colortype ?p1 ?p2 ?p3 - part)
	 :precondition (and
                        (not (= ?p1 ?p3))
			(available ?p1) (partsize ?p1 small) (colour ?p1 ?c)
			(available ?p2) (partsize ?p2 medium) (colour ?p2 ?c)
			(available ?p3) (partsize ?p3 small) (colour ?p3 ?c))
	 :effect (and
		  	(not (available ?p1))
			(not (available ?p2))
			(not (available ?p3))
			(finished ?b ?c)
			))
	 

)

