(define (domain pizza)
(:requirements :typing :equality)
(:types slice person tray size)

(:predicates 
	     (holding ?y - tray)
	     (served ?x - person)
	     (nextsize ?x - size ?y - size)
	     (servedsize ?x - size)
	     (undecidedsize)
	     (freearms)
	   )
(:functions (ontray_pizzasize ?t - tray ?z - size)
            (holding_pizzasize ?t - tray ?z - size)
	    )

(:action hold
  :parameters (?t - tray ?z - size)
  :precondition (and (freearms)
                     (>= (ontray_pizzasize ?t ?z) 1))
                      
  :effect (and (not (freearms))
	       (decrease (ontray_pizzasize ?t ?z) 1)
	       (increase (holding_pizzasize ?t ?z) 1)))


(:action leave
  :parameters (?t - tray ?z - size)
  :precondition (and (>= (holding_pizzasize ?t ?z) 1))
  :effect (and (freearms)
               (increase (ontray_pizzasize ?t ?z) 1)
	       (decrease (holding_pizzasize ?t ?z) 1)))


(:action cut
  :parameters (?t - tray ?z1 - size ?zhalf - size)
  :precondition (and (>= (holding_pizzasize ?t ?z1) 1)
		     (nextsize ?z1 ?zhalf))
  :effect (and (freearms)
               (decrease (holding_pizzasize ?t ?z1) 1)
	       (increase (ontray_pizzasize ?t ?zhalf) 2)
               ))


(:action first-serve
  :parameters (?p - person ?t - tray ?z - size)
  :precondition (and (>= (ontray_pizzasize ?t ?z) 1)
		     (freearms) (undecidedsize))
  :effect (and (served ?p)
               (not (undecidedsize))
               (servedsize ?z)
               (decrease (ontray_pizzasize ?t ?z) 1)
               ))


(:action serve
  :parameters (?p - person ?t - tray ?z - size)
  :precondition (and (>= (ontray_pizzasize ?t ?z) 1)
                     (freearms) (servedsize ?z))
  :effect (and (served ?p)
               (decrease (ontray_pizzasize ?t ?z) 1)
      ))


)

