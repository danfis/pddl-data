; pizza task with 2 trays and 5 guests
; needed slices 14 with constant factor of 1.2
; random seed: 101

(define (problem pizza-prob)
  (:domain pizza)
  (:objects
    one half quarter eighth sixteenth - size
    guest1 guest2 guest3 guest4 guest5 - person
    tray1 tray2 - tray
    slice1 slice2 - slice
  )
  (:init
     (undecidedsize)
     (freearms)
     (nextsize one half)
     (nextsize half quarter)
     (nextsize quarter eighth)
     (nextsize eighth sixteenth)
     (= (ontray_pizzasize tray1 one) 1)
     (= (holding_pizzasize tray1 one) 0)
     (= (ontray_pizzasize tray2 one) 1)
     (= (holding_pizzasize tray2 one) 0)
     (= (ontray_pizzasize tray1 half) 0)
     (= (holding_pizzasize tray1 half) 0)
     (= (ontray_pizzasize tray2 half) 0)
     (= (holding_pizzasize tray2 half) 0)
     (= (ontray_pizzasize tray1 quarter) 0)
     (= (holding_pizzasize tray1 quarter) 0)
     (= (ontray_pizzasize tray2 quarter) 0)
     (= (holding_pizzasize tray2 quarter) 0)
     (= (ontray_pizzasize tray1 eighth) 0)
     (= (holding_pizzasize tray1 eighth) 0)
     (= (ontray_pizzasize tray2 eighth) 0)
     (= (holding_pizzasize tray2 eighth) 0)
     (= (ontray_pizzasize tray1 sixteenth) 0)
     (= (holding_pizzasize tray1 sixteenth) 0)
     (= (ontray_pizzasize tray2 sixteenth) 0)
     (= (holding_pizzasize tray2 sixteenth) 0)
  )
  (:goal
    (and
     (served guest1)
     (served guest2)
     (served guest3)
     (served guest4)
     (served guest5)
    )
  )
)
