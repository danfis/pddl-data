#!/usr/bin/env python3

import sys
import time
from pddldata import *

def main(dirs, cmd):
    for d in dirs:
        for prob in problemsIt(d):
            if prob.domain.has_fluents:
                continue
            c = cmd.replace('DOMAIN', prob.domain_pddl)
            c = c.replace('NAME', prob.name)
            c = c.replace('DIR', prob.domain.path)
            c = c.replace('PROBLEM', prob.problem_pddl)
            print(c)
            ret = os.system(c)
            sig = ret & 0xff
            if sig != 0:
                sys.exit(-1)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: {0} dir [dir ...] command'.format(sys.argv[0]))
        print('')
        print('In command:')
        print('\tDOMAIN is replaced with pddl domain file')
        print('\tPROBLEM is replaced with pddl problem file')
        print('\tDIR is replaced with the directory where the pddl files are')
        print('\tNAME is replaced with problem name')
        sys.exit(-1)
    main(sys.argv[1:-1], sys.argv[-1])
