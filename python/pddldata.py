import sys
import os
import hashlib
import re

def findDuplicates(paths):
    hash_files = {}
    for path in paths:
        for dirpath, dirnames, filenames in os.walk(path):
            for filename in filenames:
                full_path = os.path.join(dirpath, filename)
                with open(full_path, 'rb') as fin:
                    h = hashlib.sha224(fin.read()).hexdigest()
                    if h in hash_files:
                        hash_files[h] += [full_path]
                    else:
                        hash_files[h] = [full_path]

    duplicates = []
    for key, full_paths in hash_files.items():
        if len(full_paths) > 1:
            duplicates += [sorted(full_paths)]
    duplicates = sorted(duplicates)

    for dup in duplicates:
        print(' '.join(dup))


re_plan_optimal_cost = re.compile('^.*[Oo]ptimal [Cc]ost:.*$')
class Plan(object):
    def __init__(self, path):
        self.path = path
        self.cost = None
        self.plan = []
        with open(path, 'r') as fin:
            for line in fin:
                line = line.strip()
                if len(line) == 0:
                    continue
                elif re_plan_optimal_cost.match(line) is not None:
                    self.cost = int(line.split()[-1])
                else:
                    self.plan += [line]


class Domain(object):
    def __init__(self, path):
        self.path = path
        self.name = path.split('/')[-1].lower()

        self.problems = self._loadProblems()

        self.has_fluents = False
        if os.path.isfile(os.path.join(path, 'fluents')):
            self.has_fluents = True

    def _loadProblems(self):
        files = os.listdir(self.path)
        files = filter(lambda x: x.find('domain') == -1, files)
        pddls = filter(lambda x: x[-5:] == '.pddl', files)
        names = sorted([x[:-5] for x in pddls])

        return [Problem(self, x) for x in names]

class Problem(object):
    def __init__(self, domain, name):
        self.domain = domain
        self.name = name
        self.domain_pddl = self._domainPddl(name)
        self.problem_pddl = os.path.join(domain.path, name + '.pddl')

        self.plan = None
        plan = os.path.join(domain.path, name + '.plan')
        if os.path.isfile(plan):
            self.plan = Plan(plan)

        self.problem_addl = os.path.join(domain.path, name + '.addl')
        if not os.path.isfile(self.problem_addl):
            self.problem_addl = None

    def _domainPddl(self, n):
        d = self.domain.path
        domain = os.path.join(d, 'domain_' + n + '.pddl')
        if os.path.isfile(domain):
            return domain

        if n.find('problem') != -1:
            domain = os.path.join(d, n.replace('problem', 'domain') + '.pddl')
            if os.path.isfile(domain):
                return domain

        domain = os.path.join(d, n + '-domain.pddl')
        if os.path.isfile(domain):
            return domain

        domain = os.path.join(d, 'domain-' + n + '.pddl')
        if os.path.isfile(domain):
            return domain

        domain = os.path.join(d, 'domain.pddl')
        if os.path.isfile(domain):
            return domain

        ps = []
        for p in n.split('-'):
            ps += [p]
            domain = os.path.join(d, '-'.join(ps) + '-domain.pddl')
            if os.path.isfile(domain):
                return domain

        raise Exception('Cannot find domain pddl file for `{}\' in `{}\'' \
                            .format(name, self.domain.name))


def domainsIt(topdir):
    domains = []
    for root, dirs, files in os.walk(topdir):
        dom_pddls = [x for x in files if x.find('domain') != -1]
        if len(dom_pddls) > 0:
            domains += [os.path.abspath(root)]
    domains = sorted(domains)
    for d in domains:
        yield Domain(d)

def problemsIt(topdir):
    for domain in domainsIt(topdir):
        for prob in domain.problems:
            yield prob
