domains = [
{'name': 'muddy-child',
 'problems': [('muddy-child/domain_3-muddy_1.pddl', 'muddy-child/3-muddy_1.pddl'),
              ('muddy-child/domain_4-muddy_1.pddl', 'muddy-child/4-muddy_1.pddl'),
              ('muddy-child/domain_5-muddy_1.pddl', 'muddy-child/5-muddy_1.pddl'),
              ('muddy-child/domain_5-muddy_2.pddl', 'muddy-child/5-muddy_2.pddl'),
              ('muddy-child/domain_6-muddy_1.pddl', 'muddy-child/6-muddy_1.pddl'),
              ('muddy-child/domain_6-muddy_2.pddl', 'muddy-child/6-muddy_2.pddl'),
              ('muddy-child/domain_7-muddy_2.pddl', 'muddy-child/7-muddy_2.pddl')]}
]
