domains = [
{'name': 'ghosh-etal-JAR-acc-cc2',
 'problems': [('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.badgoal1.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.badgoal2.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.badgoal3.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.badgoal4.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.badgoal5.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.goodgoal6.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.goodgoal7.cc2.pddl'),
              ('ghosh-etal-JAR-acc-cc2/domain.pddl', 'ghosh-etal-JAR-acc-cc2/acc.goodgoal8.cc2.pddl')]}
]
