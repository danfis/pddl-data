domains = [
{'name': 'blocker',
 'problems': [('blocker/domain.pddl', 'blocker/blocker-strips-12.pddl'),
              ('blocker/domain.pddl', 'blocker/blocker-strips-small.pddl'),
              ('blocker/domain.pddl', 'blocker/bm2.pddl'),
              ('blocker/domain.pddl', 'blocker/bm3.pddl'),
              ('blocker/domain.pddl', 'blocker/bm4.pddl'),
              ('blocker/domain.pddl', 'blocker/bm5.pddl'),
              ('blocker/domain.pddl', 'blocker/bm6.pddl'),
              ('blocker/domain.pddl', 'blocker/bm7.pddl'),
              ('blocker/domain.pddl', 'blocker/bm8.pddl')]}
]
