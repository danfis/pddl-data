domains = [
{'name': 'grid-axioms',
 'problems': [('grid-axioms/domain.pddl', 'grid-axioms/prob01.pddl'),
              ('grid-axioms/domain.pddl', 'grid-axioms/prob02.pddl'),
              ('grid-axioms/domain.pddl', 'grid-axioms/prob03.pddl'),
              ('grid-axioms/domain.pddl', 'grid-axioms/prob04.pddl'),
              ('grid-axioms/domain.pddl', 'grid-axioms/prob05.pddl')]}
]
