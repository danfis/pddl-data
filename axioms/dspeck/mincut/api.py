domains = [
{'name': 'mincut',
 'problems': [('mincut/domain.pddl', 'mincut/mc-12-00.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-01.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-02.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-03.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-04.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-05.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-06.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-07.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-08.pddl'),
              ('mincut/domain.pddl', 'mincut/mc-12-09.pddl')]}
]
