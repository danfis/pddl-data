domains = [
{'name': 'ghosh-etal-JAR-acc-cc1',
 'problems': [('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.badgoal1.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.badgoal2.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.badgoal3.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.badgoal4.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.badgoal5.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.goodgoal6.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.goodgoal7.cc1.pddl'),
              ('ghosh-etal-JAR-acc-cc1/domain.pddl', 'ghosh-etal-JAR-acc-cc1/acc.goodgoal8.cc1.pddl')]}
]
