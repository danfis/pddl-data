The gen-* tasks were generated with the following script:
```
#!/bin/bash

ID=1
while [ $ID -le 30 ]; do
    n=$((1 + $RANDOM % 6))
    d=$((1 + $RANDOM % 3))
    o=$((1 + $RANDOM % 3))
    s=$((3 + $RANDOM % 10))
    c=$((3 + $RANDOM % 10))
    dst=gen/gen-p$(printf "%02d" $ID).pddl
    if ! ./storage -p 1 -n $n -d $d -o $o -s $s -c $c -e 1 $dst; then
        continue
    fi
    mv $dst tmp
    echo ";; ./storage -p 1 -n $n -d $d -o $o -s $s -c $c -e 1 $dst" >$dst
    cat tmp >>$dst

    echo "Generated $dst"

    if ! timeout 30s ~/dev/cpddl/bin/pddl
~/dev/pddl-data/ipc-opt-noce/storage06/domain.pddl $dst --gplan astar --gplan-h
lmc; then
        continue
    fi
    echo "Solved $dst"

    ID=$(($ID + 1))
done

```
