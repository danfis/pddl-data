
PDDL_DIR = '/home/danfis/dev/pddl-data/various/satellite-typed'
COMMON_PDDLS = ['domain.pddl']
TRAIN_PDDLS = [
    'pfile1.pddl',
    'pfile2.pddl',
    'pfile3.pddl',
    'pfile4.pddl',
    'pfile5.pddl',
]  # yapf: disable
TRAIN_NAMES = None
TEST_RUNS = [
    (['pfile5.pddl'], None),
    (['pfile6.pddl'], None),
    (['pfile7.pddl'], None),
    (['pfile8.pddl'], None),
    (['pfile9.pddl'], None),
    (['pfile10.pddl'], None),
    (['pfile11.pddl'], None),
    (['pfile12.pddl'], None),
    (['pfile13.pddl'], None),
]  # yapf: disable
