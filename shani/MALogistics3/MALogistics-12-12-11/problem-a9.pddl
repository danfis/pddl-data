(define 
(problem MALogistics-12-12-11-a9)
(:domain MALogistics-12-12-11-a9)
(:init
	(city-in-area C51 A9)
	(city-in-area C52 A9)
	(city-in-area C53 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C51)
	(adj C52 C51)
	(adj C51 C52)
	(adj C53 C52)
	(adj C52 C53)
)
(:goal (and
	(package-in-city P4 C51)
	(package-in-city P8 C53)
))
)
