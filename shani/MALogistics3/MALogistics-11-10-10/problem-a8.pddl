(define 
(problem MALogistics-11-10-10-a8)
(:domain MALogistics-11-10-10-a8)
(:init
	(city-in-area C35 A8)
	(city-in-area C36 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C35)
	(adj C36 C35)
	(adj C35 C36)
)
(:goal (and
	(package-in-city P7 C36)
))
)
