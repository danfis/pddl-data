(define 
(problem MALogistics-11-10-10-a9)
(:domain MALogistics-11-10-10-a9)
(:init
	(city-in-area C36 A9)
	(city-in-area C37 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C36)
	(adj C37 C36)
	(adj C36 C37)
)
(:goal (and
	(package-in-city P7 C36)
))
)
