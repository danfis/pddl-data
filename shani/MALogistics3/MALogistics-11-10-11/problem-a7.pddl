(define 
(problem MALogistics-11-10-11-a7)
(:domain MALogistics-11-10-11-a7)
(:init
	(city-in-area C30 A7)
	(city-in-area C31 A7)
	(city-in-area C32 A7)
	(city-in-area C33 A7)
	(city-in-area C34 A7)
	(city-in-area C35 A7)
	(city-in-area C36 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C30)
	(adj C31 C30)
	(adj C30 C31)
	(adj C32 C31)
	(adj C31 C32)
	(adj C33 C32)
	(adj C32 C33)
	(adj C34 C33)
	(adj C33 C34)
	(adj C35 C34)
	(adj C34 C35)
	(adj C36 C35)
	(adj C35 C36)
	(package-in-city P0 C33)
	(package-in-city P1 C33)
	(package-in-city P10 C36)
)
(:goal (and
))
)
