(define 
(problem MALogistics-10-11-11-a7)
(:domain MALogistics-10-11-11-a7)
(:init
	(city-in-area C47 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C47)
	(package-in-city P0 C47)
)
(:goal (and
	(package-in-city P9 C47)
))
)
