(define 
(problem MALogistics-10-10-12-a2)
(:domain MALogistics-10-10-12-a2)
(:init
	(city-in-area C18 A2)
	(truck-in-area T2 A2)
	(truck-in-city T2 C18)
)
(:goal (and
	(package-in-city P7 C18)
))
)
