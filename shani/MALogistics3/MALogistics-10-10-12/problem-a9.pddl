(define 
(problem MALogistics-10-10-12-a9)
(:domain MALogistics-10-10-12-a9)
(:init
	(city-in-area C56 A9)
	(city-in-area C57 A9)
	(city-in-area C58 A9)
	(city-in-area C59 A9)
	(city-in-area C60 A9)
	(city-in-area C61 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C56)
	(adj C57 C56)
	(adj C56 C57)
	(adj C58 C57)
	(adj C57 C58)
	(adj C59 C58)
	(adj C58 C59)
	(adj C60 C59)
	(adj C59 C60)
	(adj C61 C60)
	(adj C60 C61)
	(package-in-city P7 C56)
)
(:goal (and
	(package-in-city P6 C58)
))
)
