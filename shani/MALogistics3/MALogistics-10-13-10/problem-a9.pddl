(define 
(problem MALogistics-10-13-10-a9)
(:domain MALogistics-10-13-10-a9)
(:init
	(city-in-area C57 A9)
	(city-in-area C58 A9)
	(city-in-area C59 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C57)
	(adj C58 C57)
	(adj C57 C58)
	(adj C59 C58)
	(adj C58 C59)
)
(:goal (and
	(package-in-city P6 C59)
))
)
