(define 
(problem MALogistics-10-11-10-a3)
(:domain MALogistics-10-11-10-a3)
(:init
	(city-in-area C13 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C13)
)
(:goal (and
	(package-in-city P2 C13)
))
)
