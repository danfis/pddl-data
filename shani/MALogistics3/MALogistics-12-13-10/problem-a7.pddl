(define 
(problem MALogistics-12-13-10-a7)
(:domain MALogistics-12-13-10-a7)
(:init
	(city-in-area C32 A7)
	(city-in-area C33 A7)
	(city-in-area C34 A7)
	(city-in-area C35 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C32)
	(adj C33 C32)
	(adj C32 C33)
	(adj C34 C33)
	(adj C33 C34)
	(adj C35 C34)
	(adj C34 C35)
)
(:goal (and
	(package-in-city P9 C32)
))
)
