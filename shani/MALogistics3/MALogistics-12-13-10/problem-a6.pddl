(define 
(problem MALogistics-12-13-10-a6)
(:domain MALogistics-12-13-10-a6)
(:init
	(city-in-area C32 A6)
	(city-in-area C0 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C32)
	(adj C0 C32)
	(adj C32 C0)
)
(:goal (and
	(package-in-city P9 C32)
))
)
