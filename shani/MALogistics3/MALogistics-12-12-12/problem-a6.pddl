(define 
(problem MALogistics-12-12-12-a6)
(:domain MALogistics-12-12-12-a6)
(:init
	(city-in-area C35 A6)
	(city-in-area C36 A6)
	(city-in-area C0 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C35)
	(adj C36 C35)
	(adj C35 C36)
	(adj C0 C36)
	(adj C36 C0)
)
(:goal (and
	(package-in-city P4 C35)
))
)
