(define 
(problem MALogistics-12-12-12-a11)
(:domain MALogistics-12-12-12-a11)
(:init
	(city-in-area C68 A11)
	(city-in-area C69 A11)
	(city-in-area C70 A11)
	(city-in-area C71 A11)
	(city-in-area C72 A11)
	(city-in-area C73 A11)
	(city-in-area C74 A11)
	(city-in-area C75 A11)
	(city-in-area C76 A11)
	(city-in-area C77 A11)
	(city-in-area C78 A11)
	(truck-in-area T11 A11)
	(truck-in-city T11 C68)
	(adj C69 C68)
	(adj C68 C69)
	(adj C70 C69)
	(adj C69 C70)
	(adj C71 C70)
	(adj C70 C71)
	(adj C72 C71)
	(adj C71 C72)
	(adj C73 C72)
	(adj C72 C73)
	(adj C74 C73)
	(adj C73 C74)
	(adj C75 C74)
	(adj C74 C75)
	(adj C76 C75)
	(adj C75 C76)
	(adj C77 C76)
	(adj C76 C77)
	(adj C78 C77)
	(adj C77 C78)
	(package-in-city P9 C70)
)
(:goal (and
	(package-in-city P11 C75)
))
)
