(define 
(problem MALogistics-11-11-11-a6)
(:domain MALogistics-11-11-11-a6)
(:init
	(city-in-area C57 A6)
	(city-in-area C58 A6)
	(city-in-area C59 A6)
	(city-in-area C60 A6)
	(city-in-area C61 A6)
	(city-in-area C62 A6)
	(city-in-area C63 A6)
	(city-in-area C64 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C57)
	(adj C58 C57)
	(adj C57 C58)
	(adj C59 C58)
	(adj C58 C59)
	(adj C60 C59)
	(adj C59 C60)
	(adj C61 C60)
	(adj C60 C61)
	(adj C62 C61)
	(adj C61 C62)
	(adj C63 C62)
	(adj C62 C63)
	(adj C64 C63)
	(adj C63 C64)
)
(:goal (and
	(package-in-city P4 C60)
))
)
