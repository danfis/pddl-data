(define 
(problem MALogistics-11-12-12-a3)
(:domain MALogistics-11-12-12-a3)
(:init
	(city-in-area C29 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C29)
)
(:goal (and
	(package-in-city P10 C29)
	(package-in-city P11 C29)
))
)
