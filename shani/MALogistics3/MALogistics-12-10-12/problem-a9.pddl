(define 
(problem MALogistics-12-10-12-a9)
(:domain MALogistics-12-10-12-a9)
(:init
	(city-in-area C39 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C39)
)
(:goal (and
	(package-in-city P6 C39)
))
)
