(define 
(problem MALogistics-11-12-11-a6)
(:domain MALogistics-11-12-11-a6)
(:init
	(city-in-area C28 A6)
	(city-in-area C29 A6)
	(city-in-area C30 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C28)
	(adj C29 C28)
	(adj C28 C29)
	(adj C30 C29)
	(adj C29 C30)
)
(:goal (and
	(package-in-city P1 C30)
))
)
