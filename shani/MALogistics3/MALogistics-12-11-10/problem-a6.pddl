(define 
(problem MALogistics-12-11-10-a6)
(:domain MALogistics-12-11-10-a6)
(:init
	(city-in-area C34 A6)
	(city-in-area C35 A6)
	(city-in-area C0 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C34)
	(adj C35 C34)
	(adj C34 C35)
	(adj C0 C35)
	(adj C35 C0)
)
(:goal (and
	(package-in-city P8 C34)
))
)
