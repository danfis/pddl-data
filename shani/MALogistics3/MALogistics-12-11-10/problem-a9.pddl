(define 
(problem MALogistics-12-11-10-a9)
(:domain MALogistics-12-11-10-a9)
(:init
	(city-in-area C44 A9)
	(city-in-area C45 A9)
	(city-in-area C46 A9)
	(city-in-area C47 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C44)
	(adj C45 C44)
	(adj C44 C45)
	(adj C46 C45)
	(adj C45 C46)
	(adj C47 C46)
	(adj C46 C47)
)
(:goal (and
	(package-in-city P4 C44)
))
)
