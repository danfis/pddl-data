(define 
(problem MALogistics-11-13-11-a9)
(:domain MALogistics-11-13-11-a9)
(:init
	(city-in-area C55 A9)
	(city-in-area C56 A9)
	(city-in-area C57 A9)
	(city-in-area C58 A9)
	(city-in-area C59 A9)
	(city-in-area C60 A9)
	(city-in-area C61 A9)
	(city-in-area C62 A9)
	(city-in-area C63 A9)
	(city-in-area C64 A9)
	(city-in-area C65 A9)
	(city-in-area C66 A9)
	(city-in-area C67 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C55)
	(adj C56 C55)
	(adj C55 C56)
	(adj C57 C56)
	(adj C56 C57)
	(adj C58 C57)
	(adj C57 C58)
	(adj C59 C58)
	(adj C58 C59)
	(adj C60 C59)
	(adj C59 C60)
	(adj C61 C60)
	(adj C60 C61)
	(adj C62 C61)
	(adj C61 C62)
	(adj C63 C62)
	(adj C62 C63)
	(adj C64 C63)
	(adj C63 C64)
	(adj C65 C64)
	(adj C64 C65)
	(adj C66 C65)
	(adj C65 C66)
	(adj C67 C66)
	(adj C66 C67)
	(package-in-city P4 C56)
)
(:goal (and
	(package-in-city P1 C65)
	(package-in-city P4 C62)
))
)
