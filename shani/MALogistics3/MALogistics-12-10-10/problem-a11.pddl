(define 
(problem MALogistics-12-10-10-a11)
(:domain MALogistics-12-10-10-a11)
(:init
	(city-in-area C51 A11)
	(city-in-area C52 A11)
	(city-in-area C53 A11)
	(truck-in-area T11 A11)
	(truck-in-city T11 C51)
	(adj C52 C51)
	(adj C51 C52)
	(adj C53 C52)
	(adj C52 C53)
)
(:goal (and
	(package-in-city P4 C52)
))
)
