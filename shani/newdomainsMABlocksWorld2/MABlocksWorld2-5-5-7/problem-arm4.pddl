(define 
(problem MABlocksWorld2-5-5-7-arm4)
(:domain MABlocksWorld2-5-5-7-arm4)
(:init
	(in-stack T4 S4)
	(in-stack T28 S28)
	(in-stack T15 S15)
	(clear T4 S4)
	(clear T28 S28)
	(on B1 T15 S15)
	(clear B1 S15)
	(in-stack B1 S15)
)
(:goal (and
	(in-stack T4 S4)
	(in-stack T28 S28)
	(in-stack T15 S15)
	(on B2 T28 S28)
	(clear B2 S28)
	(in-stack B2 S28)
))
)
