(define 
(problem MABlocksWorld2-6-4-8-arm5)
(:domain MABlocksWorld2-6-4-8-arm5)
(:init
	(in-stack T4 S4)
	(in-stack T40 S40)
	(in-stack T23 S23)
	(on B0 T4 S4)
	(clear B0 S4)
	(in-stack B0 S4)
	(clear T40 S40)
	(clear T23 S23)
)
(:goal (and
	(in-stack T4 S4)
	(in-stack T40 S40)
	(in-stack T23 S23)
))
)
