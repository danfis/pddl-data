(define 
(problem MABlocksWorld2-5-6-7-arm1)
(:domain MABlocksWorld2-5-6-7-arm1)
(:init
	(in-stack T2 S2)
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(in-stack T7 S7)
	(in-stack T8 S8)
	(in-stack T9 S9)
	(clear T2 S2)
	(clear T3 S3)
	(on B1 T4 S4)
	(clear B1 S4)
	(in-stack B1 S4)
	(clear T5 S5)
	(on B3 T6 S6)
	(in-stack B3 S6)
	(on B0 B3 S6)
	(in-stack B0 S6)
	(on B4 B0 S6)
	(clear B4 S6)
	(in-stack B4 S6)
	(clear T7 S7)
	(clear T8 S8)
	(on B5 T9 S9)
	(clear B5 S9)
	(in-stack B5 S9)
)
(:goal (and
	(in-stack T2 S2)
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(in-stack T7 S7)
	(in-stack T8 S8)
	(in-stack T9 S9)
	(on B5 T7 S7)
	(clear B5 S7)
	(in-stack B5 S7)
))
)
