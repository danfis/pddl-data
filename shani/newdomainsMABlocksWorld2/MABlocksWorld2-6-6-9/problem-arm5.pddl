(define 
(problem MABlocksWorld2-6-6-9-arm5)
(:domain MABlocksWorld2-6-6-9-arm5)
(:init
	(in-stack T4 S4)
	(in-stack T37 S37)
	(in-stack T25 S25)
	(clear T4 S4)
	(clear T37 S37)
	(clear T25 S25)
)
(:goal (and
	(in-stack T4 S4)
	(in-stack T37 S37)
	(in-stack T25 S25)
))
)
