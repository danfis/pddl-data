(define 
(problem MABlocksWorld2-5-5-8-arm4)
(:domain MABlocksWorld2-5-5-8-arm4)
(:init
	(in-stack T5 S5)
	(in-stack T32 S32)
	(in-stack T21 S21)
	(clear T5 S5)
	(clear T32 S32)
	(clear T21 S21)
)
(:goal (and
	(in-stack T5 S5)
	(in-stack T32 S32)
	(in-stack T21 S21)
))
)
