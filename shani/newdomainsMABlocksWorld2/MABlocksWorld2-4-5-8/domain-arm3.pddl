(define 
(domain MABlocksWorld2-4-5-8-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 A3 - ARM
	 S4 - STACK
	 (:private S23 - STACK)
	 S20 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s4-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (in-stack ?bSource S4) (clear ?bMove S4) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S4)) (not (in-stack ?bMove S4)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S4) (not  (clear ?bMove S4)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a3-s23-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S4) (in-stack ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S4) (in-stack ?bMove S4) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S4) (not (clear ?bTarget S4)))
)

(:action move-a3-s23-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S20) (in-stack ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S20) (in-stack ?bMove S20) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S20) (not (clear ?bTarget S20)))
)

(:action move-a3-s20-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (in-stack ?bSource S20) (clear ?bMove S20) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S20)) (not (in-stack ?bMove S20)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S20) (not  (clear ?bMove S20)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

)