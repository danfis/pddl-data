(define 
(problem MABlocksWorld2-6-5-7-arm5)
(:domain MABlocksWorld2-6-5-7-arm5)
(:init
	(in-stack T6 S6)
	(in-stack T31 S31)
	(in-stack T27 S27)
	(clear T6 S6)
	(clear T31 S31)
	(clear T27 S27)
)
(:goal (and
	(in-stack T6 S6)
	(in-stack T31 S31)
	(in-stack T27 S27)
))
)
