(define 
(problem MABlocksWorld2-6-5-7-arm3)
(:domain MABlocksWorld2-6-5-7-arm3)
(:init
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T25 S25)
	(clear T19 S19)
	(clear T20 S20)
	(clear T21 S21)
	(clear T22 S22)
	(on B4 T23 S23)
	(clear B4 S23)
	(in-stack B4 S23)
	(clear T24 S24)
	(clear T25 S25)
)
(:goal (and
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T25 S25)
	(on B2 T24 S24)
	(clear B2 S24)
	(in-stack B2 S24)
))
)
