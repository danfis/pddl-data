(define 
(problem MABlocksWorld2-6-5-9-arm4)
(:domain MABlocksWorld2-6-5-9-arm4)
(:init
	(in-stack T31 S31)
	(in-stack T32 S32)
	(in-stack T33 S33)
	(in-stack T34 S34)
	(in-stack T35 S35)
	(in-stack T36 S36)
	(in-stack T37 S37)
	(in-stack T0 S0)
	(clear T31 S31)
	(clear T32 S32)
	(on B3 T33 S33)
	(clear B3 S33)
	(in-stack B3 S33)
	(clear T34 S34)
	(clear T35 S35)
	(clear T36 S36)
	(clear T37 S37)
	(clear T0 S0)
)
(:goal (and
	(in-stack T31 S31)
	(in-stack T32 S32)
	(in-stack T33 S33)
	(in-stack T34 S34)
	(in-stack T35 S35)
	(in-stack T36 S36)
	(in-stack T37 S37)
	(in-stack T0 S0)
	(on B4 T34 S34)
	(clear B4 S34)
	(in-stack B4 S34)
))
)
