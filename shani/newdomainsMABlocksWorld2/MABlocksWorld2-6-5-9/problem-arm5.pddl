(define 
(problem MABlocksWorld2-6-5-9-arm5)
(:domain MABlocksWorld2-6-5-9-arm5)
(:init
	(in-stack T0 S0)
	(in-stack T38 S38)
	(in-stack T32 S32)
	(clear T0 S0)
	(clear T38 S38)
	(clear T32 S32)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T38 S38)
	(in-stack T32 S32)
))
)
