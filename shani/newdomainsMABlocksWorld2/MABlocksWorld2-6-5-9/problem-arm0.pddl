(define 
(problem MABlocksWorld2-6-5-9-arm0)
(:domain MABlocksWorld2-6-5-9-arm0)
(:init
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(in-stack T7 S7)
	(in-stack T8 S8)
	(in-stack T9 S9)
	(clear T0 S0)
	(clear T1 S1)
	(clear T2 S2)
	(clear T3 S3)
	(clear T4 S4)
	(on B2 T5 S5)
	(clear B2 S5)
	(in-stack B2 S5)
	(on B0 T6 S6)
	(clear B0 S6)
	(in-stack B0 S6)
	(clear T7 S7)
	(on B1 T8 S8)
	(clear B1 S8)
	(in-stack B1 S8)
	(clear T9 S9)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(in-stack T7 S7)
	(in-stack T8 S8)
	(in-stack T9 S9)
	(on B0 T1 S1)
	(clear B0 S1)
	(in-stack B0 S1)
	(on B1 T8 S8)
	(clear B1 S8)
	(in-stack B1 S8)
))
)
