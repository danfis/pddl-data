(define 
(problem MABlocksWorld2-6-5-9-arm3)
(:domain MABlocksWorld2-6-5-9-arm3)
(:init
	(in-stack T25 S25)
	(in-stack T26 S26)
	(in-stack T27 S27)
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(clear T25 S25)
	(clear T26 S26)
	(clear T27 S27)
	(clear T28 S28)
	(clear T29 S29)
	(clear T30 S30)
	(clear T31 S31)
)
(:goal (and
	(in-stack T25 S25)
	(in-stack T26 S26)
	(in-stack T27 S27)
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(on B3 T28 S28)
	(clear B3 S28)
	(in-stack B3 S28)
))
)
