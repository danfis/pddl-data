(define 
(domain MABlocksWorld2-6-5-9-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 T51 - BLOCK
	 T52 - BLOCK
	 T53 - BLOCK
	 T54 - BLOCK
	 A5 - ARM
	 S0 - STACK
	 (:private S38 - STACK)
	 S32 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s0-s38
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S38) (in-stack ?bTarget S38) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S38) (in-stack ?bMove S38) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S38) (not (clear ?bTarget S38)))
)

(:action move-a5-s38-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S38) (in-stack ?bSource S38) (clear ?bMove S38) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S38)) (not (in-stack ?bMove S38)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S38) (not  (clear ?bMove S38)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a5-s38-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S38) (in-stack ?bSource S38) (clear ?bMove S38) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S38)) (not (in-stack ?bMove S38)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S38) (not  (clear ?bMove S38)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

(:action move-a5-s32-s38
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S38) (in-stack ?bTarget S38) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S38) (in-stack ?bMove S38) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S38) (not (clear ?bTarget S38)))
)

)