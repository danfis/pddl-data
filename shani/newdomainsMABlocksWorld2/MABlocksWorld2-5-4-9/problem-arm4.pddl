(define 
(problem MABlocksWorld2-5-4-9-arm4)
(:domain MABlocksWorld2-5-4-9-arm4)
(:init
	(in-stack T11 S11)
	(in-stack T26 S26)
	(in-stack T23 S23)
	(clear T11 S11)
	(clear T26 S26)
	(clear T23 S23)
)
(:goal (and
	(in-stack T11 S11)
	(in-stack T26 S26)
	(in-stack T23 S23)
))
)
