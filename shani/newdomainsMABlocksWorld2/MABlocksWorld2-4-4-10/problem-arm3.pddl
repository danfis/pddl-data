(define 
(problem MABlocksWorld2-4-4-10-arm3)
(:domain MABlocksWorld2-4-4-10-arm3)
(:init
	(in-stack T6 S6)
	(in-stack T24 S24)
	(in-stack T11 S11)
	(clear T6 S6)
	(clear T24 S24)
	(clear T11 S11)
)
(:goal (and
	(in-stack T6 S6)
	(in-stack T24 S24)
	(in-stack T11 S11)
))
)
