(define 
(problem MABlocksWorld2-5-4-8-arm4)
(:domain MABlocksWorld2-5-4-8-arm4)
(:init
	(in-stack T9 S9)
	(in-stack T25 S25)
	(in-stack T22 S22)
	(on B1 T9 S9)
	(clear B1 S9)
	(in-stack B1 S9)
	(clear T25 S25)
	(clear T22 S22)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T25 S25)
	(in-stack T22 S22)
))
)
