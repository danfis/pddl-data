(define 
(problem MABlocksWorld2-4-4-8-arm3)
(:domain MABlocksWorld2-4-4-8-arm3)
(:init
	(in-stack T5 S5)
	(in-stack T21 S21)
	(in-stack T15 S15)
	(clear T5 S5)
	(clear T21 S21)
	(clear T15 S15)
)
(:goal (and
	(in-stack T5 S5)
	(in-stack T21 S21)
	(in-stack T15 S15)
))
)
