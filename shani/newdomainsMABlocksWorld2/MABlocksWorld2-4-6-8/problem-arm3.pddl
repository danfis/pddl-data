(define 
(problem MABlocksWorld2-4-6-8-arm3)
(:domain MABlocksWorld2-4-6-8-arm3)
(:init
	(in-stack T6 S6)
	(in-stack T25 S25)
	(in-stack T17 S17)
	(on B3 T6 S6)
	(clear B3 S6)
	(in-stack B3 S6)
	(clear T25 S25)
	(clear T17 S17)
)
(:goal (and
	(in-stack T6 S6)
	(in-stack T25 S25)
	(in-stack T17 S17)
))
)
