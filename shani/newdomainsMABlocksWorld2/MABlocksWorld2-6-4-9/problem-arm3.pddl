(define 
(problem MABlocksWorld2-6-4-9-arm3)
(:domain MABlocksWorld2-6-4-9-arm3)
(:init
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T25 S25)
	(in-stack T26 S26)
	(in-stack T27 S27)
	(in-stack T28 S28)
	(clear T22 S22)
	(clear T23 S23)
	(clear T24 S24)
	(clear T25 S25)
	(clear T26 S26)
	(clear T27 S27)
	(clear T28 S28)
)
(:goal (and
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T25 S25)
	(in-stack T26 S26)
	(in-stack T27 S27)
	(in-stack T28 S28)
	(on B2 T27 S27)
	(clear B2 S27)
	(in-stack B2 S27)
))
)
