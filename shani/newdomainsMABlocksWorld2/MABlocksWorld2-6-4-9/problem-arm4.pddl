(define 
(problem MABlocksWorld2-6-4-9-arm4)
(:domain MABlocksWorld2-6-4-9-arm4)
(:init
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(in-stack T32 S32)
	(in-stack T33 S33)
	(in-stack T34 S34)
	(in-stack T35 S35)
	(in-stack T0 S0)
	(clear T28 S28)
	(clear T29 S29)
	(clear T30 S30)
	(clear T31 S31)
	(clear T32 S32)
	(clear T33 S33)
	(on B1 T34 S34)
	(clear B1 S34)
	(in-stack B1 S34)
	(clear T35 S35)
	(on B2 T0 S0)
	(clear B2 S0)
	(in-stack B2 S0)
)
(:goal (and
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(in-stack T32 S32)
	(in-stack T33 S33)
	(in-stack T34 S34)
	(in-stack T35 S35)
	(in-stack T0 S0)
	(on B1 T29 S29)
	(clear B1 S29)
	(in-stack B1 S29)
))
)
