(define 
(problem MABlocksWorld2-6-4-9-arm1)
(:domain MABlocksWorld2-6-4-9-arm1)
(:init
	(in-stack T9 S9)
	(in-stack T10 S10)
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T13 S13)
	(in-stack T14 S14)
	(in-stack T15 S15)
	(clear T9 S9)
	(clear T10 S10)
	(clear T11 S11)
	(clear T12 S12)
	(clear T13 S13)
	(clear T14 S14)
	(clear T15 S15)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T10 S10)
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T13 S13)
	(in-stack T14 S14)
	(in-stack T15 S15)
	(on B0 T12 S12)
	(clear B0 S12)
	(in-stack B0 S12)
))
)
