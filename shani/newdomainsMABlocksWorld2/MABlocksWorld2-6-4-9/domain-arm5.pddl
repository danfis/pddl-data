(define 
(domain MABlocksWorld2-6-4-9-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 T51 - BLOCK
	 T52 - BLOCK
	 T53 - BLOCK
	 T54 - BLOCK
	 A5 - ARM
	 S13 - STACK
	 (:private S36 - STACK)
	 S31 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s13-s36
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S36) (in-stack ?bTarget S36) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S36) (in-stack ?bMove S36) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S36) (not (clear ?bTarget S36)))
)

(:action move-a5-s36-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S36) (in-stack ?bSource S36) (clear ?bMove S36) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S36)) (not (in-stack ?bMove S36)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S36) (not  (clear ?bMove S36)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

(:action move-a5-s36-s31
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S36) (in-stack ?bSource S36) (clear ?bMove S36) (clear ?bTarget S31) (in-stack ?bTarget S31) )
:effect (and (not (on ?bMove ?bSource S36)) (not (in-stack ?bMove S36)) (on ?bMove ?bTarget S31) (in-stack ?bMove S31) (clear ?bSource S36) (not  (clear ?bMove S36)) (clear ?bMove S31) (not (clear ?bTarget S31)))
)

(:action move-a5-s31-s36
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S31) (in-stack ?bSource S31) (clear ?bMove S31) (clear ?bTarget S36) (in-stack ?bTarget S36) )
:effect (and (not (on ?bMove ?bSource S31)) (not (in-stack ?bMove S31)) (on ?bMove ?bTarget S36) (in-stack ?bMove S36) (clear ?bSource S31) (not  (clear ?bMove S31)) (clear ?bMove S36) (not (clear ?bTarget S36)))
)

)