(define 
(problem MABlocksWorld2-4-5-7-arm3)
(:domain MABlocksWorld2-4-5-7-arm3)
(:init
	(in-stack T0 S0)
	(in-stack T18 S18)
	(in-stack T11 S11)
	(clear T0 S0)
	(clear T18 S18)
	(clear T11 S11)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T18 S18)
	(in-stack T11 S11)
))
)
