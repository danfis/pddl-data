(define 
(problem MABlocksWorld2-4-5-7-arm2)
(:domain MABlocksWorld2-4-5-7-arm2)
(:init
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T13 S13)
	(in-stack T14 S14)
	(in-stack T15 S15)
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T0 S0)
	(clear T11 S11)
	(clear T12 S12)
	(on B1 T13 S13)
	(clear B1 S13)
	(in-stack B1 S13)
	(on B3 T14 S14)
	(clear B3 S14)
	(in-stack B3 S14)
	(clear T15 S15)
	(on B0 T16 S16)
	(clear B0 S16)
	(in-stack B0 S16)
	(clear T17 S17)
	(clear T0 S0)
)
(:goal (and
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T13 S13)
	(in-stack T14 S14)
	(in-stack T15 S15)
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T0 S0)
))
)
