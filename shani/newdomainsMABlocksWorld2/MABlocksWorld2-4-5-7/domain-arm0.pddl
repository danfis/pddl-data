(define 
(domain MABlocksWorld2-4-5-7-arm0)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 A0 - ARM
	 S0 - STACK
	 (:private S1 - STACK)
	 (:private S2 - STACK)
	 (:private S3 - STACK)
	 (:private S4 - STACK)
	 (:private S5 - STACK)
	 S6 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a0-s0-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S1) (in-stack ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S1) (in-stack ?bMove S1) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S1) (not (clear ?bTarget S1)))
)

(:action move-a0-s1-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (in-stack ?bSource S1) (clear ?bMove S1) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S1)) (not (in-stack ?bMove S1)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S1) (not  (clear ?bMove S1)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a0-s1-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (in-stack ?bSource S1) (clear ?bMove S1) (clear ?bTarget S2) (in-stack ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S1)) (not (in-stack ?bMove S1)) (on ?bMove ?bTarget S2) (in-stack ?bMove S2) (clear ?bSource S1) (not  (clear ?bMove S1)) (clear ?bMove S2) (not (clear ?bTarget S2)))
)

(:action move-a0-s2-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (in-stack ?bSource S2) (clear ?bMove S2) (clear ?bTarget S1) (in-stack ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S2)) (not (in-stack ?bMove S2)) (on ?bMove ?bTarget S1) (in-stack ?bMove S1) (clear ?bSource S2) (not  (clear ?bMove S2)) (clear ?bMove S1) (not (clear ?bTarget S1)))
)

(:action move-a0-s2-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (in-stack ?bSource S2) (clear ?bMove S2) (clear ?bTarget S3) (in-stack ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S2)) (not (in-stack ?bMove S2)) (on ?bMove ?bTarget S3) (in-stack ?bMove S3) (clear ?bSource S2) (not  (clear ?bMove S2)) (clear ?bMove S3) (not (clear ?bTarget S3)))
)

(:action move-a0-s3-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (in-stack ?bSource S3) (clear ?bMove S3) (clear ?bTarget S2) (in-stack ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S3)) (not (in-stack ?bMove S3)) (on ?bMove ?bTarget S2) (in-stack ?bMove S2) (clear ?bSource S3) (not  (clear ?bMove S3)) (clear ?bMove S2) (not (clear ?bTarget S2)))
)

(:action move-a0-s3-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (in-stack ?bSource S3) (clear ?bMove S3) (clear ?bTarget S4) (in-stack ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S3)) (not (in-stack ?bMove S3)) (on ?bMove ?bTarget S4) (in-stack ?bMove S4) (clear ?bSource S3) (not  (clear ?bMove S3)) (clear ?bMove S4) (not (clear ?bTarget S4)))
)

(:action move-a0-s4-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (in-stack ?bSource S4) (clear ?bMove S4) (clear ?bTarget S3) (in-stack ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S4)) (not (in-stack ?bMove S4)) (on ?bMove ?bTarget S3) (in-stack ?bMove S3) (clear ?bSource S4) (not  (clear ?bMove S4)) (clear ?bMove S3) (not (clear ?bTarget S3)))
)

(:action move-a0-s4-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (in-stack ?bSource S4) (clear ?bMove S4) (clear ?bTarget S5) (in-stack ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S4)) (not (in-stack ?bMove S4)) (on ?bMove ?bTarget S5) (in-stack ?bMove S5) (clear ?bSource S4) (not  (clear ?bMove S4)) (clear ?bMove S5) (not (clear ?bTarget S5)))
)

(:action move-a0-s5-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (in-stack ?bSource S5) (clear ?bMove S5) (clear ?bTarget S4) (in-stack ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S5)) (not (in-stack ?bMove S5)) (on ?bMove ?bTarget S4) (in-stack ?bMove S4) (clear ?bSource S5) (not  (clear ?bMove S5)) (clear ?bMove S4) (not (clear ?bTarget S4)))
)

(:action move-a0-s5-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (in-stack ?bSource S5) (clear ?bMove S5) (clear ?bTarget S6) (in-stack ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S5)) (not (in-stack ?bMove S5)) (on ?bMove ?bTarget S6) (in-stack ?bMove S6) (clear ?bSource S5) (not  (clear ?bMove S5)) (clear ?bMove S6) (not (clear ?bTarget S6)))
)

(:action move-a0-s6-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (in-stack ?bSource S6) (clear ?bMove S6) (clear ?bTarget S5) (in-stack ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S6)) (not (in-stack ?bMove S6)) (on ?bMove ?bTarget S5) (in-stack ?bMove S5) (clear ?bSource S6) (not  (clear ?bMove S6)) (clear ?bMove S5) (not (clear ?bTarget S5)))
)

)