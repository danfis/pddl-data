(define 
(domain MABlocksWorld2-6-4-10-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 T51 - BLOCK
	 T52 - BLOCK
	 T53 - BLOCK
	 T54 - BLOCK
	 T55 - BLOCK
	 T56 - BLOCK
	 T57 - BLOCK
	 T58 - BLOCK
	 T59 - BLOCK
	 T60 - BLOCK
	 A5 - ARM
	 S6 - STACK
	 (:private S45 - STACK)
	 S33 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s6-s45
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (in-stack ?bSource S6) (clear ?bMove S6) (clear ?bTarget S45) (in-stack ?bTarget S45) )
:effect (and (not (on ?bMove ?bSource S6)) (not (in-stack ?bMove S6)) (on ?bMove ?bTarget S45) (in-stack ?bMove S45) (clear ?bSource S6) (not  (clear ?bMove S6)) (clear ?bMove S45) (not (clear ?bTarget S45)))
)

(:action move-a5-s45-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S45) (in-stack ?bSource S45) (clear ?bMove S45) (clear ?bTarget S6) (in-stack ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S45)) (not (in-stack ?bMove S45)) (on ?bMove ?bTarget S6) (in-stack ?bMove S6) (clear ?bSource S45) (not  (clear ?bMove S45)) (clear ?bMove S6) (not (clear ?bTarget S6)))
)

(:action move-a5-s45-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S45) (in-stack ?bSource S45) (clear ?bMove S45) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S45)) (not (in-stack ?bMove S45)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S45) (not  (clear ?bMove S45)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a5-s33-s45
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S45) (in-stack ?bTarget S45) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S45) (in-stack ?bMove S45) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S45) (not (clear ?bTarget S45)))
)

)