(define 
(problem MABlocksWorld2-4-4-7-arm3)
(:domain MABlocksWorld2-4-4-7-arm3)
(:init
	(in-stack T2 S2)
	(in-stack T19 S19)
	(in-stack T9 S9)
	(clear T2 S2)
	(clear T19 S19)
	(clear T9 S9)
)
(:goal (and
	(in-stack T2 S2)
	(in-stack T19 S19)
	(in-stack T9 S9)
))
)
