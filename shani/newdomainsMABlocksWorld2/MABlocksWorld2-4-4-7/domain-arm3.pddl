(define 
(domain MABlocksWorld2-4-4-7-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 A3 - ARM
	 S2 - STACK
	 (:private S19 - STACK)
	 S9 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s2-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (in-stack ?bSource S2) (clear ?bMove S2) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S2)) (not (in-stack ?bMove S2)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S2) (not  (clear ?bMove S2)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

(:action move-a3-s19-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S2) (in-stack ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S2) (in-stack ?bMove S2) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S2) (not (clear ?bTarget S2)))
)

(:action move-a3-s19-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a3-s9-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

)