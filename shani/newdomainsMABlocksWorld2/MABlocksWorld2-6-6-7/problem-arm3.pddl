(define 
(problem MABlocksWorld2-6-6-7-arm3)
(:domain MABlocksWorld2-6-6-7-arm3)
(:init
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(clear T17 S17)
	(clear T18 S18)
	(clear T19 S19)
	(clear T20 S20)
	(clear T21 S21)
	(on B0 T22 S22)
	(clear B0 S22)
	(in-stack B0 S22)
	(clear T23 S23)
)
(:goal (and
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(on B0 T17 S17)
	(in-stack B0 S17)
	(on B3 B0 S17)
	(clear B3 S17)
	(in-stack B3 S17)
))
)
