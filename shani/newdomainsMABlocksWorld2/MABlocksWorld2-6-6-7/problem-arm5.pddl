(define 
(problem MABlocksWorld2-6-6-7-arm5)
(:domain MABlocksWorld2-6-6-7-arm5)
(:init
	(in-stack T13 S13)
	(in-stack T29 S29)
	(in-stack T18 S18)
	(on B5 T13 S13)
	(clear B5 S13)
	(in-stack B5 S13)
	(clear T29 S29)
	(clear T18 S18)
)
(:goal (and
	(in-stack T13 S13)
	(in-stack T29 S29)
	(in-stack T18 S18)
))
)
