(define 
(domain MABlocksWorld2-6-4-7-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 A5 - ARM
	 S9 - STACK
	 (:private S27 - STACK)
	 S13 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s9-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

(:action move-a5-s27-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a5-s27-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

(:action move-a5-s13-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

)