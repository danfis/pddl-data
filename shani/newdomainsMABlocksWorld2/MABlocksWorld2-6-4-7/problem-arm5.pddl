(define 
(problem MABlocksWorld2-6-4-7-arm5)
(:domain MABlocksWorld2-6-4-7-arm5)
(:init
	(in-stack T9 S9)
	(in-stack T27 S27)
	(in-stack T13 S13)
	(on B3 T9 S9)
	(clear B3 S9)
	(in-stack B3 S9)
	(clear T27 S27)
	(clear T13 S13)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T27 S27)
	(in-stack T13 S13)
))
)
