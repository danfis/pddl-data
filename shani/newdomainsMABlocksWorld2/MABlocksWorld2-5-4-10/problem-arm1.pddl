(define 
(problem MABlocksWorld2-5-4-10-arm1)
(:domain MABlocksWorld2-5-4-10-arm1)
(:init
	(in-stack T8 S8)
	(in-stack T9 S9)
	(in-stack T10 S10)
	(clear T8 S8)
	(clear T9 S9)
	(on B1 T10 S10)
	(clear B1 S10)
	(in-stack B1 S10)
)
(:goal (and
	(in-stack T8 S8)
	(in-stack T9 S9)
	(in-stack T10 S10)
))
)
