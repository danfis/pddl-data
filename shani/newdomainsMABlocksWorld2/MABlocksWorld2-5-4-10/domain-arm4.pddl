(define 
(domain MABlocksWorld2-5-4-10-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 A4 - ARM
	 S0 - STACK
	 (:private S33 - STACK)
	 S23 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s0-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a4-s33-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a4-s33-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a4-s23-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

)