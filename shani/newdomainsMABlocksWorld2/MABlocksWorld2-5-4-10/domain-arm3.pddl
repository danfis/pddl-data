(define 
(domain MABlocksWorld2-5-4-10-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 A3 - ARM
	 S21 - STACK
	 (:private S22 - STACK)
	 S23 - STACK
	 (:private S24 - STACK)
	 (:private S25 - STACK)
	 (:private S26 - STACK)
	 (:private S27 - STACK)
	 (:private S28 - STACK)
	 (:private S29 - STACK)
	 (:private S30 - STACK)
	 (:private S31 - STACK)
	 (:private S32 - STACK)
	 S0 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s21-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S22) (in-stack ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S22) (in-stack ?bMove S22) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S22) (not (clear ?bTarget S22)))
)

(:action move-a3-s22-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (in-stack ?bSource S22) (clear ?bMove S22) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S22)) (not (in-stack ?bMove S22)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S22) (not  (clear ?bMove S22)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

(:action move-a3-s22-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (in-stack ?bSource S22) (clear ?bMove S22) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S22)) (not (in-stack ?bMove S22)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S22) (not  (clear ?bMove S22)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a3-s23-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S22) (in-stack ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S22) (in-stack ?bMove S22) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S22) (not (clear ?bTarget S22)))
)

(:action move-a3-s23-s24
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S24) (in-stack ?bTarget S24) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S24) (in-stack ?bMove S24) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S24) (not (clear ?bTarget S24)))
)

(:action move-a3-s24-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S24) (in-stack ?bSource S24) (clear ?bMove S24) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S24)) (not (in-stack ?bMove S24)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S24) (not  (clear ?bMove S24)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a3-s24-s25
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S24) (in-stack ?bSource S24) (clear ?bMove S24) (clear ?bTarget S25) (in-stack ?bTarget S25) )
:effect (and (not (on ?bMove ?bSource S24)) (not (in-stack ?bMove S24)) (on ?bMove ?bTarget S25) (in-stack ?bMove S25) (clear ?bSource S24) (not  (clear ?bMove S24)) (clear ?bMove S25) (not (clear ?bTarget S25)))
)

(:action move-a3-s25-s24
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S25) (in-stack ?bSource S25) (clear ?bMove S25) (clear ?bTarget S24) (in-stack ?bTarget S24) )
:effect (and (not (on ?bMove ?bSource S25)) (not (in-stack ?bMove S25)) (on ?bMove ?bTarget S24) (in-stack ?bMove S24) (clear ?bSource S25) (not  (clear ?bMove S25)) (clear ?bMove S24) (not (clear ?bTarget S24)))
)

(:action move-a3-s25-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S25) (in-stack ?bSource S25) (clear ?bMove S25) (clear ?bTarget S26) (in-stack ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S25)) (not (in-stack ?bMove S25)) (on ?bMove ?bTarget S26) (in-stack ?bMove S26) (clear ?bSource S25) (not  (clear ?bMove S25)) (clear ?bMove S26) (not (clear ?bTarget S26)))
)

(:action move-a3-s26-s25
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (in-stack ?bSource S26) (clear ?bMove S26) (clear ?bTarget S25) (in-stack ?bTarget S25) )
:effect (and (not (on ?bMove ?bSource S26)) (not (in-stack ?bMove S26)) (on ?bMove ?bTarget S25) (in-stack ?bMove S25) (clear ?bSource S26) (not  (clear ?bMove S26)) (clear ?bMove S25) (not (clear ?bTarget S25)))
)

(:action move-a3-s26-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (in-stack ?bSource S26) (clear ?bMove S26) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S26)) (not (in-stack ?bMove S26)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S26) (not  (clear ?bMove S26)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

(:action move-a3-s27-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S26) (in-stack ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S26) (in-stack ?bMove S26) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S26) (not (clear ?bTarget S26)))
)

(:action move-a3-s27-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

(:action move-a3-s28-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

(:action move-a3-s28-s29
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S29) (in-stack ?bTarget S29) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S29) (in-stack ?bMove S29) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S29) (not (clear ?bTarget S29)))
)

(:action move-a3-s29-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S29) (in-stack ?bSource S29) (clear ?bMove S29) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S29)) (not (in-stack ?bMove S29)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S29) (not  (clear ?bMove S29)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

(:action move-a3-s29-s30
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S29) (in-stack ?bSource S29) (clear ?bMove S29) (clear ?bTarget S30) (in-stack ?bTarget S30) )
:effect (and (not (on ?bMove ?bSource S29)) (not (in-stack ?bMove S29)) (on ?bMove ?bTarget S30) (in-stack ?bMove S30) (clear ?bSource S29) (not  (clear ?bMove S29)) (clear ?bMove S30) (not (clear ?bTarget S30)))
)

(:action move-a3-s30-s29
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S30) (in-stack ?bSource S30) (clear ?bMove S30) (clear ?bTarget S29) (in-stack ?bTarget S29) )
:effect (and (not (on ?bMove ?bSource S30)) (not (in-stack ?bMove S30)) (on ?bMove ?bTarget S29) (in-stack ?bMove S29) (clear ?bSource S30) (not  (clear ?bMove S30)) (clear ?bMove S29) (not (clear ?bTarget S29)))
)

(:action move-a3-s30-s31
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S30) (in-stack ?bSource S30) (clear ?bMove S30) (clear ?bTarget S31) (in-stack ?bTarget S31) )
:effect (and (not (on ?bMove ?bSource S30)) (not (in-stack ?bMove S30)) (on ?bMove ?bTarget S31) (in-stack ?bMove S31) (clear ?bSource S30) (not  (clear ?bMove S30)) (clear ?bMove S31) (not (clear ?bTarget S31)))
)

(:action move-a3-s31-s30
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S31) (in-stack ?bSource S31) (clear ?bMove S31) (clear ?bTarget S30) (in-stack ?bTarget S30) )
:effect (and (not (on ?bMove ?bSource S31)) (not (in-stack ?bMove S31)) (on ?bMove ?bTarget S30) (in-stack ?bMove S30) (clear ?bSource S31) (not  (clear ?bMove S31)) (clear ?bMove S30) (not (clear ?bTarget S30)))
)

(:action move-a3-s31-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S31) (in-stack ?bSource S31) (clear ?bMove S31) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S31)) (not (in-stack ?bMove S31)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S31) (not  (clear ?bMove S31)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

(:action move-a3-s32-s31
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S31) (in-stack ?bTarget S31) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S31) (in-stack ?bMove S31) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S31) (not (clear ?bTarget S31)))
)

(:action move-a3-s32-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a3-s0-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

)