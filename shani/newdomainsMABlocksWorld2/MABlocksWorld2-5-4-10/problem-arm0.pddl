(define 
(problem MABlocksWorld2-5-4-10-arm0)
(:domain MABlocksWorld2-5-4-10-arm0)
(:init
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(in-stack T7 S7)
	(in-stack T8 S8)
	(clear T0 S0)
	(clear T1 S1)
	(clear T2 S2)
	(clear T3 S3)
	(on B0 T4 S4)
	(clear B0 S4)
	(in-stack B0 S4)
	(clear T5 S5)
	(on B2 T6 S6)
	(clear B2 S6)
	(in-stack B2 S6)
	(clear T7 S7)
	(clear T8 S8)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(in-stack T7 S7)
	(in-stack T8 S8)
	(on B3 T5 S5)
	(clear B3 S5)
	(in-stack B3 S5)
))
)
