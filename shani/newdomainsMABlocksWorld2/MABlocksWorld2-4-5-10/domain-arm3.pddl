(define 
(domain MABlocksWorld2-4-5-10-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 A3 - ARM
	 S9 - STACK
	 (:private S26 - STACK)
	 S21 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s9-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S26) (in-stack ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S26) (in-stack ?bMove S26) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S26) (not (clear ?bTarget S26)))
)

(:action move-a3-s26-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (in-stack ?bSource S26) (clear ?bMove S26) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S26)) (not (in-stack ?bMove S26)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S26) (not  (clear ?bMove S26)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a3-s26-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (in-stack ?bSource S26) (clear ?bMove S26) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S26)) (not (in-stack ?bMove S26)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S26) (not  (clear ?bMove S26)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

(:action move-a3-s21-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S26) (in-stack ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S26) (in-stack ?bMove S26) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S26) (not (clear ?bTarget S26)))
)

)