(define 
(problem MABlocksWorld2-4-5-10-arm3)
(:domain MABlocksWorld2-4-5-10-arm3)
(:init
	(in-stack T9 S9)
	(in-stack T26 S26)
	(in-stack T21 S21)
	(clear T9 S9)
	(clear T26 S26)
	(clear T21 S21)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T26 S26)
	(in-stack T21 S21)
))
)
