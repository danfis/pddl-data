(define 
(problem MABlocksWorld2-6-6-8-arm5)
(:domain MABlocksWorld2-6-6-8-arm5)
(:init
	(in-stack T6 S6)
	(in-stack T33 S33)
	(in-stack T24 S24)
	(on B3 T6 S6)
	(clear B3 S6)
	(in-stack B3 S6)
	(clear T33 S33)
	(clear T24 S24)
)
(:goal (and
	(in-stack T6 S6)
	(in-stack T33 S33)
	(in-stack T24 S24)
))
)
