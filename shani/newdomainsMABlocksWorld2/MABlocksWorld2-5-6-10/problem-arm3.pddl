(define 
(problem MABlocksWorld2-5-6-10-arm3)
(:domain MABlocksWorld2-5-6-10-arm3)
(:init
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T0 S0)
	(clear T19 S19)
	(on B0 T20 S20)
	(clear B0 S20)
	(in-stack B0 S20)
	(clear T21 S21)
	(clear T22 S22)
	(clear T0 S0)
)
(:goal (and
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T0 S0)
	(on B4 T0 S0)
	(clear B4 S0)
	(in-stack B4 S0)
))
)
