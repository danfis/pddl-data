(define 
(problem MABlocksWorld2-4-5-9-arm3)
(:domain MABlocksWorld2-4-5-9-arm3)
(:init
	(in-stack T11 S11)
	(in-stack T25 S25)
	(in-stack T15 S15)
	(clear T11 S11)
	(clear T25 S25)
	(on B1 T15 S15)
	(clear B1 S15)
	(in-stack B1 S15)
)
(:goal (and
	(in-stack T11 S11)
	(in-stack T25 S25)
	(in-stack T15 S15)
))
)
