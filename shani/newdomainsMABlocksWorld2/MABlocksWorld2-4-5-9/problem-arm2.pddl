(define 
(problem MABlocksWorld2-4-5-9-arm2)
(:domain MABlocksWorld2-4-5-9-arm2)
(:init
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T0 S0)
	(on B0 T16 S16)
	(clear B0 S16)
	(in-stack B0 S16)
	(clear T17 S17)
	(clear T18 S18)
	(clear T19 S19)
	(on B2 T20 S20)
	(in-stack B2 S20)
	(on B4 B2 S20)
	(clear B4 S20)
	(in-stack B4 S20)
	(clear T21 S21)
	(clear T22 S22)
	(clear T23 S23)
	(clear T24 S24)
	(clear T0 S0)
)
(:goal (and
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T0 S0)
	(on B0 T19 S19)
	(clear B0 S19)
	(in-stack B0 S19)
	(on B1 T0 S0)
	(clear B1 S0)
	(in-stack B1 S0)
))
)
