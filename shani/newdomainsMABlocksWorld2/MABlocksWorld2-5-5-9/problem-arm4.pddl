(define 
(problem MABlocksWorld2-5-5-9-arm4)
(:domain MABlocksWorld2-5-5-9-arm4)
(:init
	(in-stack T0 S0)
	(in-stack T33 S33)
	(in-stack T21 S21)
	(clear T0 S0)
	(clear T33 S33)
	(clear T21 S21)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T33 S33)
	(in-stack T21 S21)
))
)
