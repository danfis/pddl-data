(define 
(domain MABlocksWorld2-6-5-8-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 A5 - ARM
	 S16 - STACK
	 (:private S40 - STACK)
	 S28 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s16-s40
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (in-stack ?bSource S16) (clear ?bMove S16) (clear ?bTarget S40) (in-stack ?bTarget S40) )
:effect (and (not (on ?bMove ?bSource S16)) (not (in-stack ?bMove S16)) (on ?bMove ?bTarget S40) (in-stack ?bMove S40) (clear ?bSource S16) (not  (clear ?bMove S16)) (clear ?bMove S40) (not (clear ?bTarget S40)))
)

(:action move-a5-s40-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S40) (in-stack ?bSource S40) (clear ?bMove S40) (clear ?bTarget S16) (in-stack ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S40)) (not (in-stack ?bMove S40)) (on ?bMove ?bTarget S16) (in-stack ?bMove S16) (clear ?bSource S40) (not  (clear ?bMove S40)) (clear ?bMove S16) (not (clear ?bTarget S16)))
)

(:action move-a5-s40-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S40) (in-stack ?bSource S40) (clear ?bMove S40) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S40)) (not (in-stack ?bMove S40)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S40) (not  (clear ?bMove S40)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

(:action move-a5-s28-s40
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S40) (in-stack ?bTarget S40) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S40) (in-stack ?bMove S40) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S40) (not (clear ?bTarget S40)))
)

)