(define 
(problem MABlocksWorld2-6-5-8-arm5)
(:domain MABlocksWorld2-6-5-8-arm5)
(:init
	(in-stack T16 S16)
	(in-stack T40 S40)
	(in-stack T28 S28)
	(clear T16 S16)
	(clear T40 S40)
	(clear T28 S28)
)
(:goal (and
	(in-stack T16 S16)
	(in-stack T40 S40)
	(in-stack T28 S28)
))
)
