(define 
(domain MABlocksWorld2-6-5-8-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 A4 - ARM
	 S32 - STACK
	 (:private S33 - STACK)
	 (:private S34 - STACK)
	 (:private S35 - STACK)
	 (:private S36 - STACK)
	 (:private S37 - STACK)
	 (:private S38 - STACK)
	 (:private S39 - STACK)
	 S0 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s32-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a4-s33-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

(:action move-a4-s33-s34
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S34) (in-stack ?bTarget S34) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S34) (in-stack ?bMove S34) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S34) (not (clear ?bTarget S34)))
)

(:action move-a4-s34-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S34) (in-stack ?bSource S34) (clear ?bMove S34) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S34)) (not (in-stack ?bMove S34)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S34) (not  (clear ?bMove S34)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a4-s34-s35
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S34) (in-stack ?bSource S34) (clear ?bMove S34) (clear ?bTarget S35) (in-stack ?bTarget S35) )
:effect (and (not (on ?bMove ?bSource S34)) (not (in-stack ?bMove S34)) (on ?bMove ?bTarget S35) (in-stack ?bMove S35) (clear ?bSource S34) (not  (clear ?bMove S34)) (clear ?bMove S35) (not (clear ?bTarget S35)))
)

(:action move-a4-s35-s34
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S35) (in-stack ?bSource S35) (clear ?bMove S35) (clear ?bTarget S34) (in-stack ?bTarget S34) )
:effect (and (not (on ?bMove ?bSource S35)) (not (in-stack ?bMove S35)) (on ?bMove ?bTarget S34) (in-stack ?bMove S34) (clear ?bSource S35) (not  (clear ?bMove S35)) (clear ?bMove S34) (not (clear ?bTarget S34)))
)

(:action move-a4-s35-s36
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S35) (in-stack ?bSource S35) (clear ?bMove S35) (clear ?bTarget S36) (in-stack ?bTarget S36) )
:effect (and (not (on ?bMove ?bSource S35)) (not (in-stack ?bMove S35)) (on ?bMove ?bTarget S36) (in-stack ?bMove S36) (clear ?bSource S35) (not  (clear ?bMove S35)) (clear ?bMove S36) (not (clear ?bTarget S36)))
)

(:action move-a4-s36-s35
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S36) (in-stack ?bSource S36) (clear ?bMove S36) (clear ?bTarget S35) (in-stack ?bTarget S35) )
:effect (and (not (on ?bMove ?bSource S36)) (not (in-stack ?bMove S36)) (on ?bMove ?bTarget S35) (in-stack ?bMove S35) (clear ?bSource S36) (not  (clear ?bMove S36)) (clear ?bMove S35) (not (clear ?bTarget S35)))
)

(:action move-a4-s36-s37
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S36) (in-stack ?bSource S36) (clear ?bMove S36) (clear ?bTarget S37) (in-stack ?bTarget S37) )
:effect (and (not (on ?bMove ?bSource S36)) (not (in-stack ?bMove S36)) (on ?bMove ?bTarget S37) (in-stack ?bMove S37) (clear ?bSource S36) (not  (clear ?bMove S36)) (clear ?bMove S37) (not (clear ?bTarget S37)))
)

(:action move-a4-s37-s36
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S37) (in-stack ?bSource S37) (clear ?bMove S37) (clear ?bTarget S36) (in-stack ?bTarget S36) )
:effect (and (not (on ?bMove ?bSource S37)) (not (in-stack ?bMove S37)) (on ?bMove ?bTarget S36) (in-stack ?bMove S36) (clear ?bSource S37) (not  (clear ?bMove S37)) (clear ?bMove S36) (not (clear ?bTarget S36)))
)

(:action move-a4-s37-s38
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S37) (in-stack ?bSource S37) (clear ?bMove S37) (clear ?bTarget S38) (in-stack ?bTarget S38) )
:effect (and (not (on ?bMove ?bSource S37)) (not (in-stack ?bMove S37)) (on ?bMove ?bTarget S38) (in-stack ?bMove S38) (clear ?bSource S37) (not  (clear ?bMove S37)) (clear ?bMove S38) (not (clear ?bTarget S38)))
)

(:action move-a4-s38-s37
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S38) (in-stack ?bSource S38) (clear ?bMove S38) (clear ?bTarget S37) (in-stack ?bTarget S37) )
:effect (and (not (on ?bMove ?bSource S38)) (not (in-stack ?bMove S38)) (on ?bMove ?bTarget S37) (in-stack ?bMove S37) (clear ?bSource S38) (not  (clear ?bMove S38)) (clear ?bMove S37) (not (clear ?bTarget S37)))
)

(:action move-a4-s38-s39
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S38) (in-stack ?bSource S38) (clear ?bMove S38) (clear ?bTarget S39) (in-stack ?bTarget S39) )
:effect (and (not (on ?bMove ?bSource S38)) (not (in-stack ?bMove S38)) (on ?bMove ?bTarget S39) (in-stack ?bMove S39) (clear ?bSource S38) (not  (clear ?bMove S38)) (clear ?bMove S39) (not (clear ?bTarget S39)))
)

(:action move-a4-s39-s38
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S39) (in-stack ?bSource S39) (clear ?bMove S39) (clear ?bTarget S38) (in-stack ?bTarget S38) )
:effect (and (not (on ?bMove ?bSource S39)) (not (in-stack ?bMove S39)) (on ?bMove ?bTarget S38) (in-stack ?bMove S38) (clear ?bSource S39) (not  (clear ?bMove S39)) (clear ?bMove S38) (not (clear ?bTarget S38)))
)

(:action move-a4-s39-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S39) (in-stack ?bSource S39) (clear ?bMove S39) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S39)) (not (in-stack ?bMove S39)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S39) (not  (clear ?bMove S39)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a4-s0-s39
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S39) (in-stack ?bTarget S39) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S39) (in-stack ?bMove S39) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S39) (not (clear ?bTarget S39)))
)

)