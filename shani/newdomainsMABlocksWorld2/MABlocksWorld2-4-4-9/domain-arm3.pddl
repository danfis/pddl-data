(define 
(domain MABlocksWorld2-4-4-9-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A3 - ARM
	 S1 - STACK
	 (:private S28 - STACK)
	 S20 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s1-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (in-stack ?bSource S1) (clear ?bMove S1) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S1)) (not (in-stack ?bMove S1)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S1) (not  (clear ?bMove S1)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

(:action move-a3-s28-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S1) (in-stack ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S1) (in-stack ?bMove S1) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S1) (not (clear ?bTarget S1)))
)

(:action move-a3-s28-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S20) (in-stack ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S20) (in-stack ?bMove S20) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S20) (not (clear ?bTarget S20)))
)

(:action move-a3-s20-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (in-stack ?bSource S20) (clear ?bMove S20) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S20)) (not (in-stack ?bMove S20)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S20) (not  (clear ?bMove S20)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

)