(define 
(problem MABlocksWorld2-4-4-9-arm3)
(:domain MABlocksWorld2-4-4-9-arm3)
(:init
	(in-stack T1 S1)
	(in-stack T28 S28)
	(in-stack T20 S20)
	(clear T1 S1)
	(clear T28 S28)
	(on B0 T20 S20)
	(clear B0 S20)
	(in-stack B0 S20)
)
(:goal (and
	(in-stack T1 S1)
	(in-stack T28 S28)
	(in-stack T20 S20)
))
)
