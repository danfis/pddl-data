(define 
(domain MABlocksWorld2-5-6-9-arm2)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 A2 - ARM
	 S18 - STACK
	 (:private S19 - STACK)
	 (:private S20 - STACK)
	 (:private S21 - STACK)
	 (:private S22 - STACK)
	 (:private S23 - STACK)
	 S24 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a2-s18-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (in-stack ?bSource S18) (clear ?bMove S18) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S18)) (not (in-stack ?bMove S18)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S18) (not  (clear ?bMove S18)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

(:action move-a2-s19-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S18) (in-stack ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S18) (in-stack ?bMove S18) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S18) (not (clear ?bTarget S18)))
)

(:action move-a2-s19-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S20) (in-stack ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S20) (in-stack ?bMove S20) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S20) (not (clear ?bTarget S20)))
)

(:action move-a2-s20-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (in-stack ?bSource S20) (clear ?bMove S20) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S20)) (not (in-stack ?bMove S20)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S20) (not  (clear ?bMove S20)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

(:action move-a2-s20-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (in-stack ?bSource S20) (clear ?bMove S20) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S20)) (not (in-stack ?bMove S20)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S20) (not  (clear ?bMove S20)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

(:action move-a2-s21-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S20) (in-stack ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S20) (in-stack ?bMove S20) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S20) (not (clear ?bTarget S20)))
)

(:action move-a2-s21-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S22) (in-stack ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S22) (in-stack ?bMove S22) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S22) (not (clear ?bTarget S22)))
)

(:action move-a2-s22-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (in-stack ?bSource S22) (clear ?bMove S22) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S22)) (not (in-stack ?bMove S22)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S22) (not  (clear ?bMove S22)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

(:action move-a2-s22-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (in-stack ?bSource S22) (clear ?bMove S22) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S22)) (not (in-stack ?bMove S22)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S22) (not  (clear ?bMove S22)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a2-s23-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S22) (in-stack ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S22) (in-stack ?bMove S22) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S22) (not (clear ?bTarget S22)))
)

(:action move-a2-s23-s24
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S24) (in-stack ?bTarget S24) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S24) (in-stack ?bMove S24) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S24) (not (clear ?bTarget S24)))
)

(:action move-a2-s24-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S24) (in-stack ?bSource S24) (clear ?bMove S24) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S24)) (not (in-stack ?bMove S24)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S24) (not  (clear ?bMove S24)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

)