(define 
(domain MABlocksWorld2-5-5-10-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 A4 - ARM
	 S11 - STACK
	 (:private S28 - STACK)
	 S15 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s11-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

(:action move-a4-s28-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

(:action move-a4-s28-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S15) (in-stack ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S15) (in-stack ?bMove S15) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S15) (not (clear ?bTarget S15)))
)

(:action move-a4-s15-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (in-stack ?bSource S15) (clear ?bMove S15) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S15)) (not (in-stack ?bMove S15)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S15) (not  (clear ?bMove S15)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

)