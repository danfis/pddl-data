(define 
(problem MABlocksWorld2-5-5-10-arm4)
(:domain MABlocksWorld2-5-5-10-arm4)
(:init
	(in-stack T11 S11)
	(in-stack T28 S28)
	(in-stack T15 S15)
	(clear T11 S11)
	(clear T28 S28)
	(clear T15 S15)
)
(:goal (and
	(in-stack T11 S11)
	(in-stack T28 S28)
	(in-stack T15 S15)
))
)
