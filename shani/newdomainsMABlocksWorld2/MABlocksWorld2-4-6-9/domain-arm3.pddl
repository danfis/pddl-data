(define 
(domain MABlocksWorld2-4-6-9-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A3 - ARM
	 S1 - STACK
	 (:private S22 - STACK)
	 S16 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s1-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (in-stack ?bSource S1) (clear ?bMove S1) (clear ?bTarget S22) (in-stack ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S1)) (not (in-stack ?bMove S1)) (on ?bMove ?bTarget S22) (in-stack ?bMove S22) (clear ?bSource S1) (not  (clear ?bMove S1)) (clear ?bMove S22) (not (clear ?bTarget S22)))
)

(:action move-a3-s22-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (in-stack ?bSource S22) (clear ?bMove S22) (clear ?bTarget S1) (in-stack ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S22)) (not (in-stack ?bMove S22)) (on ?bMove ?bTarget S1) (in-stack ?bMove S1) (clear ?bSource S22) (not  (clear ?bMove S22)) (clear ?bMove S1) (not (clear ?bTarget S1)))
)

(:action move-a3-s22-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S22) (in-stack ?bSource S22) (clear ?bMove S22) (clear ?bTarget S16) (in-stack ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S22)) (not (in-stack ?bMove S22)) (on ?bMove ?bTarget S16) (in-stack ?bMove S16) (clear ?bSource S22) (not  (clear ?bMove S22)) (clear ?bMove S16) (not (clear ?bTarget S16)))
)

(:action move-a3-s16-s22
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (in-stack ?bSource S16) (clear ?bMove S16) (clear ?bTarget S22) (in-stack ?bTarget S22) )
:effect (and (not (on ?bMove ?bSource S16)) (not (in-stack ?bMove S16)) (on ?bMove ?bTarget S22) (in-stack ?bMove S22) (clear ?bSource S16) (not  (clear ?bMove S16)) (clear ?bMove S22) (not (clear ?bTarget S22)))
)

)