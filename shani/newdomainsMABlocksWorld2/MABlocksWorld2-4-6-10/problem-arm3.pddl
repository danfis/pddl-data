(define 
(problem MABlocksWorld2-4-6-10-arm3)
(:domain MABlocksWorld2-4-6-10-arm3)
(:init
	(in-stack T2 S2)
	(in-stack T27 S27)
	(in-stack T23 S23)
	(clear T2 S2)
	(clear T27 S27)
	(clear T23 S23)
)
(:goal (and
	(in-stack T2 S2)
	(in-stack T27 S27)
	(in-stack T23 S23)
))
)
