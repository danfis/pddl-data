(define 
(domain MABlocksWorld2-4-6-10-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 A3 - ARM
	 S2 - STACK
	 (:private S27 - STACK)
	 S23 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s2-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S2) (in-stack ?bSource S2) (clear ?bMove S2) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S2)) (not (in-stack ?bMove S2)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S2) (not  (clear ?bMove S2)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

(:action move-a3-s27-s2
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S2) (in-stack ?bTarget S2) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S2) (in-stack ?bMove S2) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S2) (not (clear ?bTarget S2)))
)

(:action move-a3-s27-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a3-s23-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

)