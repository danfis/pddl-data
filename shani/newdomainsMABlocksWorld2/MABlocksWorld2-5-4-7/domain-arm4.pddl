(define 
(domain MABlocksWorld2-5-4-7-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 A4 - ARM
	 S4 - STACK
	 (:private S26 - STACK)
	 S19 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s4-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (in-stack ?bSource S4) (clear ?bMove S4) (clear ?bTarget S26) (in-stack ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S4)) (not (in-stack ?bMove S4)) (on ?bMove ?bTarget S26) (in-stack ?bMove S26) (clear ?bSource S4) (not  (clear ?bMove S4)) (clear ?bMove S26) (not (clear ?bTarget S26)))
)

(:action move-a4-s26-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (in-stack ?bSource S26) (clear ?bMove S26) (clear ?bTarget S4) (in-stack ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S26)) (not (in-stack ?bMove S26)) (on ?bMove ?bTarget S4) (in-stack ?bMove S4) (clear ?bSource S26) (not  (clear ?bMove S26)) (clear ?bMove S4) (not (clear ?bTarget S4)))
)

(:action move-a4-s26-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S26) (in-stack ?bSource S26) (clear ?bMove S26) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S26)) (not (in-stack ?bMove S26)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S26) (not  (clear ?bMove S26)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

(:action move-a4-s19-s26
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S26) (in-stack ?bTarget S26) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S26) (in-stack ?bMove S26) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S26) (not (clear ?bTarget S26)))
)

)