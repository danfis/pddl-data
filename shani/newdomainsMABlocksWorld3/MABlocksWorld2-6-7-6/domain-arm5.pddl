(define 
(domain MABlocksWorld2-6-7-6-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A5 - ARM
	 S14 - STACK
	 (:private S33 - STACK)
	 S24 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s14-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (in-stack ?bSource S14) (clear ?bMove S14) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S14)) (not (in-stack ?bMove S14)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S14) (not  (clear ?bMove S14)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a5-s33-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S14) (in-stack ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S14) (in-stack ?bMove S14) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S14) (not (clear ?bTarget S14)))
)

(:action move-a5-s33-s24
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S24) (in-stack ?bTarget S24) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S24) (in-stack ?bMove S24) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S24) (not (clear ?bTarget S24)))
)

(:action move-a5-s24-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S24) (in-stack ?bSource S24) (clear ?bMove S24) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S24)) (not (in-stack ?bMove S24)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S24) (not  (clear ?bMove S24)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

)