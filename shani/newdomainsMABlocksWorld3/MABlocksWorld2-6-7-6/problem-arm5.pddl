(define 
(problem MABlocksWorld2-6-7-6-arm5)
(:domain MABlocksWorld2-6-7-6-arm5)
(:init
	(in-stack T14 S14)
	(in-stack T33 S33)
	(in-stack T24 S24)
	(clear T14 S14)
	(clear T33 S33)
	(clear T24 S24)
)
(:goal (and
	(in-stack T14 S14)
	(in-stack T33 S33)
	(in-stack T24 S24)
))
)
