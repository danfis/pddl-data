(define 
(domain MABlocksWorld2-6-8-6-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A5 - ARM
	 S0 - STACK
	 (:private S27 - STACK)
	 S15 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s0-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

(:action move-a5-s27-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a5-s27-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S15) (in-stack ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S15) (in-stack ?bMove S15) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S15) (not (clear ?bTarget S15)))
)

(:action move-a5-s15-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (in-stack ?bSource S15) (clear ?bMove S15) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S15)) (not (in-stack ?bMove S15)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S15) (not  (clear ?bMove S15)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

)