(define 
(problem MABlocksWorld2-6-8-6-arm5)
(:domain MABlocksWorld2-6-8-6-arm5)
(:init
	(in-stack T0 S0)
	(in-stack T27 S27)
	(in-stack T15 S15)
	(clear T0 S0)
	(clear T27 S27)
	(clear T15 S15)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T27 S27)
	(in-stack T15 S15)
))
)
