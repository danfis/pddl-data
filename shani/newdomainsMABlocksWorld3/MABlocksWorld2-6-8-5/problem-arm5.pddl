(define 
(problem MABlocksWorld2-6-8-5-arm5)
(:domain MABlocksWorld2-6-8-5-arm5)
(:init
	(in-stack T9 S9)
	(in-stack T24 S24)
	(in-stack T13 S13)
	(on B3 T9 S9)
	(clear B3 S9)
	(in-stack B3 S9)
	(clear T24 S24)
	(clear T13 S13)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T24 S24)
	(in-stack T13 S13)
))
)
