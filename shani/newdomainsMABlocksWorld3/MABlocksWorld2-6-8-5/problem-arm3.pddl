(define 
(problem MABlocksWorld2-6-8-5-arm3)
(:domain MABlocksWorld2-6-8-5-arm3)
(:init
	(in-stack T15 S15)
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(clear T15 S15)
	(clear T16 S16)
	(clear T17 S17)
	(clear T18 S18)
	(clear T19 S19)
)
(:goal (and
	(in-stack T15 S15)
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(on B0 T19 S19)
	(in-stack B0 S19)
	(on B2 B0 S19)
	(clear B2 S19)
	(in-stack B2 S19)
))
)
