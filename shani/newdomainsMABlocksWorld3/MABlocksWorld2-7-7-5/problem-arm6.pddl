(define 
(problem MABlocksWorld2-7-7-5-arm6)
(:domain MABlocksWorld2-7-7-5-arm6)
(:init
	(in-stack T12 S12)
	(in-stack T27 S27)
	(in-stack T21 S21)
	(clear T12 S12)
	(clear T27 S27)
	(clear T21 S21)
)
(:goal (and
	(in-stack T12 S12)
	(in-stack T27 S27)
	(in-stack T21 S21)
))
)
