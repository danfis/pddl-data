(define 
(domain MABlocksWorld2-7-7-5-arm6)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 A6 - ARM
	 S12 - STACK
	 (:private S27 - STACK)
	 S21 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a6-s12-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (in-stack ?bSource S12) (clear ?bMove S12) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S12)) (not (in-stack ?bMove S12)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S12) (not  (clear ?bMove S12)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

(:action move-a6-s27-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S12) (in-stack ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S12) (in-stack ?bMove S12) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S12) (not (clear ?bTarget S12)))
)

(:action move-a6-s27-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S27) (in-stack ?bSource S27) (clear ?bMove S27) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S27)) (not (in-stack ?bMove S27)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S27) (not  (clear ?bMove S27)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

(:action move-a6-s21-s27
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S27) (in-stack ?bTarget S27) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S27) (in-stack ?bMove S27) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S27) (not (clear ?bTarget S27)))
)

)