(define 
(problem MABlocksWorld2-7-7-6-arm5)
(:domain MABlocksWorld2-7-7-6-arm5)
(:init
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(in-stack T32 S32)
	(in-stack T33 S33)
	(in-stack T34 S34)
	(in-stack T0 S0)
	(clear T28 S28)
	(clear T29 S29)
	(clear T30 S30)
	(clear T31 S31)
	(clear T32 S32)
	(clear T33 S33)
	(clear T34 S34)
	(on B6 T0 S0)
	(clear B6 S0)
	(in-stack B6 S0)
)
(:goal (and
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(in-stack T32 S32)
	(in-stack T33 S33)
	(in-stack T34 S34)
	(in-stack T0 S0)
))
)
