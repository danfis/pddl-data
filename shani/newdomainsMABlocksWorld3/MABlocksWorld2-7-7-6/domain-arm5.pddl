(define 
(domain MABlocksWorld2-7-7-6-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 A5 - ARM
	 S28 - STACK
	 (:private S29 - STACK)
	 (:private S30 - STACK)
	 (:private S31 - STACK)
	 S32 - STACK
	 (:private S33 - STACK)
	 (:private S34 - STACK)
	 S0 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s28-s29
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S28) (in-stack ?bSource S28) (clear ?bMove S28) (clear ?bTarget S29) (in-stack ?bTarget S29) )
:effect (and (not (on ?bMove ?bSource S28)) (not (in-stack ?bMove S28)) (on ?bMove ?bTarget S29) (in-stack ?bMove S29) (clear ?bSource S28) (not  (clear ?bMove S28)) (clear ?bMove S29) (not (clear ?bTarget S29)))
)

(:action move-a5-s29-s28
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S29) (in-stack ?bSource S29) (clear ?bMove S29) (clear ?bTarget S28) (in-stack ?bTarget S28) )
:effect (and (not (on ?bMove ?bSource S29)) (not (in-stack ?bMove S29)) (on ?bMove ?bTarget S28) (in-stack ?bMove S28) (clear ?bSource S29) (not  (clear ?bMove S29)) (clear ?bMove S28) (not (clear ?bTarget S28)))
)

(:action move-a5-s29-s30
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S29) (in-stack ?bSource S29) (clear ?bMove S29) (clear ?bTarget S30) (in-stack ?bTarget S30) )
:effect (and (not (on ?bMove ?bSource S29)) (not (in-stack ?bMove S29)) (on ?bMove ?bTarget S30) (in-stack ?bMove S30) (clear ?bSource S29) (not  (clear ?bMove S29)) (clear ?bMove S30) (not (clear ?bTarget S30)))
)

(:action move-a5-s30-s29
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S30) (in-stack ?bSource S30) (clear ?bMove S30) (clear ?bTarget S29) (in-stack ?bTarget S29) )
:effect (and (not (on ?bMove ?bSource S30)) (not (in-stack ?bMove S30)) (on ?bMove ?bTarget S29) (in-stack ?bMove S29) (clear ?bSource S30) (not  (clear ?bMove S30)) (clear ?bMove S29) (not (clear ?bTarget S29)))
)

(:action move-a5-s30-s31
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S30) (in-stack ?bSource S30) (clear ?bMove S30) (clear ?bTarget S31) (in-stack ?bTarget S31) )
:effect (and (not (on ?bMove ?bSource S30)) (not (in-stack ?bMove S30)) (on ?bMove ?bTarget S31) (in-stack ?bMove S31) (clear ?bSource S30) (not  (clear ?bMove S30)) (clear ?bMove S31) (not (clear ?bTarget S31)))
)

(:action move-a5-s31-s30
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S31) (in-stack ?bSource S31) (clear ?bMove S31) (clear ?bTarget S30) (in-stack ?bTarget S30) )
:effect (and (not (on ?bMove ?bSource S31)) (not (in-stack ?bMove S31)) (on ?bMove ?bTarget S30) (in-stack ?bMove S30) (clear ?bSource S31) (not  (clear ?bMove S31)) (clear ?bMove S30) (not (clear ?bTarget S30)))
)

(:action move-a5-s31-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S31) (in-stack ?bSource S31) (clear ?bMove S31) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S31)) (not (in-stack ?bMove S31)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S31) (not  (clear ?bMove S31)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

(:action move-a5-s32-s31
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S31) (in-stack ?bTarget S31) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S31) (in-stack ?bMove S31) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S31) (not (clear ?bTarget S31)))
)

(:action move-a5-s32-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a5-s33-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

(:action move-a5-s33-s34
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S33) (in-stack ?bSource S33) (clear ?bMove S33) (clear ?bTarget S34) (in-stack ?bTarget S34) )
:effect (and (not (on ?bMove ?bSource S33)) (not (in-stack ?bMove S33)) (on ?bMove ?bTarget S34) (in-stack ?bMove S34) (clear ?bSource S33) (not  (clear ?bMove S33)) (clear ?bMove S34) (not (clear ?bTarget S34)))
)

(:action move-a5-s34-s33
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S34) (in-stack ?bSource S34) (clear ?bMove S34) (clear ?bTarget S33) (in-stack ?bTarget S33) )
:effect (and (not (on ?bMove ?bSource S34)) (not (in-stack ?bMove S34)) (on ?bMove ?bTarget S33) (in-stack ?bMove S33) (clear ?bSource S34) (not  (clear ?bMove S34)) (clear ?bMove S33) (not (clear ?bTarget S33)))
)

(:action move-a5-s34-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S34) (in-stack ?bSource S34) (clear ?bMove S34) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S34)) (not (in-stack ?bMove S34)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S34) (not  (clear ?bMove S34)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a5-s0-s34
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S34) (in-stack ?bTarget S34) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S34) (in-stack ?bMove S34) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S34) (not (clear ?bTarget S34)))
)

)