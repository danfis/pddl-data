(define 
(problem MABlocksWorld2-7-7-6-arm3)
(:domain MABlocksWorld2-7-7-6-arm3)
(:init
	(in-stack T15 S15)
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(on B4 T15 S15)
	(clear B4 S15)
	(in-stack B4 S15)
	(on B1 T16 S16)
	(clear B1 S16)
	(in-stack B1 S16)
	(clear T17 S17)
	(on B3 T18 S18)
	(clear B3 S18)
	(in-stack B3 S18)
	(clear T19 S19)
	(clear T20 S20)
	(clear T21 S21)
)
(:goal (and
	(in-stack T15 S15)
	(in-stack T16 S16)
	(in-stack T17 S17)
	(in-stack T18 S18)
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
))
)
