(define 
(domain MABlocksWorld2-7-7-6-arm6)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 A6 - ARM
	 S8 - STACK
	 (:private S35 - STACK)
	 S32 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a6-s8-s35
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S35) (in-stack ?bTarget S35) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S35) (in-stack ?bMove S35) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S35) (not (clear ?bTarget S35)))
)

(:action move-a6-s35-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S35) (in-stack ?bSource S35) (clear ?bMove S35) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S35)) (not (in-stack ?bMove S35)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S35) (not  (clear ?bMove S35)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a6-s35-s32
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S35) (in-stack ?bSource S35) (clear ?bMove S35) (clear ?bTarget S32) (in-stack ?bTarget S32) )
:effect (and (not (on ?bMove ?bSource S35)) (not (in-stack ?bMove S35)) (on ?bMove ?bTarget S32) (in-stack ?bMove S32) (clear ?bSource S35) (not  (clear ?bMove S35)) (clear ?bMove S32) (not (clear ?bTarget S32)))
)

(:action move-a6-s32-s35
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S32) (in-stack ?bSource S32) (clear ?bMove S32) (clear ?bTarget S35) (in-stack ?bTarget S35) )
:effect (and (not (on ?bMove ?bSource S32)) (not (in-stack ?bMove S32)) (on ?bMove ?bTarget S35) (in-stack ?bMove S35) (clear ?bSource S32) (not  (clear ?bMove S32)) (clear ?bMove S35) (not (clear ?bTarget S35)))
)

)