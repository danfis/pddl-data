(define 
(problem MALogistics-7-19-5-a2)
(:domain MALogistics-7-19-5-a2)
(:init
	(city-in-area C23 A2)
	(city-in-area C24 A2)
	(city-in-area C25 A2)
	(truck-in-area T2 A2)
	(truck-in-city T2 C23)
	(adj C24 C23)
	(adj C23 C24)
	(adj C25 C24)
	(adj C24 C25)
)
(:goal (and
	(package-in-city P3 C25)
))
)
