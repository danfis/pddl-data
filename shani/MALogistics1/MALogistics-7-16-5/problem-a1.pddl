(define 
(problem MALogistics-7-16-5-a1)
(:domain MALogistics-7-16-5-a1)
(:init
	(city-in-area C2 A1)
	(truck-in-area T1 A1)
	(truck-in-city T1 C2)
)
(:goal (and
	(package-in-city P1 C2)
))
)
