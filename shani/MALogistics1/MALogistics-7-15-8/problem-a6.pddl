(define 
(problem MALogistics-7-15-8-a6)
(:domain MALogistics-7-15-8-a6)
(:init
	(city-in-area C32 A6)
	(city-in-area C33 A6)
	(city-in-area C34 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C32)
	(adj C33 C32)
	(adj C32 C33)
	(adj C34 C33)
	(adj C33 C34)
)
(:goal (and
	(package-in-city P3 C32)
))
)
