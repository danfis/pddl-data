(define 
(problem MALogistics-7-21-7-a5)
(:domain MALogistics-7-21-7-a5)
(:init
	(city-in-area C47 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C47)
)
(:goal (and
	(package-in-city P2 C47)
))
)
