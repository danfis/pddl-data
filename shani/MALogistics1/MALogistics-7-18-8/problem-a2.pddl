(define 
(problem MALogistics-7-18-8-a2)
(:domain MALogistics-7-18-8-a2)
(:init
	(city-in-area C7 A2)
	(city-in-area C8 A2)
	(city-in-area C9 A2)
	(truck-in-area T2 A2)
	(truck-in-city T2 C7)
	(adj C8 C7)
	(adj C7 C8)
	(adj C9 C8)
	(adj C8 C9)
)
(:goal (and
	(package-in-city P1 C9)
))
)
