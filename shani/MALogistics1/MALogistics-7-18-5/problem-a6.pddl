(define 
(problem MALogistics-7-18-5-a6)
(:domain MALogistics-7-18-5-a6)
(:init
	(city-in-area C36 A6)
	(city-in-area C37 A6)
	(city-in-area C38 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C36)
	(adj C37 C36)
	(adj C36 C37)
	(adj C38 C37)
	(adj C37 C38)
)
(:goal (and
	(package-in-city P2 C38)
))
)
