(define 
(problem MALogistics-7-17-4-a6)
(:domain MALogistics-7-17-4-a6)
(:init
	(city-in-area C39 A6)
	(city-in-area C40 A6)
	(city-in-area C41 A6)
	(city-in-area C42 A6)
	(city-in-area C43 A6)
	(city-in-area C44 A6)
	(city-in-area C45 A6)
	(city-in-area C46 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C39)
	(adj C40 C39)
	(adj C39 C40)
	(adj C41 C40)
	(adj C40 C41)
	(adj C42 C41)
	(adj C41 C42)
	(adj C43 C42)
	(adj C42 C43)
	(adj C44 C43)
	(adj C43 C44)
	(adj C45 C44)
	(adj C44 C45)
	(adj C46 C45)
	(adj C45 C46)
	(package-in-city P0 C45)
	(package-in-city P1 C41)
	(package-in-city P2 C45)
)
(:goal (and
	(package-in-city P1 C42)
	(package-in-city P3 C41)
))
)
