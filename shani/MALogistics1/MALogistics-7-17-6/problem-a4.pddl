(define 
(problem MALogistics-7-17-6-a4)
(:domain MALogistics-7-17-6-a4)
(:init
	(city-in-area C18 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C18)
	(package-in-city P4 C18)
)
(:goal (and
	(package-in-city P1 C18)
))
)
