(define 
(problem MALogistics-7-17-6-a3)
(:domain MALogistics-7-17-6-a3)
(:init
	(city-in-area C18 A3)
	(city-in-area C0 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C18)
	(adj C0 C18)
	(adj C18 C0)
	(package-in-city P4 C18)
)
(:goal (and
	(package-in-city P1 C18)
))
)
