(define 
(problem MALogistics-9-8-7-a4)
(:domain MALogistics-9-8-7-a4)
(:init
	(city-in-area C10 A4)
	(city-in-area C0 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C10)
	(adj C0 C10)
	(adj C10 C0)
	(package-in-city P0 C0)
)
(:goal (and
	(package-in-city P0 C0)
	(package-in-city P3 C10)
	(package-in-city P5 C10)
))
)
