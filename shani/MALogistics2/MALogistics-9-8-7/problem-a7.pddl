(define 
(problem MALogistics-9-8-7-a7)
(:domain MALogistics-9-8-7-a7)
(:init
	(city-in-area C18 A7)
	(city-in-area C19 A7)
	(city-in-area C20 A7)
	(city-in-area C21 A7)
	(city-in-area C22 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C18)
	(adj C19 C18)
	(adj C18 C19)
	(adj C20 C19)
	(adj C19 C20)
	(adj C21 C20)
	(adj C20 C21)
	(adj C22 C21)
	(adj C21 C22)
)
(:goal (and
	(package-in-city P4 C19)
))
)
