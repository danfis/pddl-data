(define 
(problem MALogistics-9-8-7-a6)
(:domain MALogistics-9-8-7-a6)
(:init
	(city-in-area C16 A6)
	(city-in-area C17 A6)
	(city-in-area C18 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C16)
	(adj C17 C16)
	(adj C16 C17)
	(adj C18 C17)
	(adj C17 C18)
)
(:goal (and
	(package-in-city P1 C16)
	(package-in-city P2 C17)
))
)
