(define 
(problem MALogistics-9-7-9-a8)
(:domain MALogistics-9-7-9-a8)
(:init
	(city-in-area C31 A8)
	(city-in-area C32 A8)
	(city-in-area C33 A8)
	(city-in-area C34 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C31)
	(adj C32 C31)
	(adj C31 C32)
	(adj C33 C32)
	(adj C32 C33)
	(adj C34 C33)
	(adj C33 C34)
	(package-in-city P4 C31)
)
(:goal (and
	(package-in-city P1 C32)
))
)
