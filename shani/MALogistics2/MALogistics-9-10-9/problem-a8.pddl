(define 
(problem MALogistics-9-10-9-a8)
(:domain MALogistics-9-10-9-a8)
(:init
	(city-in-area C46 A8)
	(city-in-area C47 A8)
	(city-in-area C48 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C46)
	(adj C47 C46)
	(adj C46 C47)
	(adj C48 C47)
	(adj C47 C48)
)
(:goal (and
	(package-in-city P0 C46)
))
)
