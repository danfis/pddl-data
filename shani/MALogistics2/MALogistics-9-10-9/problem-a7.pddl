(define 
(problem MALogistics-9-10-9-a7)
(:domain MALogistics-9-10-9-a7)
(:init
	(city-in-area C39 A7)
	(city-in-area C40 A7)
	(city-in-area C41 A7)
	(city-in-area C42 A7)
	(city-in-area C43 A7)
	(city-in-area C44 A7)
	(city-in-area C45 A7)
	(city-in-area C46 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C39)
	(adj C40 C39)
	(adj C39 C40)
	(adj C41 C40)
	(adj C40 C41)
	(adj C42 C41)
	(adj C41 C42)
	(adj C43 C42)
	(adj C42 C43)
	(adj C44 C43)
	(adj C43 C44)
	(adj C45 C44)
	(adj C44 C45)
	(adj C46 C45)
	(adj C45 C46)
	(package-in-city P4 C39)
	(package-in-city P5 C41)
)
(:goal (and
	(package-in-city P0 C46)
	(package-in-city P2 C40)
	(package-in-city P3 C45)
	(package-in-city P4 C41)
	(package-in-city P6 C45)
))
)
