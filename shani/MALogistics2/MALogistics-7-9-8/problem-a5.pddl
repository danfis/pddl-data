(define 
(problem MALogistics-7-9-8-a5)
(:domain MALogistics-7-9-8-a5)
(:init
	(city-in-area C15 A5)
	(city-in-area C16 A5)
	(city-in-area C17 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C15)
	(adj C16 C15)
	(adj C15 C16)
	(adj C17 C16)
	(adj C16 C17)
	(package-in-city P1 C15)
	(package-in-city P3 C16)
)
(:goal (and
	(package-in-city P0 C17)
	(package-in-city P2 C16)
))
)
