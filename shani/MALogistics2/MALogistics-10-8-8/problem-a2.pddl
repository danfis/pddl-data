(define 
(problem MALogistics-10-8-8-a2)
(:domain MALogistics-10-8-8-a2)
(:init
	(city-in-area C16 A2)
	(city-in-area C17 A2)
	(city-in-area C18 A2)
	(truck-in-area T2 A2)
	(truck-in-city T2 C16)
	(adj C17 C16)
	(adj C16 C17)
	(adj C18 C17)
	(adj C17 C18)
	(package-in-city P2 C17)
)
(:goal (and
	(package-in-city P4 C17)
))
)
