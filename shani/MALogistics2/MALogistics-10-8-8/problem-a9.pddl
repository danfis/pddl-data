(define 
(problem MALogistics-10-8-8-a9)
(:domain MALogistics-10-8-8-a9)
(:init
	(city-in-area C34 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C34)
)
(:goal (and
	(package-in-city P5 C34)
))
)
