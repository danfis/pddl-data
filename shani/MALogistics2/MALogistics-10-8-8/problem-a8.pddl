(define 
(problem MALogistics-10-8-8-a8)
(:domain MALogistics-10-8-8-a8)
(:init
	(city-in-area C33 A8)
	(city-in-area C34 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C33)
	(adj C34 C33)
	(adj C33 C34)
)
(:goal (and
	(package-in-city P5 C34)
))
)
