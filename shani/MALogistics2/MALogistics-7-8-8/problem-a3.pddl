(define 
(problem MALogistics-7-8-8-a3)
(:domain MALogistics-7-8-8-a3)
(:init
	(city-in-area C12 A3)
	(city-in-area C0 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C12)
	(adj C0 C12)
	(adj C12 C0)
	(package-in-city P5 C12)
)
(:goal (and
	(package-in-city P0 C12)
))
)
