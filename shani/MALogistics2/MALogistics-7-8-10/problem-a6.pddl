(define 
(problem MALogistics-7-8-10-a6)
(:domain MALogistics-7-8-10-a6)
(:init
	(city-in-area C30 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C30)
)
(:goal (and
	(package-in-city P6 C30)
))
)
