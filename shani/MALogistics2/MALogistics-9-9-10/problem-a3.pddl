(define 
(problem MALogistics-9-9-10-a3)
(:domain MALogistics-9-9-10-a3)
(:init
	(city-in-area C14 A3)
	(city-in-area C15 A3)
	(city-in-area C16 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C14)
	(adj C15 C14)
	(adj C14 C15)
	(adj C16 C15)
	(adj C15 C16)
)
(:goal (and
	(package-in-city P4 C15)
))
)
