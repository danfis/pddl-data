(define 
(problem MALogistics-10-7-7-a5)
(:domain MALogistics-10-7-7-a5)
(:init
	(city-in-area C13 A5)
	(city-in-area C14 A5)
	(city-in-area C15 A5)
	(city-in-area C16 A5)
	(city-in-area C17 A5)
	(city-in-area C0 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C13)
	(adj C14 C13)
	(adj C13 C14)
	(adj C15 C14)
	(adj C14 C15)
	(adj C16 C15)
	(adj C15 C16)
	(adj C17 C16)
	(adj C16 C17)
	(adj C0 C17)
	(adj C17 C0)
)
(:goal (and
	(package-in-city P1 C13)
))
)
