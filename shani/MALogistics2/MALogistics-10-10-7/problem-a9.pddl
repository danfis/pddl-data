(define 
(problem MALogistics-10-10-7-a9)
(:domain MALogistics-10-10-7-a9)
(:init
	(city-in-area C50 A9)
	(city-in-area C51 A9)
	(city-in-area C52 A9)
	(city-in-area C53 A9)
	(city-in-area C54 A9)
	(city-in-area C55 A9)
	(city-in-area C56 A9)
	(city-in-area C57 A9)
	(city-in-area C58 A9)
	(city-in-area C59 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C50)
	(adj C51 C50)
	(adj C50 C51)
	(adj C52 C51)
	(adj C51 C52)
	(adj C53 C52)
	(adj C52 C53)
	(adj C54 C53)
	(adj C53 C54)
	(adj C55 C54)
	(adj C54 C55)
	(adj C56 C55)
	(adj C55 C56)
	(adj C57 C56)
	(adj C56 C57)
	(adj C58 C57)
	(adj C57 C58)
	(adj C59 C58)
	(adj C58 C59)
	(package-in-city P6 C58)
)
(:goal (and
	(package-in-city P0 C53)
	(package-in-city P2 C52)
	(package-in-city P6 C56)
))
)
