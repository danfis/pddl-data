(define 
(problem MALogistics-8-8-9-a7)
(:domain MALogistics-8-8-9-a7)
(:init
	(city-in-area C30 A7)
	(city-in-area C31 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C30)
	(adj C31 C30)
	(adj C30 C31)
)
(:goal (and
	(package-in-city P2 C30)
	(package-in-city P3 C31)
	(package-in-city P6 C30)
))
)
