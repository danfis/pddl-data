(define 
(problem MALogistics-9-7-7-a8)
(:domain MALogistics-9-7-7-a8)
(:init
	(city-in-area C32 A8)
	(city-in-area C33 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C32)
	(adj C33 C32)
	(adj C32 C33)
)
(:goal (and
	(package-in-city P6 C32)
))
)
