(define 
(problem MALogistics-9-7-7-a7)
(:domain MALogistics-9-7-7-a7)
(:init
	(city-in-area C31 A7)
	(city-in-area C32 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C31)
	(adj C32 C31)
	(adj C31 C32)
)
(:goal (and
	(package-in-city P6 C32)
))
)
