(define 
(problem MALogistics-9-10-8-a7)
(:domain MALogistics-9-10-8-a7)
(:init
	(city-in-area C33 A7)
	(city-in-area C34 A7)
	(city-in-area C35 A7)
	(city-in-area C36 A7)
	(city-in-area C37 A7)
	(city-in-area C38 A7)
	(city-in-area C39 A7)
	(city-in-area C40 A7)
	(city-in-area C41 A7)
	(city-in-area C42 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C33)
	(adj C34 C33)
	(adj C33 C34)
	(adj C35 C34)
	(adj C34 C35)
	(adj C36 C35)
	(adj C35 C36)
	(adj C37 C36)
	(adj C36 C37)
	(adj C38 C37)
	(adj C37 C38)
	(adj C39 C38)
	(adj C38 C39)
	(adj C40 C39)
	(adj C39 C40)
	(adj C41 C40)
	(adj C40 C41)
	(adj C42 C41)
	(adj C41 C42)
	(package-in-city P0 C38)
	(package-in-city P5 C36)
	(package-in-city P6 C38)
)
(:goal (and
	(package-in-city P7 C33)
))
)
