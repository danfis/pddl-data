(define 
(problem MALogistics-9-10-8-a3)
(:domain MALogistics-9-10-8-a3)
(:init
	(city-in-area C17 A3)
	(city-in-area C18 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C17)
	(adj C18 C17)
	(adj C17 C18)
	(package-in-city P1 C17)
	(package-in-city P3 C17)
)
(:goal (and
	(package-in-city P5 C17)
))
)
