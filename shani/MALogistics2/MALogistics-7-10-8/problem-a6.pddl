(define 
(problem MALogistics-7-10-8-a6)
(:domain MALogistics-7-10-8-a6)
(:init
	(city-in-area C35 A6)
	(city-in-area C36 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C35)
	(adj C36 C35)
	(adj C35 C36)
)
(:goal (and
	(package-in-city P4 C35)
))
)
