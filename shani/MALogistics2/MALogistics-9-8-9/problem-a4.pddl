(define 
(problem MALogistics-9-8-9-a4)
(:domain MALogistics-9-8-9-a4)
(:init
	(city-in-area C6 A4)
	(city-in-area C0 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C6)
	(adj C0 C6)
	(adj C6 C0)
)
(:goal (and
	(package-in-city P0 C0)
	(package-in-city P1 C0)
	(package-in-city P3 C0)
))
)
