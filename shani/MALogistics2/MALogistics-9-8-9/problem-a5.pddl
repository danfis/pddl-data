(define 
(problem MALogistics-9-8-9-a5)
(:domain MALogistics-9-8-9-a5)
(:init
	(city-in-area C6 A5)
	(city-in-area C7 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C6)
	(adj C7 C6)
	(adj C6 C7)
	(package-in-city P7 C7)
)
(:goal (and
	(package-in-city P8 C7)
))
)
