(define 
(problem MALogistics-10-9-8-a8)
(:domain MALogistics-10-9-8-a8)
(:init
	(city-in-area C28 A8)
	(city-in-area C29 A8)
	(city-in-area C30 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C28)
	(adj C29 C28)
	(adj C28 C29)
	(adj C30 C29)
	(adj C29 C30)
)
(:goal (and
	(package-in-city P1 C30)
))
)
