(define 
(problem MALogistics-8-10-8-a7)
(:domain MALogistics-8-10-8-a7)
(:init
	(city-in-area C40 A7)
	(city-in-area C41 A7)
	(city-in-area C42 A7)
	(city-in-area C43 A7)
	(city-in-area C44 A7)
	(city-in-area C45 A7)
	(city-in-area C46 A7)
	(city-in-area C47 A7)
	(city-in-area C48 A7)
	(city-in-area C49 A7)
	(city-in-area C50 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C40)
	(adj C41 C40)
	(adj C40 C41)
	(adj C42 C41)
	(adj C41 C42)
	(adj C43 C42)
	(adj C42 C43)
	(adj C44 C43)
	(adj C43 C44)
	(adj C45 C44)
	(adj C44 C45)
	(adj C46 C45)
	(adj C45 C46)
	(adj C47 C46)
	(adj C46 C47)
	(adj C48 C47)
	(adj C47 C48)
	(adj C49 C48)
	(adj C48 C49)
	(adj C50 C49)
	(adj C49 C50)
	(package-in-city P2 C40)
	(package-in-city P7 C46)
)
(:goal (and
	(package-in-city P7 C49)
))
)
