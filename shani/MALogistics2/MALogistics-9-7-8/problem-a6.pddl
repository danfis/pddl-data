(define 
(problem MALogistics-9-7-8-a6)
(:domain MALogistics-9-7-8-a6)
(:init
	(city-in-area C26 A6)
	(city-in-area C27 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C26)
	(adj C27 C26)
	(adj C26 C27)
)
(:goal (and
	(package-in-city P0 C26)
	(package-in-city P5 C27)
))
)
