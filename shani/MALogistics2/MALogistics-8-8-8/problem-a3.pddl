(define 
(problem MALogistics-8-8-8-a3)
(:domain MALogistics-8-8-8-a3)
(:init
	(city-in-area C7 A3)
	(city-in-area C8 A3)
	(city-in-area C9 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C7)
	(adj C8 C7)
	(adj C7 C8)
	(adj C9 C8)
	(adj C8 C9)
)
(:goal (and
	(package-in-city P3 C9)
))
)
