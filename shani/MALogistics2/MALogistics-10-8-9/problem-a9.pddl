(define 
(problem MALogistics-10-8-9-a9)
(:domain MALogistics-10-8-9-a9)
(:init
	(city-in-area C20 A9)
	(city-in-area C21 A9)
	(city-in-area C22 A9)
	(city-in-area C23 A9)
	(city-in-area C24 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C20)
	(adj C21 C20)
	(adj C20 C21)
	(adj C22 C21)
	(adj C21 C22)
	(adj C23 C22)
	(adj C22 C23)
	(adj C24 C23)
	(adj C23 C24)
	(package-in-city P6 C23)
)
(:goal (and
	(package-in-city P2 C24)
))
)
