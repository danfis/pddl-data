(define 
(problem MALogistics-10-8-9-a0)
(:domain MALogistics-10-8-9-a0)
(:init
	(city-in-area C0 A0)
	(truck-in-area T0 A0)
	(truck-in-city T0 C0)
	(package-in-city P3 C0)
)
(:goal (and
	(package-in-city P4 C0)
))
)
