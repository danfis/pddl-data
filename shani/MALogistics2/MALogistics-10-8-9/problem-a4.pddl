(define 
(problem MALogistics-10-8-9-a4)
(:domain MALogistics-10-8-9-a4)
(:init
	(city-in-area C10 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C10)
)
(:goal (and
	(package-in-city P0 C10)
))
)
