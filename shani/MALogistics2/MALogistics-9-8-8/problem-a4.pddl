(define 
(problem MALogistics-9-8-8-a4)
(:domain MALogistics-9-8-8-a4)
(:init
	(city-in-area C23 A4)
	(city-in-area C24 A4)
	(city-in-area C25 A4)
	(city-in-area C26 A4)
	(city-in-area C0 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C23)
	(adj C24 C23)
	(adj C23 C24)
	(adj C25 C24)
	(adj C24 C25)
	(adj C26 C25)
	(adj C25 C26)
	(adj C0 C26)
	(adj C26 C0)
)
(:goal (and
	(package-in-city P0 C0)
	(package-in-city P5 C24)
	(package-in-city P6 C24)
))
)
