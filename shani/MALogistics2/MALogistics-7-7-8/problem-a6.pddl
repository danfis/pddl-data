(define 
(problem MALogistics-7-7-8-a6)
(:domain MALogistics-7-7-8-a6)
(:init
	(city-in-area C29 A6)
	(city-in-area C30 A6)
	(city-in-area C31 A6)
	(city-in-area C32 A6)
	(city-in-area C33 A6)
	(city-in-area C34 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C29)
	(adj C30 C29)
	(adj C29 C30)
	(adj C31 C30)
	(adj C30 C31)
	(adj C32 C31)
	(adj C31 C32)
	(adj C33 C32)
	(adj C32 C33)
	(adj C34 C33)
	(adj C33 C34)
	(package-in-city P5 C34)
)
(:goal (and
	(package-in-city P3 C33)
))
)
