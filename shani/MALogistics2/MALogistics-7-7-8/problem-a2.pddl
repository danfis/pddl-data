(define 
(problem MALogistics-7-7-8-a2)
(:domain MALogistics-7-7-8-a2)
(:init
	(city-in-area C13 A2)
	(truck-in-area T2 A2)
	(truck-in-city T2 C13)
)
(:goal (and
	(package-in-city P1 C13)
))
)
