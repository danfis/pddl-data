(define 
(problem MALogistics-10-7-8-a8)
(:domain MALogistics-10-7-8-a8)
(:init
	(city-in-area C38 A8)
	(city-in-area C39 A8)
	(city-in-area C40 A8)
	(city-in-area C41 A8)
	(city-in-area C42 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C38)
	(adj C39 C38)
	(adj C38 C39)
	(adj C40 C39)
	(adj C39 C40)
	(adj C41 C40)
	(adj C40 C41)
	(adj C42 C41)
	(adj C41 C42)
	(package-in-city P1 C40)
	(package-in-city P5 C39)
	(package-in-city P6 C40)
)
(:goal (and
	(package-in-city P7 C42)
))
)
