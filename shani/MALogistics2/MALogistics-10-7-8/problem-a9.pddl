(define 
(problem MALogistics-10-7-8-a9)
(:domain MALogistics-10-7-8-a9)
(:init
	(city-in-area C42 A9)
	(city-in-area C43 A9)
	(city-in-area C44 A9)
	(truck-in-area T9 A9)
	(truck-in-city T9 C42)
	(adj C43 C42)
	(adj C42 C43)
	(adj C44 C43)
	(adj C43 C44)
)
(:goal (and
	(package-in-city P7 C42)
))
)
