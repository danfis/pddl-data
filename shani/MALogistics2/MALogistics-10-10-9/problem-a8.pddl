(define 
(problem MALogistics-10-10-9-a8)
(:domain MALogistics-10-10-9-a8)
(:init
	(city-in-area C35 A8)
	(city-in-area C36 A8)
	(city-in-area C37 A8)
	(city-in-area C38 A8)
	(city-in-area C39 A8)
	(city-in-area C40 A8)
	(city-in-area C41 A8)
	(city-in-area C42 A8)
	(city-in-area C43 A8)
	(city-in-area C44 A8)
	(city-in-area C45 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C35)
	(adj C36 C35)
	(adj C35 C36)
	(adj C37 C36)
	(adj C36 C37)
	(adj C38 C37)
	(adj C37 C38)
	(adj C39 C38)
	(adj C38 C39)
	(adj C40 C39)
	(adj C39 C40)
	(adj C41 C40)
	(adj C40 C41)
	(adj C42 C41)
	(adj C41 C42)
	(adj C43 C42)
	(adj C42 C43)
	(adj C44 C43)
	(adj C43 C44)
	(adj C45 C44)
	(adj C44 C45)
	(package-in-city P0 C38)
)
(:goal (and
	(package-in-city P7 C37)
))
)
