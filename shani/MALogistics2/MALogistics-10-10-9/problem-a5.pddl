(define 
(problem MALogistics-10-10-9-a5)
(:domain MALogistics-10-10-9-a5)
(:init
	(city-in-area C29 A5)
	(city-in-area C0 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C29)
	(adj C0 C29)
	(adj C29 C0)
)
(:goal (and
	(package-in-city P1 C0)
))
)
