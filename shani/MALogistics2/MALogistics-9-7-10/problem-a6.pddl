(define 
(problem MALogistics-9-7-10-a6)
(:domain MALogistics-9-7-10-a6)
(:init
	(city-in-area C16 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C16)
)
(:goal (and
	(package-in-city P8 C16)
))
)
