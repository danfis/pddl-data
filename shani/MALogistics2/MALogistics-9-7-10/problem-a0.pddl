(define 
(problem MALogistics-9-7-10-a0)
(:domain MALogistics-9-7-10-a0)
(:init
	(city-in-area C0 A0)
	(truck-in-area T0 A0)
	(truck-in-city T0 C0)
)
(:goal (and
	(package-in-city P0 C0)
))
)
