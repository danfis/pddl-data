(define 
(problem MALogistics-9-7-10-a5)
(:domain MALogistics-9-7-10-a5)
(:init
	(city-in-area C16 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C16)
)
(:goal (and
	(package-in-city P8 C16)
))
)
