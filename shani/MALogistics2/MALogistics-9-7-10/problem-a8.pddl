(define 
(problem MALogistics-9-7-10-a8)
(:domain MALogistics-9-7-10-a8)
(:init
	(city-in-area C20 A8)
	(city-in-area C21 A8)
	(city-in-area C22 A8)
	(city-in-area C23 A8)
	(city-in-area C24 A8)
	(city-in-area C25 A8)
	(city-in-area C26 A8)
	(city-in-area C27 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C20)
	(adj C21 C20)
	(adj C20 C21)
	(adj C22 C21)
	(adj C21 C22)
	(adj C23 C22)
	(adj C22 C23)
	(adj C24 C23)
	(adj C23 C24)
	(adj C25 C24)
	(adj C24 C25)
	(adj C26 C25)
	(adj C25 C26)
	(adj C27 C26)
	(adj C26 C27)
	(package-in-city P0 C20)
	(package-in-city P8 C21)
)
(:goal (and
	(package-in-city P1 C25)
	(package-in-city P6 C21)
	(package-in-city P7 C20)
))
)
