(define 
(problem MALogistics-8-10-10-a4)
(:domain MALogistics-8-10-10-a4)
(:init
	(city-in-area C17 A4)
	(city-in-area C18 A4)
	(city-in-area C0 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C17)
	(adj C18 C17)
	(adj C17 C18)
	(adj C0 C18)
	(adj C18 C0)
	(package-in-city P5 C18)
)
(:goal (and
	(package-in-city P5 C18)
))
)
