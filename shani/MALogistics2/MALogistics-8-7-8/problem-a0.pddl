(define 
(problem MALogistics-8-7-8-a0)
(:domain MALogistics-8-7-8-a0)
(:init
	(city-in-area C0 A0)
	(truck-in-area T0 A0)
	(truck-in-city T0 C0)
)
(:goal (and
	(package-in-city P1 C0)
))
)
