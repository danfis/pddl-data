(define 
(problem MALogistics-10-8-10-a6)
(:domain MALogistics-10-8-10-a6)
(:init
	(city-in-area C17 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C17)
	(package-in-city P7 C17)
)
(:goal (and
	(package-in-city P0 C17)
))
)
