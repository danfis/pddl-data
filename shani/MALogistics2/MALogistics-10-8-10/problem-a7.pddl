(define 
(problem MALogistics-10-8-10-a7)
(:domain MALogistics-10-8-10-a7)
(:init
	(city-in-area C17 A7)
	(city-in-area C18 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C17)
	(adj C18 C17)
	(adj C17 C18)
	(package-in-city P7 C17)
)
(:goal (and
	(package-in-city P0 C17)
))
)
