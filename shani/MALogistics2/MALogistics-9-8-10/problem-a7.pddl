(define 
(problem MALogistics-9-8-10-a7)
(:domain MALogistics-9-8-10-a7)
(:init
	(city-in-area C14 A7)
	(city-in-area C15 A7)
	(city-in-area C16 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C14)
	(adj C15 C14)
	(adj C14 C15)
	(adj C16 C15)
	(adj C15 C16)
)
(:goal (and
	(package-in-city P0 C16)
	(package-in-city P8 C15)
))
)
