(define 
(problem MALogistics-9-8-10-a5)
(:domain MALogistics-9-8-10-a5)
(:init
	(city-in-area C7 A5)
	(city-in-area C8 A5)
	(city-in-area C9 A5)
	(city-in-area C10 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C7)
	(adj C8 C7)
	(adj C7 C8)
	(adj C9 C8)
	(adj C8 C9)
	(adj C10 C9)
	(adj C9 C10)
	(package-in-city P9 C7)
)
(:goal (and
	(package-in-city P3 C7)
	(package-in-city P6 C10)
	(package-in-city P7 C10)
))
)
