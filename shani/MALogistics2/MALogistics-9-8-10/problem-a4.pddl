(define 
(problem MALogistics-9-8-10-a4)
(:domain MALogistics-9-8-10-a4)
(:init
	(city-in-area C7 A4)
	(city-in-area C0 A4)
	(truck-in-area T4 A4)
	(truck-in-city T4 C7)
	(adj C0 C7)
	(adj C7 C0)
	(package-in-city P5 C0)
	(package-in-city P9 C7)
)
(:goal (and
	(package-in-city P3 C7)
))
)
