(define 
(problem MALogistics-10-7-9-a3)
(:domain MALogistics-10-7-9-a3)
(:init
	(city-in-area C17 A3)
	(truck-in-area T3 A3)
	(truck-in-city T3 C17)
	(package-in-city P1 C17)
)
(:goal (and
	(package-in-city P4 C17)
))
)
