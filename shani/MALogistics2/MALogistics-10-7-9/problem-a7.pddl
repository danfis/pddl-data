(define 
(problem MALogistics-10-7-9-a7)
(:domain MALogistics-10-7-9-a7)
(:init
	(city-in-area C33 A7)
	(city-in-area C34 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C33)
	(adj C34 C33)
	(adj C33 C34)
)
(:goal (and
	(package-in-city P7 C33)
))
)
