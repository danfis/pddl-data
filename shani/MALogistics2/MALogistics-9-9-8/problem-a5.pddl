(define 
(problem MALogistics-9-9-8-a5)
(:domain MALogistics-9-9-8-a5)
(:init
	(city-in-area C23 A5)
	(city-in-area C24 A5)
	(city-in-area C25 A5)
	(truck-in-area T5 A5)
	(truck-in-city T5 C23)
	(adj C24 C23)
	(adj C23 C24)
	(adj C25 C24)
	(adj C24 C25)
)
(:goal (and
	(package-in-city P0 C25)
	(package-in-city P4 C23)
))
)
