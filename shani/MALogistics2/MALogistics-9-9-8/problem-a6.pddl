(define 
(problem MALogistics-9-9-8-a6)
(:domain MALogistics-9-9-8-a6)
(:init
	(city-in-area C25 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C25)
)
(:goal (and
	(package-in-city P0 C25)
))
)
