(define 
(problem MALogistics-10-10-8-a8)
(:domain MALogistics-10-10-8-a8)
(:init
	(city-in-area C44 A8)
	(truck-in-area T8 A8)
	(truck-in-city T8 C44)
)
(:goal (and
	(package-in-city P1 C44)
))
)
