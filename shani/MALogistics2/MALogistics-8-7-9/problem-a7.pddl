(define 
(problem MALogistics-8-7-9-a7)
(:domain MALogistics-8-7-9-a7)
(:init
	(city-in-area C35 A7)
	(city-in-area C36 A7)
	(truck-in-area T7 A7)
	(truck-in-city T7 C35)
	(adj C36 C35)
	(adj C35 C36)
	(package-in-city P2 C36)
	(package-in-city P6 C35)
)
(:goal (and
	(package-in-city P4 C36)
))
)
