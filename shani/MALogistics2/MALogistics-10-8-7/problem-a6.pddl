(define 
(problem MALogistics-10-8-7-a6)
(:domain MALogistics-10-8-7-a6)
(:init
	(city-in-area C21 A6)
	(city-in-area C22 A6)
	(city-in-area C23 A6)
	(city-in-area C24 A6)
	(city-in-area C25 A6)
	(city-in-area C26 A6)
	(city-in-area C27 A6)
	(truck-in-area T6 A6)
	(truck-in-city T6 C21)
	(adj C22 C21)
	(adj C21 C22)
	(adj C23 C22)
	(adj C22 C23)
	(adj C24 C23)
	(adj C23 C24)
	(adj C25 C24)
	(adj C24 C25)
	(adj C26 C25)
	(adj C25 C26)
	(adj C27 C26)
	(adj C26 C27)
)
(:goal (and
	(package-in-city P6 C21)
))
)
