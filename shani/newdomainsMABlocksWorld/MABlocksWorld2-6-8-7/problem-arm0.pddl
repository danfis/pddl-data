(define 
(problem MABlocksWorld2-6-8-7-arm0)
(:domain MABlocksWorld2-6-8-7-arm0)
(:init
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(clear T0 S0)
	(on B0 T1 S1)
	(clear B0 S1)
	(in-stack B0 S1)
	(clear T2 S2)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
))
)
