(define 
(problem MABlocksWorld2-5-6-6-arm3)
(:domain MABlocksWorld2-5-6-6-arm3)
(:init
	(in-stack T9 S9)
	(in-stack T10 S10)
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T0 S0)
	(on B1 T9 S9)
	(clear B1 S9)
	(in-stack B1 S9)
	(clear T10 S10)
	(on B5 T11 S11)
	(clear B5 S11)
	(in-stack B5 S11)
	(clear T12 S12)
	(on B3 T0 S0)
	(in-stack B3 S0)
	(on B2 B3 S0)
	(clear B2 S0)
	(in-stack B2 S0)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T10 S10)
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T0 S0)
	(on B3 T9 S9)
	(clear B3 S9)
	(in-stack B3 S9)
	(on B4 T10 S10)
	(clear B4 S10)
	(in-stack B4 S10)
	(on B5 T11 S11)
	(clear B5 S11)
	(in-stack B5 S11)
))
)
