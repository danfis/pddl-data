(define 
(domain MABlocksWorld2-5-6-6-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 A4 - ARM
	 S1 - STACK
	 (:private S13 - STACK)
	 S8 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s1-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S1) (in-stack ?bSource S1) (clear ?bMove S1) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S1)) (not (in-stack ?bMove S1)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S1) (not  (clear ?bMove S1)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

(:action move-a4-s13-s1
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S1) (in-stack ?bTarget S1) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S1) (in-stack ?bMove S1) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S1) (not (clear ?bTarget S1)))
)

(:action move-a4-s13-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a4-s8-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

)