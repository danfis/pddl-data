(define 
(problem MABlocksWorld2-5-7-7-arm2)
(:domain MABlocksWorld2-5-7-7-arm2)
(:init
	(in-stack T10 S10)
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T13 S13)
	(on B3 T10 S10)
	(clear B3 S10)
	(in-stack B3 S10)
	(clear T11 S11)
	(clear T12 S12)
	(clear T13 S13)
)
(:goal (and
	(in-stack T10 S10)
	(in-stack T11 S11)
	(in-stack T12 S12)
	(in-stack T13 S13)
))
)
