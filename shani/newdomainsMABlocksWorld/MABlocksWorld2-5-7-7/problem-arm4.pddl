(define 
(problem MABlocksWorld2-5-7-7-arm4)
(:domain MABlocksWorld2-5-7-7-arm4)
(:init
	(in-stack T6 S6)
	(in-stack T16 S16)
	(in-stack T8 S8)
	(clear T6 S6)
	(clear T16 S16)
	(on B5 T8 S8)
	(clear B5 S8)
	(in-stack B5 S8)
)
(:goal (and
	(in-stack T6 S6)
	(in-stack T16 S16)
	(in-stack T8 S8)
))
)
