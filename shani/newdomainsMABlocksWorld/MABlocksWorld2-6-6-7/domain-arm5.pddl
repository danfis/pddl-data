(define 
(domain MABlocksWorld2-6-6-7-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 A5 - ARM
	 S9 - STACK
	 (:private S25 - STACK)
	 S17 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s9-s25
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S25) (in-stack ?bTarget S25) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S25) (in-stack ?bMove S25) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S25) (not (clear ?bTarget S25)))
)

(:action move-a5-s25-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S25) (in-stack ?bSource S25) (clear ?bMove S25) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S25)) (not (in-stack ?bMove S25)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S25) (not  (clear ?bMove S25)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a5-s25-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S25) (in-stack ?bSource S25) (clear ?bMove S25) (clear ?bTarget S17) (in-stack ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S25)) (not (in-stack ?bMove S25)) (on ?bMove ?bTarget S17) (in-stack ?bMove S17) (clear ?bSource S25) (not  (clear ?bMove S25)) (clear ?bMove S17) (not (clear ?bTarget S17)))
)

(:action move-a5-s17-s25
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (in-stack ?bSource S17) (clear ?bMove S17) (clear ?bTarget S25) (in-stack ?bTarget S25) )
:effect (and (not (on ?bMove ?bSource S17)) (not (in-stack ?bMove S17)) (on ?bMove ?bTarget S25) (in-stack ?bMove S25) (clear ?bSource S17) (not  (clear ?bMove S17)) (clear ?bMove S25) (not (clear ?bTarget S25)))
)

)