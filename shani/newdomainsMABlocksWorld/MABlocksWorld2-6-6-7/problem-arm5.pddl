(define 
(problem MABlocksWorld2-6-6-7-arm5)
(:domain MABlocksWorld2-6-6-7-arm5)
(:init
	(in-stack T9 S9)
	(in-stack T25 S25)
	(in-stack T17 S17)
	(clear T9 S9)
	(clear T25 S25)
	(clear T17 S17)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T25 S25)
	(in-stack T17 S17)
	(on B2 T17 S17)
	(clear B2 S17)
	(in-stack B2 S17)
))
)
