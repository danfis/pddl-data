(define 
(problem MABlocksWorld2-6-6-7-arm4)
(:domain MABlocksWorld2-6-6-7-arm4)
(:init
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T0 S0)
	(clear T19 S19)
	(clear T20 S20)
	(clear T21 S21)
	(clear T22 S22)
	(clear T23 S23)
	(clear T24 S24)
	(clear T0 S0)
)
(:goal (and
	(in-stack T19 S19)
	(in-stack T20 S20)
	(in-stack T21 S21)
	(in-stack T22 S22)
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T0 S0)
	(on B5 T21 S21)
	(clear B5 S21)
	(in-stack B5 S21)
	(on B4 T22 S22)
	(clear B4 S22)
	(in-stack B4 S22)
))
)
