(define 
(problem MABlocksWorld2-5-5-7-arm1)
(:domain MABlocksWorld2-5-5-7-arm1)
(:init
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(clear T3 S3)
	(on B1 T4 S4)
	(in-stack B1 S4)
	(on B3 B1 S4)
	(clear B3 S4)
	(in-stack B3 S4)
	(clear T5 S5)
	(clear T6 S6)
)
(:goal (and
	(in-stack T3 S3)
	(in-stack T4 S4)
	(in-stack T5 S5)
	(in-stack T6 S6)
	(on B3 T3 S3)
	(clear B3 S3)
	(in-stack B3 S3)
))
)
