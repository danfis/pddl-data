(define 
(domain MABlocksWorld2-4-5-4-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 A3 - ARM
	 S5 - STACK
	 (:private S14 - STACK)
	 S8 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s5-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (in-stack ?bSource S5) (clear ?bMove S5) (clear ?bTarget S14) (in-stack ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S5)) (not (in-stack ?bMove S5)) (on ?bMove ?bTarget S14) (in-stack ?bMove S14) (clear ?bSource S5) (not  (clear ?bMove S5)) (clear ?bMove S14) (not (clear ?bTarget S14)))
)

(:action move-a3-s14-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (in-stack ?bSource S14) (clear ?bMove S14) (clear ?bTarget S5) (in-stack ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S14)) (not (in-stack ?bMove S14)) (on ?bMove ?bTarget S5) (in-stack ?bMove S5) (clear ?bSource S14) (not  (clear ?bMove S14)) (clear ?bMove S5) (not (clear ?bTarget S5)))
)

(:action move-a3-s14-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (in-stack ?bSource S14) (clear ?bMove S14) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S14)) (not (in-stack ?bMove S14)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S14) (not  (clear ?bMove S14)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a3-s8-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S14) (in-stack ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S14) (in-stack ?bMove S14) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S14) (not (clear ?bTarget S14)))
)

)