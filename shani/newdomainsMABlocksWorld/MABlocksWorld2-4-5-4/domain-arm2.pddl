(define 
(domain MABlocksWorld2-4-5-4-arm2)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 A2 - ARM
	 S10 - STACK
	 (:private S11 - STACK)
	 (:private S12 - STACK)
	 (:private S13 - STACK)
	 S0 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a2-s10-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

(:action move-a2-s11-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a2-s11-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S12) (in-stack ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S12) (in-stack ?bMove S12) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S12) (not (clear ?bTarget S12)))
)

(:action move-a2-s12-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (in-stack ?bSource S12) (clear ?bMove S12) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S12)) (not (in-stack ?bMove S12)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S12) (not  (clear ?bMove S12)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

(:action move-a2-s12-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (in-stack ?bSource S12) (clear ?bMove S12) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S12)) (not (in-stack ?bMove S12)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S12) (not  (clear ?bMove S12)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

(:action move-a2-s13-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S12) (in-stack ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S12) (in-stack ?bMove S12) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S12) (not (clear ?bTarget S12)))
)

(:action move-a2-s13-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a2-s0-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

)