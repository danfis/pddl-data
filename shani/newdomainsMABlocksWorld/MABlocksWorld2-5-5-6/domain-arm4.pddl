(define 
(domain MABlocksWorld2-5-5-6-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 A4 - ARM
	 S10 - STACK
	 (:private S23 - STACK)
	 S14 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s10-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

(:action move-a4-s23-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a4-s23-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S23) (in-stack ?bSource S23) (clear ?bMove S23) (clear ?bTarget S14) (in-stack ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S23)) (not (in-stack ?bMove S23)) (on ?bMove ?bTarget S14) (in-stack ?bMove S14) (clear ?bSource S23) (not  (clear ?bMove S23)) (clear ?bMove S14) (not (clear ?bTarget S14)))
)

(:action move-a4-s14-s23
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (in-stack ?bSource S14) (clear ?bMove S14) (clear ?bTarget S23) (in-stack ?bTarget S23) )
:effect (and (not (on ?bMove ?bSource S14)) (not (in-stack ?bMove S14)) (on ?bMove ?bTarget S23) (in-stack ?bMove S23) (clear ?bSource S14) (not  (clear ?bMove S14)) (clear ?bMove S23) (not (clear ?bTarget S23)))
)

)