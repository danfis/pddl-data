(define 
(problem MABlocksWorld2-5-5-6-arm4)
(:domain MABlocksWorld2-5-5-6-arm4)
(:init
	(in-stack T10 S10)
	(in-stack T23 S23)
	(in-stack T14 S14)
	(clear T10 S10)
	(clear T23 S23)
	(on B3 T14 S14)
	(clear B3 S14)
	(in-stack B3 S14)
)
(:goal (and
	(in-stack T10 S10)
	(in-stack T23 S23)
	(in-stack T14 S14)
))
)
