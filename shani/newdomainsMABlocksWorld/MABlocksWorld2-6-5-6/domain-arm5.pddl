(define 
(domain MABlocksWorld2-6-5-6-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A5 - ARM
	 S5 - STACK
	 (:private S16 - STACK)
	 S7 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s5-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (in-stack ?bSource S5) (clear ?bMove S5) (clear ?bTarget S16) (in-stack ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S5)) (not (in-stack ?bMove S5)) (on ?bMove ?bTarget S16) (in-stack ?bMove S16) (clear ?bSource S5) (not  (clear ?bMove S5)) (clear ?bMove S16) (not (clear ?bTarget S16)))
)

(:action move-a5-s16-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (in-stack ?bSource S16) (clear ?bMove S16) (clear ?bTarget S5) (in-stack ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S16)) (not (in-stack ?bMove S16)) (on ?bMove ?bTarget S5) (in-stack ?bMove S5) (clear ?bSource S16) (not  (clear ?bMove S16)) (clear ?bMove S5) (not (clear ?bTarget S5)))
)

(:action move-a5-s16-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (in-stack ?bSource S16) (clear ?bMove S16) (clear ?bTarget S7) (in-stack ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S16)) (not (in-stack ?bMove S16)) (on ?bMove ?bTarget S7) (in-stack ?bMove S7) (clear ?bSource S16) (not  (clear ?bMove S16)) (clear ?bMove S7) (not (clear ?bTarget S7)))
)

(:action move-a5-s7-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (in-stack ?bSource S7) (clear ?bMove S7) (clear ?bTarget S16) (in-stack ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S7)) (not (in-stack ?bMove S7)) (on ?bMove ?bTarget S16) (in-stack ?bMove S16) (clear ?bSource S7) (not  (clear ?bMove S7)) (clear ?bMove S16) (not (clear ?bTarget S16)))
)

)