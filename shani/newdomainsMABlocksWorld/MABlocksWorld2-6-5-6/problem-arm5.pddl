(define 
(problem MABlocksWorld2-6-5-6-arm5)
(:domain MABlocksWorld2-6-5-6-arm5)
(:init
	(in-stack T5 S5)
	(in-stack T16 S16)
	(in-stack T7 S7)
	(clear T5 S5)
	(clear T16 S16)
	(clear T7 S7)
)
(:goal (and
	(in-stack T5 S5)
	(in-stack T16 S16)
	(in-stack T7 S7)
	(on B3 T5 S5)
	(clear B3 S5)
	(in-stack B3 S5)
))
)
