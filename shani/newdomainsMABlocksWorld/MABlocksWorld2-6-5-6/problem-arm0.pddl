(define 
(problem MABlocksWorld2-6-5-6-arm0)
(:domain MABlocksWorld2-6-5-6-arm0)
(:init
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(clear T0 S0)
	(clear T1 S1)
	(on B4 T2 S2)
	(clear B4 S2)
	(in-stack B4 S2)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T1 S1)
	(in-stack T2 S2)
	(on B2 T2 S2)
	(clear B2 S2)
	(in-stack B2 S2)
))
)
