(define 
(problem MABlocksWorld2-6-5-6-arm2)
(:domain MABlocksWorld2-6-5-6-arm2)
(:init
	(in-stack T8 S8)
	(in-stack T9 S9)
	(in-stack T10 S10)
	(clear T8 S8)
	(clear T9 S9)
	(clear T10 S10)
)
(:goal (and
	(in-stack T8 S8)
	(in-stack T9 S9)
	(in-stack T10 S10)
))
)
