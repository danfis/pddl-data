(define 
(domain MABlocksWorld2-6-9-7-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 B8 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 A5 - ARM
	 S6 - STACK
	 (:private S17 - STACK)
	 S14 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s6-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (in-stack ?bSource S6) (clear ?bMove S6) (clear ?bTarget S17) (in-stack ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S6)) (not (in-stack ?bMove S6)) (on ?bMove ?bTarget S17) (in-stack ?bMove S17) (clear ?bSource S6) (not  (clear ?bMove S6)) (clear ?bMove S17) (not (clear ?bTarget S17)))
)

(:action move-a5-s17-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (in-stack ?bSource S17) (clear ?bMove S17) (clear ?bTarget S6) (in-stack ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S17)) (not (in-stack ?bMove S17)) (on ?bMove ?bTarget S6) (in-stack ?bMove S6) (clear ?bSource S17) (not  (clear ?bMove S17)) (clear ?bMove S6) (not (clear ?bTarget S6)))
)

(:action move-a5-s17-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (in-stack ?bSource S17) (clear ?bMove S17) (clear ?bTarget S14) (in-stack ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S17)) (not (in-stack ?bMove S17)) (on ?bMove ?bTarget S14) (in-stack ?bMove S14) (clear ?bSource S17) (not  (clear ?bMove S17)) (clear ?bMove S14) (not (clear ?bTarget S14)))
)

(:action move-a5-s14-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (in-stack ?bSource S14) (clear ?bMove S14) (clear ?bTarget S17) (in-stack ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S14)) (not (in-stack ?bMove S14)) (on ?bMove ?bTarget S17) (in-stack ?bMove S17) (clear ?bSource S14) (not  (clear ?bMove S14)) (clear ?bMove S17) (not (clear ?bTarget S17)))
)

)