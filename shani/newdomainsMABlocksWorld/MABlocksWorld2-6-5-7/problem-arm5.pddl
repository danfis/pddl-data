(define 
(problem MABlocksWorld2-6-5-7-arm5)
(:domain MABlocksWorld2-6-5-7-arm5)
(:init
	(in-stack T7 S7)
	(in-stack T21 S21)
	(in-stack T10 S10)
	(clear T7 S7)
	(clear T21 S21)
	(clear T10 S10)
)
(:goal (and
	(in-stack T7 S7)
	(in-stack T21 S21)
	(in-stack T10 S10)
))
)
