(define 
(domain MABlocksWorld2-6-5-7-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 A5 - ARM
	 S7 - STACK
	 (:private S21 - STACK)
	 S10 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s7-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (in-stack ?bSource S7) (clear ?bMove S7) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S7)) (not (in-stack ?bMove S7)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S7) (not  (clear ?bMove S7)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

(:action move-a5-s21-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S7) (in-stack ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S7) (in-stack ?bMove S7) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S7) (not (clear ?bTarget S7)))
)

(:action move-a5-s21-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S21) (in-stack ?bSource S21) (clear ?bMove S21) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S21)) (not (in-stack ?bMove S21)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S21) (not  (clear ?bMove S21)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a5-s10-s21
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S21) (in-stack ?bTarget S21) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S21) (in-stack ?bMove S21) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S21) (not (clear ?bTarget S21)))
)

)