(define 
(domain MABlocksWorld2-5-7-6-arm2)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 A2 - ARM
	 S5 - STACK
	 (:private S6 - STACK)
	 (:private S7 - STACK)
	 (:private S8 - STACK)
	 S9 - STACK
	 S10 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a2-s5-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (in-stack ?bSource S5) (clear ?bMove S5) (clear ?bTarget S6) (in-stack ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S5)) (not (in-stack ?bMove S5)) (on ?bMove ?bTarget S6) (in-stack ?bMove S6) (clear ?bSource S5) (not  (clear ?bMove S5)) (clear ?bMove S6) (not (clear ?bTarget S6)))
)

(:action move-a2-s6-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (in-stack ?bSource S6) (clear ?bMove S6) (clear ?bTarget S5) (in-stack ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S6)) (not (in-stack ?bMove S6)) (on ?bMove ?bTarget S5) (in-stack ?bMove S5) (clear ?bSource S6) (not  (clear ?bMove S6)) (clear ?bMove S5) (not (clear ?bTarget S5)))
)

(:action move-a2-s6-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S6) (in-stack ?bSource S6) (clear ?bMove S6) (clear ?bTarget S7) (in-stack ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S6)) (not (in-stack ?bMove S6)) (on ?bMove ?bTarget S7) (in-stack ?bMove S7) (clear ?bSource S6) (not  (clear ?bMove S6)) (clear ?bMove S7) (not (clear ?bTarget S7)))
)

(:action move-a2-s7-s6
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (in-stack ?bSource S7) (clear ?bMove S7) (clear ?bTarget S6) (in-stack ?bTarget S6) )
:effect (and (not (on ?bMove ?bSource S7)) (not (in-stack ?bMove S7)) (on ?bMove ?bTarget S6) (in-stack ?bMove S6) (clear ?bSource S7) (not  (clear ?bMove S7)) (clear ?bMove S6) (not (clear ?bTarget S6)))
)

(:action move-a2-s7-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (in-stack ?bSource S7) (clear ?bMove S7) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S7)) (not (in-stack ?bMove S7)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S7) (not  (clear ?bMove S7)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a2-s8-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S7) (in-stack ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S7) (in-stack ?bMove S7) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S7) (not (clear ?bTarget S7)))
)

(:action move-a2-s8-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a2-s9-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a2-s9-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a2-s10-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

)