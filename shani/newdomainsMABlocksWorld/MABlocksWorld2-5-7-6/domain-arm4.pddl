(define 
(domain MABlocksWorld2-5-7-6-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 A4 - ARM
	 S4 - STACK
	 (:private S13 - STACK)
	 S9 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s4-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (in-stack ?bSource S4) (clear ?bMove S4) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S4)) (not (in-stack ?bMove S4)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S4) (not  (clear ?bMove S4)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

(:action move-a4-s13-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S4) (in-stack ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S4) (in-stack ?bMove S4) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S4) (not (clear ?bTarget S4)))
)

(:action move-a4-s13-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S13) (in-stack ?bSource S13) (clear ?bMove S13) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S13)) (not (in-stack ?bMove S13)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S13) (not  (clear ?bMove S13)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a4-s9-s13
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S13) (in-stack ?bTarget S13) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S13) (in-stack ?bMove S13) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S13) (not (clear ?bTarget S13)))
)

)