(define 
(problem MABlocksWorld2-6-6-6-arm2)
(:domain MABlocksWorld2-6-6-6-arm2)
(:init
	(in-stack T12 S12)
	(in-stack T13 S13)
	(in-stack T14 S14)
	(clear T12 S12)
	(on B5 T13 S13)
	(clear B5 S13)
	(in-stack B5 S13)
	(clear T14 S14)
)
(:goal (and
	(in-stack T12 S12)
	(in-stack T13 S13)
	(in-stack T14 S14)
))
)
