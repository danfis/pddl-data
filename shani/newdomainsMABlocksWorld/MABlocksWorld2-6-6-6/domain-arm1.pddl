(define 
(domain MABlocksWorld2-6-6-6-arm1)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A1 - ARM
	 S7 - STACK
	 (:private S8 - STACK)
	 (:private S9 - STACK)
	 S10 - STACK
	 (:private S11 - STACK)
	 S12 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a1-s7-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S7) (in-stack ?bSource S7) (clear ?bMove S7) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S7)) (not (in-stack ?bMove S7)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S7) (not  (clear ?bMove S7)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a1-s8-s7
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S7) (in-stack ?bTarget S7) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S7) (in-stack ?bMove S7) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S7) (not (clear ?bTarget S7)))
)

(:action move-a1-s8-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a1-s9-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a1-s9-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S9) (in-stack ?bSource S9) (clear ?bMove S9) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S9)) (not (in-stack ?bMove S9)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S9) (not  (clear ?bMove S9)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a1-s10-s9
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S9) (in-stack ?bTarget S9) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S9) (in-stack ?bMove S9) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S9) (not (clear ?bTarget S9)))
)

(:action move-a1-s10-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

(:action move-a1-s11-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a1-s11-s12
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S12) (in-stack ?bTarget S12) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S12) (in-stack ?bMove S12) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S12) (not (clear ?bTarget S12)))
)

(:action move-a1-s12-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S12) (in-stack ?bSource S12) (clear ?bMove S12) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S12)) (not (in-stack ?bMove S12)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S12) (not  (clear ?bMove S12)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

)