(define 
(problem MABlocksWorld2-6-8-6-arm5)
(:domain MABlocksWorld2-6-8-6-arm5)
(:init
	(in-stack T4 S4)
	(in-stack T13 S13)
	(in-stack T8 S8)
	(clear T4 S4)
	(clear T13 S13)
	(clear T8 S8)
)
(:goal (and
	(in-stack T4 S4)
	(in-stack T13 S13)
	(in-stack T8 S8)
))
)
