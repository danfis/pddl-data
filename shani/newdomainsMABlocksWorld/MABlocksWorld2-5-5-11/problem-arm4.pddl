(define 
(problem MABlocksWorld2-5-5-11-arm4)
(:domain MABlocksWorld2-5-5-11-arm4)
(:init
	(in-stack T9 S9)
	(in-stack T30 S30)
	(in-stack T20 S20)
	(clear T9 S9)
	(clear T30 S30)
	(clear T20 S20)
)
(:goal (and
	(in-stack T9 S9)
	(in-stack T30 S30)
	(in-stack T20 S20)
))
)
