(define 
(domain MABlocksWorld2-5-5-11-arm2)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 T37 - BLOCK
	 T38 - BLOCK
	 T39 - BLOCK
	 T40 - BLOCK
	 T41 - BLOCK
	 T42 - BLOCK
	 T43 - BLOCK
	 T44 - BLOCK
	 T45 - BLOCK
	 T46 - BLOCK
	 T47 - BLOCK
	 T48 - BLOCK
	 T49 - BLOCK
	 T50 - BLOCK
	 T51 - BLOCK
	 T52 - BLOCK
	 T53 - BLOCK
	 T54 - BLOCK
	 T55 - BLOCK
	 A2 - ARM
	 S18 - STACK
	 (:private S19 - STACK)
	 S20 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a2-s18-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (in-stack ?bSource S18) (clear ?bMove S18) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S18)) (not (in-stack ?bMove S18)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S18) (not  (clear ?bMove S18)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

(:action move-a2-s19-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S18) (in-stack ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S18) (in-stack ?bMove S18) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S18) (not (clear ?bTarget S18)))
)

(:action move-a2-s19-s20
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S19) (in-stack ?bSource S19) (clear ?bMove S19) (clear ?bTarget S20) (in-stack ?bTarget S20) )
:effect (and (not (on ?bMove ?bSource S19)) (not (in-stack ?bMove S19)) (on ?bMove ?bTarget S20) (in-stack ?bMove S20) (clear ?bSource S19) (not  (clear ?bMove S19)) (clear ?bMove S20) (not (clear ?bTarget S20)))
)

(:action move-a2-s20-s19
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S20) (in-stack ?bSource S20) (clear ?bMove S20) (clear ?bTarget S19) (in-stack ?bTarget S19) )
:effect (and (not (on ?bMove ?bSource S20)) (not (in-stack ?bMove S20)) (on ?bMove ?bTarget S19) (in-stack ?bMove S19) (clear ?bSource S20) (not  (clear ?bMove S20)) (clear ?bMove S19) (not (clear ?bTarget S19)))
)

)