(define 
(domain MABlocksWorld2-5-8-6-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 A4 - ARM
	 S3 - STACK
	 (:private S11 - STACK)
	 S8 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s3-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S3) (in-stack ?bSource S3) (clear ?bMove S3) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S3)) (not (in-stack ?bMove S3)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S3) (not  (clear ?bMove S3)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

(:action move-a4-s11-s3
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S3) (in-stack ?bTarget S3) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S3) (in-stack ?bMove S3) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S3) (not (clear ?bTarget S3)))
)

(:action move-a4-s11-s8
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S11) (in-stack ?bSource S11) (clear ?bMove S11) (clear ?bTarget S8) (in-stack ?bTarget S8) )
:effect (and (not (on ?bMove ?bSource S11)) (not (in-stack ?bMove S11)) (on ?bMove ?bTarget S8) (in-stack ?bMove S8) (clear ?bSource S11) (not  (clear ?bMove S11)) (clear ?bMove S8) (not (clear ?bTarget S8)))
)

(:action move-a4-s8-s11
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S8) (in-stack ?bSource S8) (clear ?bMove S8) (clear ?bTarget S11) (in-stack ?bTarget S11) )
:effect (and (not (on ?bMove ?bSource S8)) (not (in-stack ?bMove S8)) (on ?bMove ?bTarget S11) (in-stack ?bMove S11) (clear ?bSource S8) (not  (clear ?bMove S8)) (clear ?bMove S11) (not (clear ?bTarget S11)))
)

)