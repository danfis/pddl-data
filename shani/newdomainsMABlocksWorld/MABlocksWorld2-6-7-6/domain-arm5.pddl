(define 
(domain MABlocksWorld2-6-7-6-arm5)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 T36 - BLOCK
	 A5 - ARM
	 S4 - STACK
	 (:private S15 - STACK)
	 S10 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a5-s4-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S4) (in-stack ?bSource S4) (clear ?bMove S4) (clear ?bTarget S15) (in-stack ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S4)) (not (in-stack ?bMove S4)) (on ?bMove ?bTarget S15) (in-stack ?bMove S15) (clear ?bSource S4) (not  (clear ?bMove S4)) (clear ?bMove S15) (not (clear ?bTarget S15)))
)

(:action move-a5-s15-s4
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (in-stack ?bSource S15) (clear ?bMove S15) (clear ?bTarget S4) (in-stack ?bTarget S4) )
:effect (and (not (on ?bMove ?bSource S15)) (not (in-stack ?bMove S15)) (on ?bMove ?bTarget S4) (in-stack ?bMove S4) (clear ?bSource S15) (not  (clear ?bMove S15)) (clear ?bMove S4) (not (clear ?bTarget S4)))
)

(:action move-a5-s15-s10
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (in-stack ?bSource S15) (clear ?bMove S15) (clear ?bTarget S10) (in-stack ?bTarget S10) )
:effect (and (not (on ?bMove ?bSource S15)) (not (in-stack ?bMove S15)) (on ?bMove ?bTarget S10) (in-stack ?bMove S10) (clear ?bSource S15) (not  (clear ?bMove S15)) (clear ?bMove S10) (not (clear ?bTarget S10)))
)

(:action move-a5-s10-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S10) (in-stack ?bSource S10) (clear ?bMove S10) (clear ?bTarget S15) (in-stack ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S10)) (not (in-stack ?bMove S10)) (on ?bMove ?bTarget S15) (in-stack ?bMove S15) (clear ?bSource S10) (not  (clear ?bMove S10)) (clear ?bMove S15) (not (clear ?bTarget S15)))
)

)