(define 
(problem MABlocksWorld2-5-6-10-arm3)
(:domain MABlocksWorld2-5-6-10-arm3)
(:init
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T25 S25)
	(in-stack T26 S26)
	(in-stack T27 S27)
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(in-stack T0 S0)
	(clear T23 S23)
	(clear T24 S24)
	(clear T25 S25)
	(clear T26 S26)
	(clear T27 S27)
	(clear T28 S28)
	(clear T29 S29)
	(clear T30 S30)
	(clear T31 S31)
	(clear T0 S0)
)
(:goal (and
	(in-stack T23 S23)
	(in-stack T24 S24)
	(in-stack T25 S25)
	(in-stack T26 S26)
	(in-stack T27 S27)
	(in-stack T28 S28)
	(in-stack T29 S29)
	(in-stack T30 S30)
	(in-stack T31 S31)
	(in-stack T0 S0)
	(on B5 T27 S27)
	(clear B5 S27)
	(in-stack B5 S27)
))
)
