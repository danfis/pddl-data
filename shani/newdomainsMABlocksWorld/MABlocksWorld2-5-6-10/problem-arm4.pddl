(define 
(problem MABlocksWorld2-5-6-10-arm4)
(:domain MABlocksWorld2-5-6-10-arm4)
(:init
	(in-stack T0 S0)
	(in-stack T32 S32)
	(in-stack T21 S21)
	(clear T0 S0)
	(clear T32 S32)
	(clear T21 S21)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T32 S32)
	(in-stack T21 S21)
))
)
