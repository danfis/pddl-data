(define 
(problem MABlocksWorld2-5-5-10-arm4)
(:domain MABlocksWorld2-5-5-10-arm4)
(:init
	(in-stack T18 S18)
	(in-stack T39 S39)
	(in-stack T24 S24)
	(clear T18 S18)
	(clear T39 S39)
	(clear T24 S24)
)
(:goal (and
	(in-stack T18 S18)
	(in-stack T39 S39)
	(in-stack T24 S24)
))
)
