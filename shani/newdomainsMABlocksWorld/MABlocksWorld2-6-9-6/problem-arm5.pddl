(define 
(problem MABlocksWorld2-6-9-6-arm5)
(:domain MABlocksWorld2-6-9-6-arm5)
(:init
	(in-stack T0 S0)
	(in-stack T18 S18)
	(in-stack T13 S13)
	(clear T0 S0)
	(on B2 T18 S18)
	(clear B2 S18)
	(in-stack B2 S18)
	(clear T13 S13)
)
(:goal (and
	(in-stack T0 S0)
	(in-stack T18 S18)
	(in-stack T13 S13)
))
)
