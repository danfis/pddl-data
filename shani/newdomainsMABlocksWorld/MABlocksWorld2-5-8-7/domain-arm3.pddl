(define 
(domain MABlocksWorld2-5-8-7-arm3)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 A3 - ARM
	 S15 - STACK
	 (:private S16 - STACK)
	 (:private S17 - STACK)
	 S0 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a3-s15-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S15) (in-stack ?bSource S15) (clear ?bMove S15) (clear ?bTarget S16) (in-stack ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S15)) (not (in-stack ?bMove S15)) (on ?bMove ?bTarget S16) (in-stack ?bMove S16) (clear ?bSource S15) (not  (clear ?bMove S15)) (clear ?bMove S16) (not (clear ?bTarget S16)))
)

(:action move-a3-s16-s15
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (in-stack ?bSource S16) (clear ?bMove S16) (clear ?bTarget S15) (in-stack ?bTarget S15) )
:effect (and (not (on ?bMove ?bSource S16)) (not (in-stack ?bMove S16)) (on ?bMove ?bTarget S15) (in-stack ?bMove S15) (clear ?bSource S16) (not  (clear ?bMove S16)) (clear ?bMove S15) (not (clear ?bTarget S15)))
)

(:action move-a3-s16-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S16) (in-stack ?bSource S16) (clear ?bMove S16) (clear ?bTarget S17) (in-stack ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S16)) (not (in-stack ?bMove S16)) (on ?bMove ?bTarget S17) (in-stack ?bMove S17) (clear ?bSource S16) (not  (clear ?bMove S16)) (clear ?bMove S17) (not (clear ?bTarget S17)))
)

(:action move-a3-s17-s16
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (in-stack ?bSource S17) (clear ?bMove S17) (clear ?bTarget S16) (in-stack ?bTarget S16) )
:effect (and (not (on ?bMove ?bSource S17)) (not (in-stack ?bMove S17)) (on ?bMove ?bTarget S16) (in-stack ?bMove S16) (clear ?bSource S17) (not  (clear ?bMove S17)) (clear ?bMove S16) (not (clear ?bTarget S16)))
)

(:action move-a3-s17-s0
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S17) (in-stack ?bSource S17) (clear ?bMove S17) (clear ?bTarget S0) (in-stack ?bTarget S0) )
:effect (and (not (on ?bMove ?bSource S17)) (not (in-stack ?bMove S17)) (on ?bMove ?bTarget S0) (in-stack ?bMove S0) (clear ?bSource S17) (not  (clear ?bMove S17)) (clear ?bMove S0) (not (clear ?bTarget S0)))
)

(:action move-a3-s0-s17
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S0) (in-stack ?bSource S0) (clear ?bMove S0) (clear ?bTarget S17) (in-stack ?bTarget S17) )
:effect (and (not (on ?bMove ?bSource S0)) (not (in-stack ?bMove S0)) (on ?bMove ?bTarget S17) (in-stack ?bMove S17) (clear ?bSource S0) (not  (clear ?bMove S0)) (clear ?bMove S17) (not (clear ?bTarget S17)))
)

)