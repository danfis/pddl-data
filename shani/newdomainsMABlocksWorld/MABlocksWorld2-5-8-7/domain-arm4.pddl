(define 
(domain MABlocksWorld2-5-8-7-arm4)
(:types BLOCK ARM STACK)
(:constants
	 B0 - BLOCK
	 B1 - BLOCK
	 B2 - BLOCK
	 B3 - BLOCK
	 B4 - BLOCK
	 B5 - BLOCK
	 B6 - BLOCK
	 B7 - BLOCK
	 T0 - BLOCK
	 T1 - BLOCK
	 T2 - BLOCK
	 T3 - BLOCK
	 T4 - BLOCK
	 T5 - BLOCK
	 T6 - BLOCK
	 T7 - BLOCK
	 T8 - BLOCK
	 T9 - BLOCK
	 T10 - BLOCK
	 T11 - BLOCK
	 T12 - BLOCK
	 T13 - BLOCK
	 T14 - BLOCK
	 T15 - BLOCK
	 T16 - BLOCK
	 T17 - BLOCK
	 T18 - BLOCK
	 T19 - BLOCK
	 T20 - BLOCK
	 T21 - BLOCK
	 T22 - BLOCK
	 T23 - BLOCK
	 T24 - BLOCK
	 T25 - BLOCK
	 T26 - BLOCK
	 T27 - BLOCK
	 T28 - BLOCK
	 T29 - BLOCK
	 T30 - BLOCK
	 T31 - BLOCK
	 T32 - BLOCK
	 T33 - BLOCK
	 T34 - BLOCK
	 T35 - BLOCK
	 A4 - ARM
	 S5 - STACK
	 (:private S18 - STACK)
	 S14 - STACK
)(:predicates
	(on ?b1 - BLOCK ?b2 - BLOCK ?s - STACK)
	(in-stack ?b - BLOCK ?s - STACK)
	(clear ?b - BLOCK ?s - STACK)
)
(:action move-a4-s5-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S5) (in-stack ?bSource S5) (clear ?bMove S5) (clear ?bTarget S18) (in-stack ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S5)) (not (in-stack ?bMove S5)) (on ?bMove ?bTarget S18) (in-stack ?bMove S18) (clear ?bSource S5) (not  (clear ?bMove S5)) (clear ?bMove S18) (not (clear ?bTarget S18)))
)

(:action move-a4-s18-s5
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (in-stack ?bSource S18) (clear ?bMove S18) (clear ?bTarget S5) (in-stack ?bTarget S5) )
:effect (and (not (on ?bMove ?bSource S18)) (not (in-stack ?bMove S18)) (on ?bMove ?bTarget S5) (in-stack ?bMove S5) (clear ?bSource S18) (not  (clear ?bMove S18)) (clear ?bMove S5) (not (clear ?bTarget S5)))
)

(:action move-a4-s18-s14
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S18) (in-stack ?bSource S18) (clear ?bMove S18) (clear ?bTarget S14) (in-stack ?bTarget S14) )
:effect (and (not (on ?bMove ?bSource S18)) (not (in-stack ?bMove S18)) (on ?bMove ?bTarget S14) (in-stack ?bMove S14) (clear ?bSource S18) (not  (clear ?bMove S18)) (clear ?bMove S14) (not (clear ?bTarget S14)))
)

(:action move-a4-s14-s18
:parameters (?bMove - BLOCK ?bSource - BLOCK ?bTarget - BLOCK)
:precondition (and (on ?bMove ?bSource S14) (in-stack ?bSource S14) (clear ?bMove S14) (clear ?bTarget S18) (in-stack ?bTarget S18) )
:effect (and (not (on ?bMove ?bSource S14)) (not (in-stack ?bMove S14)) (on ?bMove ?bTarget S18) (in-stack ?bMove S18) (clear ?bSource S14) (not  (clear ?bMove S14)) (clear ?bMove S18) (not (clear ?bTarget S18)))
)

)