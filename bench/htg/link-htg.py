#!/usr/bin/env python3

import os
import sys

SRC_DIR = '../../htg'

# Domains that have flat structure. Should just be copied.
FLAT_DOMAINS = ['genome-edit-distance',
                'genome-edit-distance-positional',
                'genome-edit-distance-split',
                'organic-synthesis-alkene',
                'organic-synthesis-MIT',
                'organic-synthesis-original',
                'pipesworld-tankage-nosplit']

NOT_FLAT_DOMAINS = [
    'blocksworld-large-simple',
    'logistics-large-simple',
    'rovers-large-simple',
    'childsnack-contents',
    'visitall-multidimensional',
    ]

def link_already_flattened_domains():
    for d in FLAT_DOMAINS:
        source = os.path.join(SRC_DIR, d)
        destin = os.path.join('./', d).lower()
        print('Link: {0} -> {1}'.format(destin, source))
        os.symlink(source, destin, target_is_directory = True)


def link_not_flat_domains():
    for d in NOT_FLAT_DOMAINS:
        source = os.path.join(SRC_DIR, d)
        destin = os.path.join('./', d).lower()
        for subd in os.listdir(source):
            src = os.path.join(source, subd)
            if os.path.isdir(src):
                dst = destin + '-{0}'.format(subd)
                dst = dst.lower()
                print('Link: {0} -> {1}'.format(dst, src))
                os.symlink(src, dst, target_is_directory = True)

if __name__ == '__main__':
    for f in os.listdir('.'):
        if os.path.islink(f):
            os.unlink(f)
    link_already_flattened_domains()
    link_not_flat_domains()
